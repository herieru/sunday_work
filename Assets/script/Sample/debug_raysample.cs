﻿///<summary>
/// 概要:シングルトンのためのクラス
///
/// <filename>
/// ファイル名:Singleton
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debug_raysample : MonoBehaviour {


    Vector3 pos = new Vector3();
    float deg_y = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        deg_y += 0.1f;
        if(360.0f < deg_y)
        {
            deg_y = 0;
        }
        Vector3 _dir =   Quaternion.Euler(0, deg_y, 0) * (Vector3.forward * 100);

       // Logger.Console.Log("角度" + deg_y);
       // Logger.Console.Log("座標" + _dir);
       // Logger.Console.DrawRay(pos, _dir, Color.white, 1, true);

		
	}
}
