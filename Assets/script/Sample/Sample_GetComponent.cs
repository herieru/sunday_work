﻿///<summary>
///GetComponentのサンプル
/// 
/// 
/// </summary>
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sample_GetComponent : MonoBehaviour
{

    GameObject _obj = null;
    MeshRenderer _renderer = null;
    int _tmp;

    public int _tmps { get; private set; }


    void Awake()
    {
        _obj = GameObject.Find("center");
        

    }

    // Use this for initialization
    void Start()
    {
        _renderer = _obj.GetComponentInChildren<MeshRenderer>();//検索を内部的に行い。最初に見つかったものだけを取得する。
        if(null == _renderer) { return; }

        Debug.Log(_renderer.gameObject.name);
        _renderer = null;
        try
        {
            _dottidemoiinnday(_renderer);
        }
        catch
        {
            Debug.Log("関数を挟んでも、ちゃんとTryCatchできるよ！");//関数を挟んでもちゃんと出来た
        }
        finally
        {

        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void _dottidemoiinnday(MeshRenderer _set_renderer)
    {
        renderer_debug_log(_set_renderer);
    }

    private void renderer_debug_log(MeshRenderer _set_renderer)
    {
        Debug.Log(_set_renderer.gameObject.name);
    }


     public void ButtonGetter()
    {
        Debug.Log("Clicke！");
    }
}
