﻿///<summary>
/// 概要:サンプル
/// UIがGraficを恐らくすべてにおいて引いているがそのUIが
/// Grafic型からすべてを取得することが可能なのかという実験サンプル
/// 
/// このスクリプトを、付けることでVector4型で、カラーが設定出来、
/// それをUpdateないで常に更新する。
///
///
/// <filename>
/// ファイル名:GraficcastScript
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraficcastScript : MonoBehaviour {

    [SerializeField]
    Color color;

    [SerializeField]
    Graphic graphic;

    [SerializeField]
    bool isRandam;


	// Use this for initialization
	void Start () {
        color = new Color(1, 1, 1, 1);
        if(null == graphic)
        {
            graphic = GetComponent<Graphic>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (isRandam)
        {
            Color _color = new Color(Random.value, Random.value, Random.value);
            graphic.color = _color;
        }
        else
        {
            graphic.color = color;
        }
        



	}
}
