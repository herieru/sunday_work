﻿///<summary>
/// 概要：シェーダーに値を与えるためのもの
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shader_sample : MonoBehaviour
{
    [SerializeField]
    public Vector3 local_por;

    public Vector3 positon;

	// Use this for initialization
	void Start () {
        //GetComponent<Renderer>().material.SetColor("BaseColor", Color.yellow);
        //_color = Color.yellow;
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Renderer>().material.SetVector("Position", transform.localPosition);
        positon = transform.position;
        local_por = transform.localPosition;
    }
}
