﻿///<summary>
///Findを試すためのソース
/// 空のゲームオブジェクトも取得することができるのかというもの
/// 
/// 
/// 
/// 
/// </summary>


using UnityEngine;
using System.Collections;

public class Find : MonoBehaviour {

    private GameObject obj = null;

	// Use this for initialization
	void Start () {
        obj = GameObject.Find("center");

	}
	
	// Update is called once per frame
	void Update () {
        if(null == obj) { return; }

        Vector3 _pos = obj.transform.position;
        _pos.z += 0.01f;
        obj.transform.position = _pos;

    }
}


//TODO:このコンポーネントから、Findとかで呼び出す。