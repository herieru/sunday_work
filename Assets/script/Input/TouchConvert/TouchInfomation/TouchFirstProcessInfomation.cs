﻿///<summary>
/// 概要：一つのタッチ情報を保持しなければいけない内容等を保存する為のもの
/// 情報をまとめるためのクラスとしての役割を持っている。
/// 
/// これに追加の情報をその媒体（スマホとかPCとか）に対して、
/// 追加の情報を行う場合は継承をする作り
/// 
/// <filename>
/// TouchFirstProcessInfomation.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class TouchFirstProcessInfomation
{
    #region field

    protected Touch touch;
    protected TouchPhase touch_phase;
    /// <summary>
    /// 開始地点　コンストラクタ時にしか入力できないようにする
    /// </summary>
    protected Vector2 start_position;
    //指のユニークなID
    protected int finger_id;
    #endregion

    #region propaty

    /// <summary>
    /// タッチ情報自身　これは状態が常に変わる前提
    /// </summary>
    public Touch touch_info
    {
        get { return touch; }
    }


    /// <summary>
    /// タッチ情報を扱うFingerID
    /// </summary>
    public int FingerID
    {
        get
        {
            return finger_id;
        }
    }

    /// <summary>
    /// タッチダウンされたタイミングでの、開始位置
    /// </summary>
    public Vector2 TouchDownPoint
    {
        get
        {
            return start_position;   
        }
    }
    
    /// <summary>
    /// 情報を消してよいか？　EndedかCancellになっていたらtrue
    /// </summary>
    public bool IsDiscardingAllowing
    {
        get
        {
            return (touch.phase == TouchPhase.Ended ||
                touch.phase == TouchPhase.Canceled);
        }
    }

    #endregion

    #region input touch infomation function group

    /// <summary>
    /// タッチ情報から変化してはいけないものとかを保持しておく
    /// </summary>
    /// <param name="_touch"></param>
    public TouchFirstProcessInfomation(Touch _touch)
    {
        touch = _touch;
        start_position = _touch.position;
        touch_phase = _touch.phase;
        finger_id = _touch.fingerId;
    }

    /// <summary>
    /// タッチの情報を新規で、入れる
    /// </summary>
    /// <param name="_touch"></param>
    public void SetNewTouchInfo(Touch _touch)
    {
        touch = _touch;
        touch_phase = _touch.phase;
    }

    #endregion


}
