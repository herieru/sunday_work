﻿///<summary>
/// 概要：スマートフォンでの、タッチの情報の取り扱いのもの。
/// TouchInfoProcesserと比べて、より細かい情報に対して情報を扱う。
/// さらに情報を加えたものを変更したい場合は、別途継承した上で、追加情報をDictionary
/// でIDに対して紐づいた、Dictionaryで管理を行う
/// 
/// 扱う情報
/// ・タッチした際に、当たったもののLayerMaskのタイプを保持しておく
/// ・
///
/// <filename>
/// CellFhoneTouchInfo.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class CellFhoneTouchInfo : TouchFirstProcessInfomation
{
    private const int NonTouch = -1;
    public LayerMask hit_layer_mask;

    #region propaty

    /// <summary>
    /// タッチが何かしらと判定を取っているか？
    /// </summary>
    public bool isHitTouch
    {
        get { return hit_layer_mask != NonTouch; }
    }

    #endregion

    public CellFhoneTouchInfo(Touch _touch):base (_touch)
    {
        //当たっていない状態として扱う。
        hit_layer_mask = NonTouch;
    }

    public void NotificationHitMask(LayerMask _mask)
    {
        hit_layer_mask = _mask;
    }
}
