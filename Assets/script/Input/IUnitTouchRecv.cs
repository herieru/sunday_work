﻿///<summary>
/// 概要：ユニット系で、タッチ情報を受け取りを行いやすくするための
/// interfaceこれを継承させる事で、ユニットがタッチ情報を受け取る機構
/// を持ち合わせている状態になる。
///
/// <filename>
/// IUnitTouchRecv.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>

using UnityEngine;

interface IUnitTouchRecv  
{
    /// <summary>
    /// タッチした際のインターフェース　
    /// </summary>
    /// <param name="_finger_id">指のID</param>
    /// <param name="_press_pos">押した場所、位置</param>
    void SetPosition(int _finger_id,Vector3 _press_pos);

    /// <summary>
    /// タッチを放す事によってどうこう行う為のインターフェース
    /// </summary>
    /// <param name="_finger_id"></param>
    /// <param name="_release_pos"></param>
    void ReleaseSetPosition(int _finger_id, Vector3 _release_pos);

}
