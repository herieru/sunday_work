﻿///<summary>
/// 概要：常に更新を行って、タッチ情報の管理＋1次加工を行う。
/// ここでいう1次加工は以下の内容
/// 取得できた情報から、その情報に対して、適切な処置な廃棄などを行えるようにするための
/// 情報を加工する。例えば、話したフレームの間までは保持し、その次のフレームには廃棄をするみたいな。
/// それを別途のシングルトンのタッチ情報を通知するものに対して通知を行う。
/// 必ずどこかでインスタンスを生成してください
/// 
/// 
/// COMMENT：そもそもタッチ情報だけを扱おうとしていることが問題なのかもしれない。
/// 
/// 
///
/// <filename>
/// TouchInfoProcesser.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>

#region　デファイン調整

//ユニティエディタ
#if UNITY_EDITOR
#define U_Editor
#endif
//アンドロイド
#if UNITY_ANDROID && !UNIY_EDITOR
#define U_Android
#endif
//アイフォン
#if UNITY_IPHONE && !UNIY_EDITOR
#define U_IPHONE
#endif

#endregion

using System.Collections.Generic;
using UnityEngine;

public class TouchInfoProcesser : MonoBehaviour 
{
    #region field

    /// <summary>
    /// タッチした情報を取得してくるスタイル
    /// </summary>
    private Dictionary<int, TouchFirstProcessInfomation> processed_touch_info_dic;


    #endregion

    #region propaty
    /// <summary>
    /// タッチの情報が存在しているかどうか？
    /// </summary>
    public bool isExistTouchInfo
    {
        get { return processed_touch_info_dic.Count > 0; }
    }

    #endregion

    /// <summary>
    /// TODO:仮で、情報を取得する為のもの　改修は行う
    /// 必ず、タッチ情報が存在している事を前提で、取得してください。
    /// </summary>
    public TouchFirstProcessInfomation GetDictionaryIndex(int _index = 0)
    {
        //対象のindexに対してアクセスが可能か？
        if(processed_touch_info_dic.Count < _index)
        {
            return null;
        }

        var _info = processed_touch_info_dic[_index];
        return _info;
    }


    /// <summary>
    /// タッチIDに基づいたIDのタッチ情報を取得を行う。
    /// </summary>
    /// <param name="_is_exist">そのIDが存在するか？</param>
    /// <param name="_finger_id">取得したいタッチID</param>
    /// <returns></returns>
    public TouchFirstProcessInfomation UseFingerIDGetDictionaryInfomation(out bool _is_exist,int _finger_id)
    {
        _is_exist = false;
        if(false == processed_touch_info_dic.ContainsKey(_finger_id))
        {
            return null;
        }
        TouchFirstProcessInfomation _touch_info = null;

        _is_exist = true;
        processed_touch_info_dic.TryGetValue(_finger_id, out _touch_info);

        return _touch_info;
    }
    
    // Use this for initialization
    void Start () 
	{
        processed_touch_info_dic = new Dictionary<int, TouchFirstProcessInfomation>();
	}


    /// <summary>
    /// デバック用の表示領域
    /// </summary>
    private void OnGUI()
    {

        foreach(var _key in processed_touch_info_dic.Keys)
        {
            TouchFirstProcessInfomation _touch_info = null;
            processed_touch_info_dic.TryGetValue(_key, out _touch_info);
            
            if(null == _touch_info)
            {
                continue;
            }

            GUILayout.Label("position:X" + _touch_info.touch_info.position.x + " Y:" + _touch_info.touch_info.position.y + " ID : " + _touch_info.touch_info.fingerId
                + " first_touch_down:X: " + _touch_info.TouchDownPoint.x + " first_touch_down:Y:" + _touch_info.TouchDownPoint.y
                , GUILayout.ExpandWidth(true));
        }
    }

    // Update is called once per frame
    void Update () 
	{
        //まだ生成されていない場合の考慮
		if(null == processed_touch_info_dic)
        {
            return;
        }
        //ここで前回のフレームの際に離された判断されている場合は取り消す
        remove_unnecessary_infomation();

#if U_Editor
        update_editor_touch();
#endif
#if U_Android
        //アンドロイドでの対応
        update_cellphone_touch();
#endif

#if U_IPHONE
//アイフォン対応　FIX;今の所するつもりなし
#endif
    }

    private void LateUpdate()
    {
        
    }

    /// <summary>
    /// 不要になっているタッチ情報を取り除く
    /// </summary>
    private void remove_unnecessary_infomation()
    {
  
        //削除を行う為のタッチIDを保持する。
        List<int> _remove_touch_id = new List<int>();

        foreach (var _key in processed_touch_info_dic.Keys)
        {
            TouchFirstProcessInfomation _procss = null;
            processed_touch_info_dic.TryGetValue(_key, out _procss);

            //基本的にはあるけど、念のため
            if (null == _procss)
            {
                continue;
            }
            
            if(_procss.IsDiscardingAllowing)
            {
                _remove_touch_id.Add(_key);
            }
        }

        //上記のforeachの中での処理でもいけると思うが、念のため
        for(int _index = 0;_index < _remove_touch_id.Count;_index++)
        {
            processed_touch_info_dic.Remove(_remove_touch_id[_index]);
        }
    }

#if U_Editor
    /// <summary>
    /// エディターによるタッチ方法
    /// </summary>
    private void update_editor_touch()
    {
        //マウスで対応
        for (int _mouse_info_access = 0; _mouse_info_access < 3; _mouse_info_access++)
        {
            TouchPhase _phase = editor_touch_info_create(_mouse_info_access);
            if (_phase != TouchPhase.Canceled)
            {
                Touch _touch = new Touch();
                //IDを仮で情報として入れる。
                _touch.fingerId = _mouse_info_access;
                _touch.position = Input.mousePosition;
                _touch.phase = _phase;
                _touch.maximumPossiblePressure = 1.0f;
                update_set_touch_info(_touch);
            }
        }
    }
#endif


#if U_Android
    /// <summary>
    /// スマホによるタッチの影響を見る
    /// </summary>
    private void update_cellphone_touch()
    {
        Touch[] _touchs = Input.touches;
        foreach(var _touch in _touchs)
        {
            update_set_touch_info(_touch);
        }
    }
#endif

    /// <summary>
    /// エディター時にタッチの情報を取得を行う。
    /// 動きがない場合の処理は行っていないので注意
    /// attention:dont get infomation what dont move frame
    /// </summary>
    /// <param name="_access_mouse_info">マウスのタッチを取得する先</param>
    /// <returns></returns>
    private TouchPhase editor_touch_info_create(int _access_mouse_info)
    {
        if(_access_mouse_info > 2 || _access_mouse_info < 0)
        {
            //アクセス範囲外なので、とりあえず、システム的なエラーとして返す
            return TouchPhase.Canceled;
        }

        //タッチし始め
        if(Input.GetMouseButtonDown(_access_mouse_info))
        {
            return TouchPhase.Began;
        }

        //タッチ中の状態
        if(Input.GetMouseButton(_access_mouse_info))
        {
            return TouchPhase.Moved;
        }

        //タッチを辞めました
        if(Input.GetMouseButtonUp(_access_mouse_info))
        {
            return TouchPhase.Ended;
        }


        //システムのエラー
        return TouchPhase.Canceled;
    }
    

    /// <summary>
    /// タッチ情報をIDでDictionaryで保存を行う。
    /// </summary>
    /// <param name="_touch"></param>
    private void update_set_touch_info(Touch _touch)
    {
        //存在していなければ、新規で追加
        if(false == processed_touch_info_dic.ContainsKey(_touch.fingerId))
        {
            TouchFirstProcessInfomation _touch_process_info = new TouchFirstProcessInfomation(_touch);

            processed_touch_info_dic.Add(_touch.fingerId, _touch_process_info);
            return;
        }

        TouchFirstProcessInfomation _get_touch;
        processed_touch_info_dic.TryGetValue(_touch.fingerId,out _get_touch);
        _get_touch.SetNewTouchInfo(_touch);

    }

}
