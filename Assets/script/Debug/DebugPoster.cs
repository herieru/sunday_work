﻿///<summary>
/// 概要：ConsolDebuggerに対しての受け付け先のもの
/// 中間のクッション的な役割を持っている。
/// でも、基本的には、ここから行う事は、投げる事だけ
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HackDebug
{
    public class DebugPoster : Singleton<DebugPoster>
    {
        #region ログの通知
        //ログの通知
        System.Action<string[], string> notification = null;

        /// <summary>
        /// 通知の呼び出し口の登録
        /// </summary>
        public void SetNotification(System.Action<string[], string> _notification)
        {
            notification = _notification;
        }

        /// <summary>
        /// 通知を一度綺麗にする。
        /// </summary>
        public void ExistNotificationNull()
        {
            notification = null;
        }

        /// <summary>
        /// 通知が出来る状態か？
        /// </summary>
        /// <returns></returns>
        public bool ExitstNotification()
        {
            if (null == notification)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// ログをセットを行う。
        /// 通知が行える状態だったら、行う。
        /// </summary>
        /// <param name="_log"></param>
        /// <param name="_tag"></param>
        /// <returns></returns>
        public bool SetLog(string _log, params string[] _tag)
        {
            if (false == ExitstNotification())
            {
                return false;
            }

            notification(_tag, _log);

            return true;
        }
        #endregion

        #region ベクトルの通知を行う。

        #region ベクトル通知インナークラス
        public class VectorNotifiData
        {
            public Vector3 start_pos;
            public Vector3 dir;
            public Color color;
        }
        #endregion

        System.Action<string[], VectorNotifiData> _vector_tags = null;

        /// <summary>
        /// ベクトルの表示を行うためのもの
        /// </summary>
        /// <param name="_start_pos"></param>
        /// <param name="_dir"></param>
        /// <param name="_color"></param>
        /// <param name="_tags"></param>
        public void SetVectorNotification(Vector3 _start_pos,Vector3 _dir,Color _color,params string[] _tags)
        {

        }





        #endregion
    }
}
