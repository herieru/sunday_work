﻿///<summary>
/// 概要：条件部分の設定を行うためのもの
/// そのインターフェース。
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IGunbitTurmsBase
{

    public TurmsInfo info;
    /// <summary>
    /// 条件が記述され、その中で、条件を満たしたら、trueが入ってくる
    /// </summary>
    /// <returns></returns>
    public abstract bool isAchieveTurm();
}

/// <summary>
/// 情報を取得用の入れ物
/// </summary>
public class TurmsInfo
{
    public GameObject player { get; set; }
    public GameObject myself { get; set; }
    public StatusBase player_status { get; set; }
}


