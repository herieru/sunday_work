﻿///<summary>
/// 概要：ガンビットのHPの条件によって分岐を返すためのもの
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class BranchesHitpoint : IGunbitTurmsBase
{


    public override bool isAchieveTurm()
    {
        return false;
    }

    
}


