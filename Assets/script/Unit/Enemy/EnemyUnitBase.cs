﻿///<summary>
/// 概要:敵ユニット系のベースクラス
/// 全ての敵ユニット系のクラスは全てここを継承を行う。
/// 生成を行う部分は、別途別のクラスが管理を行う。
/// 
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnitBase : UnitBase
{
    //Comment:ScriptableObjectか何かで一括で管理したステータスの元
    //出現させてもいいかもしれない。

    /// <summary>
    /// 対象となる相手のGameObject
    /// これを基準に、色々行う
    /// </summary>
    protected GameObject Target;


    /// <summary>
    /// 前回の移動方角
    /// </summary>
    protected Vector3 prev_dir;

    
    //Comment：この部分はあらかじめついていない形で
    public EnemyUnitBase(EnemyStatusBase _status)
    {
        status = _status;
    }

    public void Awake()
    {
        StatusBase _base_status = new StatusBase(55, 2, StatusBase.UnitType.Enemy);
        status = new EnemyStatusBase(5,_base_status);
    }

    public void Update()
    {
        Action();
        
    }


    /// <summary>
    /// 色々な作成の元に行動が決定される。
    /// </summary>
    public virtual void  Action()
    {
        if(null == Target)
        {
            return;
        }
        Vector3 _pos = this.transform.position;
        Vector3 _dir = Target.transform.position - _pos;
        _dir.Normalize();

        _dir = Calclator.InterPolationVector3.RestrictRadianPolation(prev_dir, _dir, 0.9f,1.0f);

        HackDebug.Drawer.DrawArrow(_pos, _dir * 10, Color.red);
        prev_dir = _dir;


        //5fは適当な数字 でも今の所それなりに適度にいい感じ
        _pos = _pos + _dir * 5f * Time.deltaTime;

        this.transform.position = _pos;

       
    }

    /// <summary>
    /// ターゲット対象のオブジェクトの情報を渡す。
    /// FIX:interfaceにした方がいい？
    /// </summary>
    /// <param name="_target_object">対象のGameObject</param>
    public void SetTarget(GameObject _target_object)
    {
        Target = _target_object;

        Vector3 _pos = this.transform.position;
        prev_dir = Target.transform.position - _pos;
        prev_dir.Normalize();

    }

    /// <summary>
    /// 当たった際に呼び出される。
    /// Hｐ計算を行って、0になったら、UnitManagerに通知して、消す。
    /// </summary>
    /// <param name="_status"></param>
    /// <returns>false == 倒し切れていない　true == 倒しきれた。</returns>
    public override bool  HitAttack(StatusBase _status)
    {
        getstatus.SetDamaged(_status.getsum_atk_point);
        //TODO：ダメージ表記とかを出すようにしてもいいかもしれない。
        //どこかに対して、通知を投げる？


        if(status.isLife)
        {
            var _player_pos = UnitManagerSystem.Instance.Player.transform.position;
            Vector3 _dir = this.transform.position - _player_pos;
            this.transform.position = this.transform.position + _dir / 4;

            return false; 
        }

        //TODO:ここでHP計算とかを行う。
        UnitManagerSystem.Instance.DeadEnemy(this.gameObject);

        return true;
    }
}
