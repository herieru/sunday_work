﻿///<summary>
/// 概要：敵専用のステータスのもの
///
/// <filename>
/// EnemyStatusBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class EnemyStatusBase : StatusBase
{

    private int exp;

    /// <summary>
    /// ステータスの基本
    /// </summary>
    /// <param name="_exp">倒した時に得られる獲得経験値　でも引数で持たせるのは適切でないかも</param>
    /// <param name="_base"></param>
    public EnemyStatusBase(int _exp,StatusBase _base) : base(_base.hitpoint,_base.attackpoint,_base.type)
    {
        exp = _exp;
    }

    /// <summary>
    /// 攻撃力の合計を返すためのもの
    /// </summary>
    public override int getsum_atk_point
    {
        get
        {
            return base.getsum_atk_point;
        }
    }

    /// <summary>
    /// 獲得経験値
    /// </summary>
    /// <returns>このキャラクターが所有している経験値</returns>
    public virtual int GetExp()
    {
        return exp;
    }
}
