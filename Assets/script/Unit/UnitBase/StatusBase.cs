﻿///<summary>
/// 概要：ステータスのベースクラス。
/// 基本的な要素として、情報を保持する用
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusBase
{
    /// <summary>
    /// 基本的なステータスの値を受け取り、保持を行う。
    /// </summary>
    /// <param name="_hp"></param>
    /// <param name="_attack_point"></param>
    public StatusBase(int _hp, int _attack_point, UnitType _type)
    {
        hitpoint = _hp;
        attackpoint = _attack_point;
        type = _type;
    }
    #region Enum
    /// <summary>
    /// ユニットのタイプを示す
    /// </summary>
    public enum UnitType
    {
        Player,         //プレイヤー
        Enemy,          //敵
        Weapon,         //完全に武器　武器同士は当たり判定を持たない
        EnemyBullet,    //敵の弾
        PlayerBullet,   //プレイヤー側の弾
        Another,        //上記以外
    }

    #endregion



    /// <summary>
    /// 基本的なステータス　HP　と　攻撃力
    /// </summary>
    public int hitpoint { get; private set; }
    public int attackpoint { get; private set; }
    public UnitType type { get; private set; }

    public bool isLife
    {
        get
        {
            //弾丸などのhpが不要なユニットの場合を想定したもの。
            return (type == UnitType.Another || hitpoint > 0);
        }
    }

    /// <summary>
    /// 継承先では、場合によっては、武器の攻撃力を取得した上で
    /// 攻撃力を追加下ものを返却する。
    /// </summary>
    public virtual int getsum_atk_point
    {
        get
        {
            return attackpoint;
        }
    }

    /// <summary>
    /// ダメージ計算用の
    /// </summary>
    public void SetDamaged(int _damage)
    {
        hitpoint -= _damage;
        if(hitpoint <= 0)
        {
            hitpoint = 0;
        }
    }


}
