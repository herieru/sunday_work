﻿///<summary>
/// 概要：全てのユニットのベースクラス
/// Factoryクラス部分で作成された際に、GameObjectに対して
/// つけられる。
/// 
/// 
/// TODO：ユニット系の全ての制御が完了したら
/// どんどん書いていく。
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitBase : MonoBehaviour,HitInterface
{
    #region フィールド
    protected StatusBase status;

    #region
    public StatusBase getstatus { get { return status; } }
    #endregion


    #endregion

    public UnitBase()
    {
        var _status = new StatusBase(10, 10, StatusBase.UnitType.Another);
    }


    public UnitBase(StatusBase _status)
    {
        status = _status;
    }


    /// <summary>
    /// 基本的には、この関数で処理をするが、追加で行いたい場合は
    /// overrideを行う。
    /// </summary>
    /// <param name="_atk_status">与えてきた相手のステータス</param>
    /// <returns></returns>
    public virtual bool HitAttack(StatusBase _atk_status)
    {
        if(false == status.isLife)
        {
            return false;
        }

        status.SetDamaged(_atk_status.attackpoint);

        return true;
    }
}
