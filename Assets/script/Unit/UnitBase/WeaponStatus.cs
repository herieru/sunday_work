﻿///<summary>
/// 概要：武器系のステータスの部分のクラス
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStatus : StatusBase
{

    private float propagation_weight_power = 3.0f;

    /// <summary>
    /// 武器の重さ
    /// </summary>
    public float weight;

    /// <summary>
    /// 武器を取得してくる
    /// </summary>
    /// <param name="_hp"></param>
    /// <param name="_attack_point"></param>
    /// <param name="weight"></param>
    /// <param name="_type"></param>
    public WeaponStatus(int _hp, int _attack_point,float _weight, UnitType _type) 
        : base(_hp, _attack_point, _type)
    {
        weight = _weight;
    }

    /// <summary>
    /// 武器の重さを考慮した、攻撃力を返す
    /// 武器の攻撃力と、重さを考慮した攻撃力
    /// </summary>
    public override int getsum_atk_point
    {
        get
        {
            return Mathf.FloorToInt((float)attackpoint * (weight / propagation_weight_power));
        }
    }
}
