﻿///<summary>
/// 概要：攻撃などが当たった際に、色々と行うための関数を持たせるやつ。
/// 当たった対象に対して呼び出すのもありだし、その他でもあり。
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface HitInterface
{
    //ステータスを送り込むように行う。
    bool HitAttack(StatusBase _atk_status);
}
