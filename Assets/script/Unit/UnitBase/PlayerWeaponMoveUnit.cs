﻿///<summary>
/// 概要：プレイヤーの武器の動き部分の基底クラスに当たるもの
/// ここで、外部から生成されて、外部から関数呼び出しなどをされて、
/// それを使用して、動きを実現を行う。
/// 
///　武器の重さなどによって状態を変化させて、遅くなる速度が軽減されたりする。
///　一定値まで、減速が行われ、その速度に合わせて回転を行う。
/// 
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponMoveUnit
{
    #region　読み込み定数定義

    /// <summary>
    /// 最大の重さとした時の減速のための計算
    /// </summary>
    private const float MaxWeight = 20.0f;
    private const float MinmamLoapLength = 1f;
    private const float MaxmamLoapLength = 10.0f;
    private const float MaxSpeed = 60.0f;
    /// <summary>
    /// 1フレームで回る最高角度　ここはかなりゲームのプレイに影響を及ぼすものなので
    /// TODO:再検討
    /// </summary>
    private const float MaxRollingRad = 120.0f;
    #endregion

    #region 初期化可能読み込み定数
    private readonly float minimam_speed;
    private readonly WeaponStatus weaponStatus;
    private readonly float max_speed;
    #endregion

    #region フィールド
    /// <summary>
    /// 動作をしていいかの有無
    /// </summary>
    public enum MoveState
    {
        GoMoving,   //Speedのある限り回す
        NutoralMove,//地味に動く　内部で、止められている時以外は、緩やかに動く
        Stop,       //更新がきても、止まっているだけ
    }

    /// <summary>
    /// 状態変化
    /// </summary>
    private MoveState moveState;
    /// <summary>
    /// ロープの長さ　　
    /// TODO:ここが持っていたらおかしい。
    /// 最低限参照に切り替える必要がある。
    /// 最終的に、ここの上の階層が持つ
    /// </summary>
    private float loap_length;

    public float LoapLength
    {
        get { return loap_length; }
    }


    //今回移動した角度
    private float now_flame_rad = 0.0f;


    public bool isAcuteAngle
    {
        get { return now_flame_rad <= 180; }
    }



    /// <summary>
    /// 移動スピード
    /// </summary>
    private float move_speed;
    /// <summary>
    /// 親となるゲームオブジェクト
    /// </summary>
    private GameObject player_object;

    /// <summary>
    /// 前回の場所
    /// </summary>
    private Vector3 prev_pos;

    /// <summary>
    /// 最新の場所
    /// </summary>
    private Vector3 now_pos;

    /// <summary>
    /// 前と今の中間地点のベクトルを返す。
    /// </summary>
    public Vector3 BetweenDir
    {
        get
        {
            Vector3 _now_dir = now_pos - player_object.transform.position;
            Vector3 _prev_dir = prev_pos - player_object.transform.position;
            HackDebug.Drawer.DrawLineRay(now_pos, Vector3.up * 100.0f, Color.yellow);
            HackDebug.Drawer.DrawLineRay(prev_pos, Vector3.up * 100.0f, Color.blue);

            

            HackDebug.Drawer.DrawLineRay(player_object.transform.position, _now_dir, Color.green);
            HackDebug.Drawer.DrawLineRay(player_object.transform.position, _prev_dir, Color.cyan);

            return ((_now_dir + _prev_dir) / 2).normalized;
        }
    }

    /// <summary>
    /// 半分の角度を調べる
    /// </summary>
    public float MovedRad
    {
        get
        {
            
            Vector3 _now_dir = now_pos - player_object.transform.position;
            Vector3 _prev_dir = prev_pos - player_object.transform.position;
            float _rad = Vector3.Angle(_prev_dir, _now_dir);
            
            return _rad;
         }
    }
    

    

    //素材(マテリアル)

    #endregion

    #region public method
    /// <summary>
    /// 武器のステータスと、最小の走りの状態を受け取る。
    /// 武器につけるサブなどはこれを呼び出す前に計算されて渡されている状態。
    /// </summary>
    /// <param name="_player_object">プレイヤーとなる本体のオブジェクト</param>
    /// <param name="_status">武器の基本ステータス</param>
    /// <param name="_minimum">最低保障のスピード</param>
    public PlayerWeaponMoveUnit(GameObject _player_object,WeaponStatus _status,float _minimum_speed = 0.001f)
    {
        move_speed = _minimum_speed;
        moveState = MoveState.NutoralMove;
        player_object = _player_object;
        minimam_speed = _minimum_speed;
        weaponStatus = _status;
        loap_length = MinmamLoapLength   * 5;
        max_speed = MaxSpeed;
    }

    /// <summary>
    /// 移動の状態を切り替えるためのもの　
    /// タッチするとか、離すとかを主軸のもの
    /// </summary>
    /// <param name="_move_state">どういう動かしかたをするか</param>
    /// <param name="_next_player_move_point">次にプレイヤーが行く予定の場所２D->３D座標の値が入る</param>
    public void SwitchState(MoveState _move_state,Vector3 _next_player_move_point)
    {
        if(moveState != _move_state)
        {
            moveState = _move_state;
            if(moveState == MoveState.Stop)
            {
                setting_next_moveing_point_inverse(_next_player_move_point);
            }
        }
    }
    
    /// <summary>
    /// Speedをリセットを行う。
    /// </summary>
    public void ResetSpeed()
    {
        move_speed = minimam_speed;
    }

    /// <summary>
    /// スピードをセットする。動力としての提供
    /// 元で制御されたタイミングで呼び出される。
    /// </summary>
    /// <param name="_speed"></param>
    public virtual void AddSpeed(float _speed)
    {
        move_speed += _speed;
        if(move_speed >= max_speed)
        {
            move_speed = max_speed;
        }
    }

    /// <summary>
    /// 現在の位置から、スピードに合わせて、指定の角度回転を行う。
    /// 基本的には、呼び出し元で制御を行う。
    /// </summary>
    /// <param name="_now_pos">現在のこのボールの位置</param>
    public Vector3 update_moving_object(Vector3 _now_pos)
    {
        prev_pos = _now_pos;
        _now_pos = calclation_next_pos(_now_pos);
        now_pos = _now_pos;
        return _now_pos;
    }


    /// <summary>
    /// 停止している際のオブジェクト位置計算
    /// </summary>
    /// <param name="_now_pos">現在のボールの位置</param>
    /// <param name="_dir">配置してほしい方角</param>
    /// <returns></returns>
    public Vector3 update_stopping_object(Vector3 _now_pos,Vector3 _dir)
    {
        prev_pos = _now_pos;
        _now_pos = player_object.transform.position + (_dir * loap_length);
        now_pos = _now_pos;
        return _now_pos;
    }


    #endregion //end public method


    /// <summary>
    /// 次の位置を求める
    /// <param name="_now_weapon_pos">現在のこの武器の位置</param>
    /// </summary>
    private Vector3 calclation_next_pos(Vector3 _now_weapon_pos)
    {
        //スピードから動かす角度を求めて、角度計算した先を求める。
        //方角だけを抜き取って取得を行う。
        Vector3 _now_pos_dir = _now_weapon_pos - player_object.transform.position;
        float _round_rad = intterpolate_move_radian_angle();
        Vector3 _next_pos = Quaternion.AngleAxis(_round_rad, player_object.transform.up) * (_now_pos_dir.normalized * loap_length);

        _next_pos.y = 0.0f;

        tobe_slow_speed();
        return player_object.transform.position + _next_pos;
    }

    /// <summary>
    /// 現在のスピードから、動く角度を求める。
    /// </summary>
    /// <returns>角度</returns>
    private float intterpolate_move_radian_angle()
    {
        float _rate = Calclator.InterpolationCalculator.RestrictRateBetweenZerotoOne(move_speed,max_speed);
        float _range = MaxRollingRad * _rate;

        return _range;
    }

    /// <summary>
    /// スピードを規定の割合で落としていく。常に更新タイミングとして呼び出す必要あり。
    /// この武器の重さから、減速率を求めるもの。
    /// Change;とりあえず、三角関数＋円形を描く際のY値として、計算を行い、そのYの半分の割合を消失として計算する。
    /// それゆえ早いほど勢いは消失しやすい状態
    /// </summary>
    private void tobe_slow_speed()
    {
        //０－１にして、これをXの値とする。　常に　斜辺は１として計算
        //早いほど、減衰率をあげたい　１－
        float _cheak_value = 1 - (move_speed / max_speed);

        float _weaking_rate_speed = 1 - _cheak_value * _cheak_value;

     

        //最大重さ20の中で、の割合を求める 軽いほど減速率は低くなる。
        _weaking_rate_speed = _weaking_rate_speed  *(weaponStatus.weight / MaxWeight);
   
        //減速される速度
        float _ans_slow_down_speed = (_weaking_rate_speed * max_speed) * Time.deltaTime;

        move_speed -= _ans_slow_down_speed;
        if(make_transition_state()  && moveState != MoveState.Stop)
        {
            moveState = MoveState.NutoralMove;
        }

    }

    /// <summary>
    /// スピードが最低限まで下がってないか？
    /// </summary>
    /// <returns></returns>
    private bool make_transition_state()
    {
        if(move_speed < minimam_speed)
        {
            move_speed = minimam_speed;
            return true;
        }

        return false;
    }


    /// <summary>
    /// 進む方向と逆方向に進むように設定を行う。
    /// </summary>
    /// <param name="_next_player_move_point">タッチされている箇所から座標に治されている座標</param>
    private Vector3 setting_next_moveing_point_inverse(Vector3 _next_player_move_point)
    {
        Vector3 _dir = _next_player_move_point - player_object.transform.position;
        //真逆の位置を取得したいため。
        _dir = Quaternion.AngleAxis(180.0f, Vector3.up) * _dir;
        _dir.y = 0;
        //TODO;ここに、現在の距離に合わせた、長さの位置で調整を行う必要あり。
        //というのも、タッチした場所が、必ずとも釣り合わないため。

        return player_object.transform.position + _dir;
    }

    
}
