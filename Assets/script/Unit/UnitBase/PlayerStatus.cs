﻿///<summary>
/// 概要：プレイヤー自身のステータス
/// ここでは、基本的にデータとかの参照を持っており
/// それらを使って、相手に通知をさせる仕組みがある。
/// 武器自身はこのステータスを持っている所が持つので、ここには持っていない。
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : StatusBase
{
    public StatusBase weapon { get; private set; }

    public PlayerStatus(int _hp, int _attack_point, UnitType _type,StatusBase _weapon)
        :base(_hp,_attack_point,_type)
    {
        weapon = _weapon;
    }

    /// <summary>
    /// プレイヤー自身の攻撃力と、武器の攻撃力を足す
    /// </summary>
    public override int getsum_atk_point
    {
        get
        {
            return attackpoint + weapon.getsum_atk_point;
        }
    }

}
