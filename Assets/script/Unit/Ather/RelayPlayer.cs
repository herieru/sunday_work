﻿///<summary>
/// 概要：プレイヤーユニットに対して、タッチの情報を流すだけのもの
///
/// <filename>
/// RelayPlayer.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using ClickRecive;

public class RelayPlayer : MonoBehaviour,IClickReceive 
{
    private PlayerUnit player_unit;

    // Use this for initialization
    void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{	
        if(null == player_unit)
        {
            player_unit = UnitManagerSystem.Instance.PlayerUnit;
        }
	}

    /// <summary>
    /// タッチ情報を受け取るための機構。
    /// </summary>
    /// <param name="_type">クリックのタイプ</param>
    /// <param name="_touch_info">タッチの情報</param>
    public void ClickRecever(ReciveType _type, TouchFirstProcessInfomation _touch_info)
    {
        Debug.Log("オブジェクトへの通知は来てる。");
        switch (_type)
        {
            case ReciveType.Begin:
                player_unit.BeginProcess(_touch_info);
                break;
            case ReciveType.Drag:
                player_unit.DragProcess(_touch_info);
                break;
            case ReciveType.Ended:
                player_unit.EndProcess(_touch_info);
                break;
        }
    }
}
