﻿///<summary>
/// 概要：MonoBeheivior継承のプレイヤーが使用する武器クラス。
/// プレイヤーの武器のためのクラス
/// プレイヤーの移動距離に応じて、この武器の重さとから、どれくらい回転するかを計算を行い
/// 回転する。
/// このスクリプトは動作する。武器自身のオブジェクトにつけるもの
/// 
/// COMMENT:今後の展開をどうするかはわからないが、
/// イカのように、サブ的な能力UPを付与する事で、その能力の影響を受けた作りに出来るようにしときたい。
///
/// TODO:最低限でもロープの長さ、
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public  class ArroundBallWeapon:WeaponUnitBase
{

    private const int NoReactionId = -1;
    /// <summary>
    /// プレイヤー自身。中心的存在
    /// </summary>
    public GameObject player_object;

    /// <summary>
    /// 動きに関するもの
    /// </summary>
    public PlayerWeaponMoveUnit move_unit = null;

    //更新をしてもよいか？タッチされてー＞PlayerMoveクラスから通知が来た際に、更新が可能になる。
    private bool is_update = true;

    /// <summary>
    /// タッチしている場所と逆位置
    /// </summary>
    private Vector3 inverse_touch_dir = new Vector3();


    /// <summary>
    /// タッチしているID
    /// </summary>
    private int finger_id = NoReactionId;

    public float LoapLength
    {
        get
        {
            return move_unit.LoapLength;
        }
    }




    /// <summary>
    /// TODO：特に不要なので削除する必要あり。
    /// </summary>
    /// <param name="_status">武器のステータス</param>
    /// <param name="_parent">親オブジェクト</param>
    public ArroundBallWeapon(WeaponStatus _status,GameObject _parent) : base(_status)
    {
        player_object = _parent;
        move_unit = new PlayerWeaponMoveUnit(player_object, _status, 0.01f);
    }

    /// <summary>
    /// MonoBehabiorでの動かし方をするので、初期化はこの形式に変更。
    /// </summary>
    /// <param name="_status">武器のステータス</param>
    /// <param name="_parent">親オブジェクト</param>
    public void initalized(WeaponStatus _status, GameObject _parent)
    {
        player_object = _parent;
        move_unit = new PlayerWeaponMoveUnit(player_object, _status, 0.01f);
        is_update =  true;
    }


    /// <summary>
    /// MonoBeheiviorによるUpdate
    /// </summary>
    public override void  Update()
    {
        
        //タッチ状態かどうか？
        if (is_update)
        {
            //TODO:ここはタッチしたときに適応できてないので、修正が必要
            this.transform.position = move_unit.update_moving_object(transform.position);
        }
        else
        {
            //タッチされた場所と逆ベクトル側に位置をおく
            this.transform.position = move_unit.update_stopping_object(transform.position, inverse_touch_dir);
        }
    }


    #region　動きに関するクラスにつなぐための関数


    /// <summary>
    /// ３D空間上に直されたスクリーンからのタッチ座標を受け取って、その位置を取得しにいく。
    /// </summary>
    /// <param name="_finger_id">タッチのID</param>
    /// <param name="_touch_down_point">タッチダウンして、当たったフィールドの対象地点３D地点</param>
    public void SetTouchDown(int _finger_id ,Vector3 _touch_down_point)
    {
        Debug.Log("touchDown");
        if(_finger_id != NoReactionId)
        {
            return;
        }
        finger_id = _finger_id;
        is_update = false;
        //タッチ　－＞　親オブジェクトのベクトル
        inverse_touch_dir = ((player_object.transform.position - _touch_down_point).normalized);
        inverse_touch_dir.y = 0;
        move_unit.ResetSpeed();
    }

    /// <summary>
    /// 継続しているタッチ情報を取得する
    /// </summary>
    /// <param name="_finger_id">タッチのID</param>
    /// <param name="_touch_move_point">今回の地点３D空間上</param>
    public void SetTouchMove(int _finger_id,Vector3 _touch_move_point)
    {
        Debug.Log("touchMove");
        //違うタッチは無効
        if (finger_id != _finger_id)
        {
            return;
        }
        is_update = false;
        //タッチ　－＞　親オブジェクトのベクトル
        inverse_touch_dir = ((player_object.transform.position - _touch_move_point).normalized);
        inverse_touch_dir.y = 0;

    }



    #endregion

    public override void Attack()
    {
        
    }

    /// <summary>
    /// 相手から当たったと反応があった際におこなう事
    /// </summary>
    /// <param name="_atk_status"></param>
    /// <returns></returns>
    public override bool HitAttack(StatusBase _atk_status)
    {
        if(_atk_status.type == StatusBase.UnitType.Enemy
            || _atk_status.type == StatusBase.UnitType.EnemyBullet
            )
        {
            return false;
        }

        ///ここは後で消す
        return base.HitAttack(_atk_status);
    }

    /// <summary>
    /// 移動距離に応じて、移動を行う。
    /// </summary>
    /// <param name="_move_length"></param>
    public void NotificationEndMoveEnd(float _move_length)
    {
        Debug.Log("add notification");
        move_unit.AddSpeed(_move_length);
        is_update = true;
    }
}
