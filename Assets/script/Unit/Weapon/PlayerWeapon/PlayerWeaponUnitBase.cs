﻿///<summary>
/// 概要：プレイヤーの武器クラスの基底クラスに当たる
/// 基本的に、動きとステータスを保持して、それによってタッチの情報を受け取る事で、動きを行ったりする。
///
/// <filename>
/// PlayerWeaponUnitBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using ClickRecive;

//MonoBeheivor継承してる。
public class PlayerWeaponUnitBase : WeaponUnitBase ,IReceiveProcess
{
    //タッチとかがない状態になった際に、これをIDに対して代入する
    private const int NoReactionId = -1;

    #region field

    //タッチされた時に保持するタッチID もしかしたら　複数のタッチのために何か持たせるかも？
    private int use_finger_id;

    //親となるプレイヤーのオブジェクト
    private GameObject player_object;

    /// <summary>
    /// 初期値は適当
    /// </summary>
    private float loap_length  = 5f;

    /// <summary>
    /// タッチ方向と逆方向に対しての向きを保持する。
    /// </summary>
    private Vector3 inverse_touch_dir = new Vector3();

    /// <summary>
    /// 自由に動き回っていい状態かどうか？
    /// タッチ中はfalse
    /// </summary>
    private bool is_free_arround = true;


    private PlayerWeaponMoveUnit weapon_move_unit = null;

    #endregion

    #region propaty

    /// <summary>
    /// 現在のロープの長さを取得してもらう。
    /// </summary>
    public float LoapLength
    {
        get { return weapon_move_unit.LoapLength; }
    }

    /// <summary>
    /// 中間のベクトル
    /// </summary>
    public Vector3 BetweenDir
    {
        get
        {
            return weapon_move_unit.BetweenDir;
        }
    }

    /// <summary>
    /// 中間の角度
    /// </summary>
    public float MoveRad
    {
        get
        {
            return weapon_move_unit.MovedRad;
        }
    }



    #endregion



    /// <summary>
    ///　ステータス等を持って初期化を行う。
    /// </summary>
    /// <param name="_status">基本的なもの</param>
    /// <param name="_player">親となるプレイヤーオブジェクト</param>
    public PlayerWeaponUnitBase(WeaponStatus _status,GameObject _player) : base(_status)
    {
        player_object = _player;
        weapon_move_unit = new PlayerWeaponMoveUnit(player_object, _status);
        is_free_arround = true;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="_status"></param>
    /// <param name="_player"></param>
    public void Initalized(WeaponStatus _status, GameObject _player)
    {
        player_object = _player;
        weapon_move_unit = new PlayerWeaponMoveUnit(player_object, _status);
        is_free_arround = true;

        //FIXME:ステータスの中身等の互換があるかどうか；
        status = _status;
        use_finger_id = NoReactionId;
    }



public override void Update()
    {
        if (is_free_arround)
        {
            //こっちはタッチしていない時の処理
            this.transform.position = weapon_move_unit.update_moving_object(transform.position);
        }
        else
        {
            //タッチされた場所と逆ベクトル側に位置をおく
            this.transform.position = weapon_move_unit.update_stopping_object(transform.position, inverse_touch_dir);

        }
    }

    /// <summary>
    /// タッチの情報をタッチしている位置から求める
    /// </summary>
    /// <param name="_touch_pos"></param>
    /// <returns></returns>
    private Vector3 get_touch_point_object(Vector3 _touch_pos)
    {
        return InputSystemManager.Instance.GetPositionScreenToHitPosition(_touch_pos);
    }

    #region タッチによるイベント挙動

    /// <summary>
    /// タッチしたて  
    /// 本当は、補間関係で結びつけておきたい　そうなるとDragのほうも行わなければいけない。
    /// </summary>
    /// <param name="_info"></param>
    public void BeginProcess(TouchFirstProcessInfomation _info)
    {
        
        if(use_finger_id != NoReactionId)
        {
            //現在すでに使用しているため処理は行わない。
            return;
        }

        //以下は当たっている前提なので、タッチ関係の部分を変更する際は、注意が必要
        use_finger_id = _info.FingerID;
        //スクリーンの位置から、当たっているかどうかを調べて、その位置を特定する。
        Vector3 _hit_pos = get_touch_point_object(_info.touch_info.position);
        is_free_arround = false;

        inverse_touch_dir = (player_object.transform.position - _hit_pos).normalized;
        //高さは不要なので、0
        inverse_touch_dir.y = 0;
        weapon_move_unit.ResetSpeed();
        
    }

    /// <summary>
    /// 引き続きドラッグ状態
    /// </summary>
    /// <param name="_info"></param>
    public void DragProcess(TouchFirstProcessInfomation _info)
    {
        if(use_finger_id !=_info.FingerID)
        {
            return;
        }
        Vector3 _hit_pos = get_touch_point_object(_info.touch_info.position);
        inverse_touch_dir = ((player_object.transform.position - _hit_pos).normalized);
        inverse_touch_dir.y = 0;
    }


    /// <summary>
    /// ここに関しては、別途数値がくるはずなので、関知しない。
    /// 保持しているタッチIDだけを解放する。
    /// </summary>
    /// <param name="_info"></param>
    public void EndProcess(TouchFirstProcessInfomation _info)
    {
        if(_info.FingerID != use_finger_id)
        {
            return;
        }
        use_finger_id = NoReactionId;
        //COMMENT:もしかしたらこっちでも制御する機構は必要かも？
    }

    #endregion


    /// <summary>
    /// プレイヤーの移動が完了した際にくる通知
    /// </summary>
    /// <param name="_move_distance">進んだ距離を通知して返す</param>
    public virtual void NotidicatinMoveEndMethod(float _move_distance)
    {
        if(null == weapon_move_unit)
        {
            return;
        }

        weapon_move_unit.AddSpeed(_move_distance);
        is_free_arround = true;
    }

}
