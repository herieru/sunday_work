﻿///<summary>
/// 概要：武器ユニットの基底クラス。
/// この時点では、敵の武器でも、プレイヤーの武器でもない。
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class WeaponUnitBase : UnitBase
{
    public WeaponUnitBase(WeaponStatus _status)
    {
        status = _status;
    }

    /// <summary>
    /// 親オブジェクト　武器を持っているものが
    /// </summary>
    public GameObject parent { set; private get; }


    /// <summary>
    /// 親との関係性を持つかどうか？
    /// </summary>
    public bool is_indepence;


    /// <summary>
    /// 継承先が持っている攻撃をとる。
    /// </summary>
    public virtual void Attack()
    {

    }

    public virtual void Update()
    {
        
    }

}
