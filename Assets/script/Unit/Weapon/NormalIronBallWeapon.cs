﻿///<summary>
/// 概要：MonoBeheivor継承の武器クラス
/// 通常の
/// こっちは最終的には消す。
/// お試しでやるには意外と便利かもしれない。
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalIronBallWeapon : WeaponUnitBase
{

    [SerializeField]
    GameObject parent;

    WeaponStatus status;
    //TODO:アイテムなど、素材などからステータス更新が必要

    private PlayerWeaponMoveUnit move_unit;

    public void Start()
    {
        status = new WeaponStatus(10, 10, 10, StatusBase.UnitType.Weapon);
        move_unit = new PlayerWeaponMoveUnit(parent, status);
    }

    public void Update()
    {
        transform.position = move_unit.update_moving_object(this.transform.position);


        //これは一時的な機構
        if(Input.GetKeyDown(KeyCode.Space))
        {
            move_unit.AddSpeed(10.0f);
        }
    }

    public NormalIronBallWeapon(WeaponStatus _status) : base(_status)
    {

    }


}


