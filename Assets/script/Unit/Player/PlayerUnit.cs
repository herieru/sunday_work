﻿///<summary>
/// 概要：プレイヤー自身のユニット
/// 現状、プレイヤーのUpdate内で、色々と行っているので色々えぐいことになっている。
/// その為、
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using ClickRecive;
using UnityEngine;


//IReceiveProcessは階層を分けた方がいいかもしれない。
public class PlayerUnit : UnitBase,IReceiveProcess
{

    //これはひとまず仮での実装　実際のPrefabは、データ情報から取得を行う予定。
    [SerializeField]
    private GameObject weapon_prefab;

    /// <summary>
    /// このキャラクターが、持っている武器
    /// </summary>
    private PlayerWeaponUnitBase weapon;

    private PlayerMoveBase move_base;

    public void Awake()
    {
        //Playerのオブジェクト情報を投げる。
        UnitManagerSystem.Instance.Player = this.gameObject;
    }


    public void Start()
    {
         //TODO:セーブデータなどの情報から、生成するものを生成する。
        //ひとまず仮で作成を行う
        var _weapon_status = new WeaponStatus(1, 5, 3, StatusBase.UnitType.Weapon);
        status = new PlayerStatus(10, 5, StatusBase.UnitType.Player, _weapon_status);

        GameObject _weapon_object = GameObject.Instantiate(weapon_prefab, this.transform);
        //ここもひとまず仮 どのクラスを生成するかはデータによって決定される
        distance_create_weapon(_weapon_status,this.gameObject,_weapon_object);
       

        move_base = new PlayerMoveBase();

        move_base.SetWeaponNotifications(weapon.NotidicatinMoveEndMethod);
        
    }

    public PlayerUnit()
    {
       
    }

    /// <summary>
    /// MonoBeheivorのUpdate
    /// </summary>
    public void Update()
    {
        Vector3 _next_pos;
        
        //何かしらのタッチ中
        if (false == InputSystemManager.Instance.IsExistTouchInfo)
        { 
            /////移動処理
            if (move_base.in_update(out _next_pos, transform.position))
            {
                transform.position = _next_pos;
            }
        }
    }

    /// <summary>
    /// 当たり判定を行う。
    /// </summary>
    public void LateUpdate()
    {
        ///ボールとのベクトルを前から現在にかけてのベクトルをとって、
        ///その範囲内にいるか？　距離－＞ベクトルの方が計算的にはやさしそう。

        //処理出来ない。する必要がない際は何もしない。
        if(false == UnitManagerSystem.ExistInstance 
            && UnitManagerSystem.Instance.PopEnemyCount == 0)
        {
            return;
        }
        ///ひとまずここは仮
        var _weapon = weapon;

        if (null == _weapon)
        {
            return;
        }

        float _loap_length = _weapon.LoapLength;

        List<GameObject> _enemy_object = UnitManagerSystem.Instance.EnemyUnitList;


        float _rad = weapon.MoveRad;
        Vector3 _between_dir = weapon.BetweenDir;
        HackDebug.Drawer.DrawLineRay(transform.position, _between_dir, Color.magenta);
        foreach(var _enemy in _enemy_object)
        {
            Vector3 _enemy_pos = _enemy.transform.position;
            Vector3 _enemy_dir = (_enemy_pos - this.transform.position);
            //距離を測って、武器が持っている最長距離より遠かったらそれは当たらないとする。
            float _distance = _enemy_dir.magnitude;

            
            //武器の範囲内。
            if (_distance > _loap_length)
            {
                Debug.LogWarning("ろーぷの長さをみたさなかった");
                continue;
            }
            //IDEA：特定の角度以上進んでいるのなら、もはや範囲内なら全部当たりでも良いかも？
            float _enemy_rad = Vector3.Angle(_between_dir, _enemy_dir);
            if(_enemy_rad >= _rad)
            {
                Debug.LogWarning("角度で死んだ");
                continue;
            }

            var _enemy_hit = _enemy.GetComponent<HitInterface>();
            _enemy_hit.HitAttack(status);

            Debug.LogWarning("Name:" + _enemy.name + "が当たった");
        }
        

    }


    /// <summary>
    /// 生成を行う。武器情報を元に、生成を行い値をセットする
    /// </summary>
    /// <param name="_status">武器のステータス</param>
    /// <param name="_add_component_object">Weaponクラスをつけるオブジェクト</param>
    /// <param name="_set_parent">親となるオブジェクト</param>
    /// <returns>生成した</returns>
    private void distance_create_weapon(WeaponStatus _status,GameObject _set_parent,GameObject _add_component_object)
    {
        //TODO:ここに、読み込みに必要なデータを引っ張ってきて、どうこうするもの
        var _weapon_base = _add_component_object.AddComponent<PlayerWeaponUnitBase>();


        ///すべてに対して、共通の関数として呼びたいので、これは必要。
        _weapon_base.Initalized(_status, _set_parent);

        weapon = _weapon_base;
    }
    
    /// <summary>
    /// タッチし始めの工程
    /// </summary>
    /// <param name="_info"></param>
    public void BeginProcess(TouchFirstProcessInfomation _info)
    {
        //Debug.Log("beginがきた");
        if (_info.touch_info.phase == TouchPhase.Began)
        {
            weapon.BeginProcess(_info);
        }
    }

    /// <summary>
    /// タッチ後に、移動している工程
    /// </summary>
    /// <param name="_info"></param>
    public void DragProcess(TouchFirstProcessInfomation _info)
    {
        //TOOD：以下は、通知を持っているオブジェクトに対して、通知をしていくだけ
        
        if (_info.touch_info.phase == TouchPhase.Moved)
        {
            weapon.DragProcess(_info);            
        }
    }

    /// <summary>
    /// タッチを辞めた工程
    /// </summary>
    /// <param name="_info"></param>
    public void EndProcess(TouchFirstProcessInfomation _info)
    {
        //TOOD:それぞれの持っているユニットに対して、通知を行っていくだけ。
        //Debug.Log("endがきた");
        if (_info.touch_info.phase == TouchPhase.Ended)
        {
           Vector3 _touch_pos = _info.touch_info.position;
           _touch_pos = InputSystemManager.Instance.GetPositionScreenToHitPosition(_touch_pos);
           //_touch_poss.y = 0;//ここは何かしら変えるかも？
           //this.transform.position = _touch_poss;
            move_base.SetReleasePos(_info.FingerID, _touch_pos, transform.position);
            weapon.EndProcess(_info);
        }
    }
}
