﻿///<summary>
/// 概要：プレイヤーの動きに関する規定クラスのもの
///
/// <filename>
/// PlayerMoveBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveBase
{
    /// <summary>
    /// タッチしていない状態を表す。
    /// </summary>
    protected readonly int NoReactionId = -1;


    /// <summary>
    /// 動く必要があるか？
    /// 移動先が設定された際に、まだその地点まで移動しきっていない時に
    /// trueになる。
    /// </summary>
    private bool is_need_movement;


    /// <summary>
    /// 移動方向
    /// </summary>
    private Vector3 move_dir = Vector3.zero;

    /// <summary>
    /// 0-1で変化をさせる
    /// </summary>
    private float move_rate;

    /// <summary>
    /// 現状反応を示しているID
    /// </summary>
    private int indicate_reaction_finger_id;
    #region property
    /// <summary>
    /// 更新をかける必要があるか？
    /// </summary>
    public bool IsNeedUpdate
    {
        get
        {
            return is_need_movement;
        }
    }
    #endregion


    public PlayerMoveBase()
    {
        indicate_reaction_finger_id = NoReactionId;
    }

    /// <summary>
    /// これを保持している所から更新をかけてください。
    /// </summary>
    public virtual Vector3 in_update(Vector3 _now_pos)
    {
        return Vector3.zero;
    }

    //合計値を足す。
    private float sum = 0.0f;

    System.Action<float> end_notification;

    public void SetWeaponNotifications(System.Action<float> _notification)
    {
        end_notification = _notification;
    }

    /// <summary>
    /// 更新処理をした際に、その更新が必要かどうかまで判断を返して
    /// out部分にその結果を返す
    /// あくまでも更新としての処理を行う為の関数
    /// </summary>
    /// <param name="_next_pos"></param>
    /// <param name="_now_pos"></param>
    /// <returns>false = 更新してはだめ</returns>
    public virtual bool in_update(out Vector3 _next_pos, Vector3 _now_pos)
    {
        _next_pos = Vector3.zero;
        if (false == IsNeedUpdate)
        {
            return false;
        }
        Debug.Log("in_update");

        //TODO：移動計算処理
        float _rate = Calclator.InterpolationCalculator.InisialFsymbolMovepolation(move_rate);
        _next_pos = _now_pos + move_dir * _rate;
        
        //HACK:現在加算式に、移動量が決まってしまっているので、合計が次の地点までの量になってない、
        HackDebug.Drawer.DrawArrow(_now_pos, move_dir * _rate, Color.red);
       // UnityEditor.EditorApplication.isPaused = true;
        if(sum >= 1.0f)
        {
            is_need_movement = false;
            indicate_reaction_finger_id = NoReactionId;
            if(null != end_notification)
            {
                HackDebug.Console.Log("Length:" + move_dir.magnitude);
                end_notification(move_dir.magnitude);
            }
            
            return false;
        }
        move_rate +=  Time.deltaTime;
        sum += _rate;
        
        return true;
    }


    

    /// <summary>
    /// interfaceにする場合はほしい情報を登録できる仕組みを作る必要がある。
    /// タッチの状態を更新を行う。
    /// </summary>
    /// <param name="_finger_id">タッチのID</param>
    /// <param name="_touch_pos">タッチした場所（３D空間上）</param>
    public virtual void SetTouchUpdate(int _finger_id,Vector3 _touch_pos)
    {


        if(indicate_reaction_finger_id == NoReactionId)
        {
            _finger_id = _finger_id;
        }
        //すでにIDが入っていて別の情報がやってきたら弾く
        else if(indicate_reaction_finger_id != _finger_id)
        {
            return;
        }

    }


    /// <summary>
    /// タッチした指を離した場所を取得する。
    /// タッチの制御はここでは行わない。
    /// </summary>
    /// <param name="_finger_id">指のID</param>
    /// <param name="_release_touch_pos">離した場所</param>
    /// <param name="_now_pos">現在のオブジェクトの位置</param>
    public virtual void SetReleasePos(int _finger_id, Vector3 _release_touch_pos, Vector3 _now_pos)
    {
        if (_finger_id == indicate_reaction_finger_id)
        {
            Debug.Log("MoveParts No Reaction");
            return;
        }


        indicate_reaction_finger_id = _finger_id;
        move_dir = _release_touch_pos - _now_pos;
        HackDebug.Drawer.DrawArrow(_now_pos, move_dir, Color.yellow);
        
        is_need_movement = true;
        sum = 0;
        move_rate = 0;
    }
}
