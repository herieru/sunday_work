﻿///<summary>
/// 概要：プレイヤーが呼び出す為のモノ
/// プレイヤーが更新等を行い
/// それに対して結果を返す形での処理を行う。
/// <filename>
/// PlayerMoveParts.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveParts
{
    private readonly int NoReactionId = -1;


    /// <summary>
    /// 動く必要があるか？
    /// 移動先が設定された際に、まだその地点まで移動しきっていない時に
    /// trueになる。
    /// </summary>
    private bool is_need_movement;

    /// <summary>
    /// 移動方向
    /// </summary>
    private Vector3 move_dir = Vector3.zero;

    /// <summary>
    /// 0-1で変化をさせる
    /// </summary>
    private float move_rate;

    /// <summary>
    /// 現状反応を示しているID
    /// </summary>
    private int indicate_reaction_finger_id;
    #region property
    /// <summary>
    /// 更新をかける必要があるか？
    /// </summary>
    public bool IsNeedUpdate
    {
        get
        {
            return is_need_movement;
        }
    }
    #endregion


    public PlayerMoveParts()
    {
        indicate_reaction_finger_id = NoReactionId;
    }

    /// <summary>
    /// これを保持している所から更新をかけてください。
    /// </summary>
    public Vector3 in_update(Vector3 _now_pos)
    {
        return Vector3.zero;
    }

    /// <summary>
    /// 更新処理をした際に、その更新が必要かどうかまで判断を返して
    /// out部分にその結果を返す
    /// </summary>
    /// <param name="_next_pos"></param>
    /// <param name="_now_pos"></param>
    /// <returns>false = 更新してはだめ</returns>
    public bool in_update(out Vector3 _next_pos,Vector3 _now_pos)
    {
        _next_pos = Vector3.zero;
        if(false == IsNeedUpdate)
        {
            return false;
        }

        //TODO：移動計算処理
        float _rate = Calclator.InterpolationCalculator.InisialFsymbolMovepolation(move_rate);
        _next_pos = _now_pos + move_dir * _rate;

        return true;
    }

    /// <summary>
    /// タッチした指を離した場所を取得する。
    /// タッチの制御はここでは行わない。
    /// </summary>
    /// <param name="_finger_id">指のID</param>
    /// <param name="_release_touch_pos">離した場所</param>
    /// <param name="_now_pos">現在のオブジェクトの位置</param>
    public void SetReleasePos(int _finger_id,Vector3 _release_touch_pos,Vector3 _now_pos)
    {
        if(_finger_id == indicate_reaction_finger_id)
        {
            Debug.Log("MoveParts No Reaction");
            return;
        }


        indicate_reaction_finger_id = _finger_id;
        move_dir = _release_touch_pos - _now_pos;
        is_need_movement = true;
    }

}
