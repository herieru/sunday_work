﻿///<summary>
/// 概要：タッチなどを受け取る部分のinterface
/// ひとまず、以下の内容での実装。
///
/// <filename>
/// IClickReceive.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

namespace ClickRecive
{
    public enum ReciveType
    {
        None,       //何も反応しない
        Begin,
        Drag,
        Ended,
    }

    /// <summary>
    /// タッチ情報を受け取るために、
    /// ・オブジェクトー＞UIManagerに流す仕組みと
    /// ・UIManagerチェックー＞処理したい部分を受け取る箇所を取得する必要がある。
    /// </summary>
    interface IClickReceive
    {
        /// <summary>
        /// タッチ情報を受け取るためのもの
        /// </summary>
        /// <param name="_type"></param>
        /// <param name="_touch_info"></param>
        void ClickRecever(ReciveType _type, TouchFirstProcessInfomation _touch_info);
    }

    interface IReceiveProcess
    {
        void BeginProcess(TouchFirstProcessInfomation _info);

        void DragProcess(TouchFirstProcessInfomation _info);

        void EndProcess(TouchFirstProcessInfomation _info);

    }

}
