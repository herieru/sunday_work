﻿///<summary>
/// 概要：UI版のクリックしたら反応を示す為のベースクラス。
/// ここはあくまでも、UIManagerに対して反応が来たというのを投げる為のクラス。
/// 
/// <filename>
/// ClickEventUIBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class ClickEventUIBase : MonoBehaviour , IPointerClickHandler
{
    public virtual void OnPointerClick(PointerEventData eventData)
    {
        //UIManagerに対して情報を投げる。
        UIManager.Instance.SetReactionGameObject(eventData.pointerId,eventData.pointerPress);
    }
}
