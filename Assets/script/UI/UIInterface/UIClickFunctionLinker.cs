﻿///<summary>
/// 概要：UIの反応を取得するためのものが、設定された際に一緒に設定しなければいけない。
/// やることとしては、
/// UIManagerに対して、このコンポーネントがついているオブジェクトの登録を行う。
/// 
///
/// <filename>
/// UIClickFunctionLinker.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;


public class UIClickFunctionLinker : MonoBehaviour 
{

    private bool is_function_ok = false;
	// Use this for initialization
	void Start () 
	{
        is_function_ok = UIManager.Instance.SetNotificationGameObject(this.gameObject);
	}

    private void Update()
    {
        if(is_function_ok)
        {
            return;
        }
        UIManager.Instance.SetNotificationGameObject(this.gameObject);
        is_function_ok = true;
        
    }
}
