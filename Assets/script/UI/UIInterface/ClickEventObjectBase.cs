﻿///<summary>
/// 概要：抑えた、移動した、放したといった一連の情報をUIManagerに対して
/// 情報を投げる為のモノです。
/// 取得する対象のGameObjectに対してコンポーネントとして付けます。
/// 
/// もしかしたら、３D用と２D用を分ける必要があるかもしれない。
/// その時はRectTransformが取れるか否かとかで確かめる？
///
/// <filename>
/// ClickEventObjectBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class ClickEventObjectBase : MonoBehaviour , IPointerClickHandler,IPointerDownHandler, IDragHandler
{

    public void OnPointerDown(PointerEventData _eventData)
    {
        Debug.Log("down_event");
#if UNITY_EDITOR
        _eventData.pointerId = 0;
#endif
        UIManager.Instance.SetReactionGameObject(_eventData.pointerId, _eventData.pointerPressRaycast.gameObject);
    }


    public void OnDrag(PointerEventData _eventData)
    {
        Debug.Log("drag_event");
#if UNITY_EDITOR
        _eventData.pointerId = 0;
#endif
        UIManager.Instance.SetReactionGameObject(_eventData.pointerId, _eventData.pointerDrag);

    }

    public void OnPointerClick(PointerEventData _eventData)
    {
        Debug.Log("click_event");
#if UNITY_EDITOR
        _eventData.pointerId = 0;
#endif
        UIManager.Instance.SetReactionGameObject(_eventData.pointerId, _eventData.pointerEnter);

    }
}
