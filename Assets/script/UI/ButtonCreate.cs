﻿


using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonCreate :MonoBehaviour{

    private Button button = null;
    private bool is_visual;
    
    //ボタンを生成して、押すとそのボタンが表示されなくなるクラス。
    public ButtonCreate()
    {
       
    }

    public void Awake()
    {
        is_visual = true;
    }


    void Update()
    {
        
    }

    void OnGUI()
    {
        if (is_visual)
        {
            if (GUI.Button(new Rect(10, 10, 50, 50), "Buton1"))
            {
                Debug.Log("aaaa");
                is_visual = !is_visual;
            }
        }
        else
        {
            if (GUI.Button(new Rect(50, 50, 50, 50), "Button2"))
            {
                Debug.Log("bbbbb");
                is_visual = !is_visual;
            }
        }
    }

    public void ButtonClickEvent()
    {
        Debug.Log("button_click");
    }
    
}
