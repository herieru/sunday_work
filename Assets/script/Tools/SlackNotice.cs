﻿///<summary>
/// 概要：スラックにアプリケーションから通知を行うようにする。
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SlackNotice : MonoBehaviour {

    string slack_url = "https://hooks.slack.com/services/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";

    void Awake()
    {

    }

    public class PostData
    {
        public string Text;
        public string UserName;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            PostText("通知テスト");
        }
    }

    public void PostText(string _text)
    {
        PostData data = new PostData();
        data.Text = _text;
        data.UserName = "incoming-webhook";
        string json_data = JsonUtility.ToJson(data);
        StartCoroutine(Post(slack_url, json_data));
    }


    IEnumerator Post(string url, string _json_data)
    {

        Dictionary<string, string> header = new Dictionary<string, string>();
        // jsonでリクエストを送るのへッダ例
        header.Add("Content-Type","application/json;charset=UTF-8");

        byte[] postBytes = Encoding.UTF8.GetBytes(_json_data);

        // 送信開始
        WWW www = new WWW(url, postBytes,header);
        yield return www;

        // 成功
        if (www.error == null)
        {
            Debug.Log("Post Success");
        }
        // 失敗
        else
        {
            Debug.Log("Post Failure");
            Debug.Log("ErrorLog：" + www.error);
        }
    }
}
