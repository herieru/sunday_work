﻿///<summary>
/// 概要：補間とかの計算を行う為の計算機
/// 基本的には、一つのパラメーターに対して補間を行うもの
/// 色々なものに対して補間を行えたらいいなとかは思っている。
///
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 計算機系をまとめた名前空間
/// </summary>
namespace Calclator
{
    //補間系統をまとめた計算機
    public class InterpolationCalculator
    {
        /// <summary>
        /// 1次関数で補間を行う。
        /// </summary>
        /// <param name="_set_value">補間を行う為の数値</param>
        /// <param name="_rate">０－１の間でセットされる</param>
        /// <returns></returns>
        public static float LinnearCalc(float _set_value, float _rate)
        {

            _rate = RestrictRate(_rate);
            _set_value = _set_value* _rate;

            return _set_value;
        }

        /// <summary>
        /// 2次関数で補間を行う
        /// </summary>
        /// <param name="_set_value"></param>
        /// <param name="_rate"></param>
        /// <returns></returns>
        public static float QuadraticCalclation(float _set_value, float _rate)
        {
            _rate = RestrictRate(_rate);
            _rate = _rate* _rate;
            _set_value = _set_value* _rate;

            return _set_value;
        }


        /// <summary>
        /// 上限下限を制限するためのもの　０－１に制限するもの
        /// </summary>
        /// <param name="_rate"></param>
        /// <returns></returns>
        private static float RestrictRate(float _rate)
        {
            //上限下限設定
            _rate = Mathf.Max(0.0f, _rate);
            _rate = Mathf.Min(1.0f, _rate);
            return _rate;
        }


        /// <summary>
        /// 調査対象の値が、最小ー最大の中での割合を0-1に計算を行う為のもの
        /// 調査対象の値も無理やり握り潰す　＋　しか返さない
        /// </summary>
        /// <param name="_survey_value">どんな値でもどんとこい</param>
        /// <param name="_max">最大数値</param>
        /// <returns></returns>
        public static float RestrictRateBetweenZerotoOne(float _survey_value,float _max)
        {
            float _rate = _survey_value / _max;
            _rate = RestrictRate(_rate);

            return _rate;
        }


        /// <summary>
        /// 指定した、範囲での、中で、与えた値を制限する。
        /// </summary>
        /// <param name="_value">制限をかける値</param>
        /// <param name="_min">最低値</param>
        /// <param name="_max">最高値</param>
        /// <returns></returns>
        public static float ChangeBetweenSpecifiedRange(float _value,float _min,float _max)
        {
            float _rate = Mathf.Max(_value, _min);
            _rate = Mathf.Min(_value, _max);

            return _rate;
        }
        

        /// <summary>
        /// 斜めのｆのように移動する補間関数
        /// 0 - 1で返す
        /// </summary>
        /// <param name="_now">現在の値</param>
        /// <returns>0　-　１</returns>
        public static float InisialFsymbolMovepolation(float _now)
        {
            _now  = RestrictRate(_now);

            return _now * _now * (3.0f - 2.0f * _now);
        }

    }//end class


    /// <summary>
    /// ベクトルに関しての補間関数
    /// </summary>
    public class InterPolationVector3
    {

        /// <summary>
        /// 1次関数的に、補間を行うもの
        /// _rateは0-1内に変換されて使用されます
        /// Ex)_rate = 15 -> 1   -10 ->0
        /// </summary>
        /// <param name="_base">_rateの基準となるもの</param>
        /// <param name="_next"></param>
        /// <param name="_rate">０－１でbaseにかける　残りを_nextにかける</param>
        /// <returns></returns>
        public static Vector3 LinnerPolation(Vector3 _base,Vector3 _next,float _rate)
        {
            _rate = Calclator.InterpolationCalculator.ChangeBetweenSpecifiedRange(_rate, 0, 1);

            Vector3 _rtn_vec = _base * _rate + _next * (1 - _rate);
            _rtn_vec.Normalize();

            return _rtn_vec;
        }
        

        /// <summary>
        /// 変化する角度を測ってその範囲内で、回転を行う
        /// 回転角度を求めるのに計算を色々と行うので、頻繁に使用すると重くなるかも？
        /// </summary>
        /// <param name="_base">_rate,limit_radの基準</param>
        /// <param name="_next">次に向かう方角</param>
        /// <param name="_rate">割合</param>
        /// <param name="_limit_rad">最大変化角度</param>
        /// <returns></returns>
        public static Vector3 RestrictRadianPolation(Vector3 _base,Vector3 _next,float _rate,float _limit_rad)
        {
            Vector3 _temp_dir = LinnerPolation(_base, _next, _rate);

            //真逆かどうか？
            float _rad = Vector3.Angle(_base, _temp_dir);
            //指定した角度以内なので問題ない。
            if(_limit_rad > _rad)
            {
                return _temp_dir;
            }

            //ここから右回りから、左回りかを判断した上で、制限の角度をかけた回転を求める

            //回転軸の作成
            Vector3 _up_angle = Vector3.Cross(_base, Vector3.right);

            //直行のベクトルの作成
            Vector3 _right_angle = Vector3.Cross(_base, _up_angle);

            float _right_rad = Vector3.Angle(_right_angle, _temp_dir);

            if (_right_rad > 90.0f)
            {
                //右側
                _temp_dir =  Quaternion.AngleAxis(_limit_rad,_up_angle) * _base;

            }
            else
            {
                //左側
                _temp_dir = Quaternion.AngleAxis(-_limit_rad, _up_angle) * _base;

            }

            return _temp_dir;



        }


    }//end

}
