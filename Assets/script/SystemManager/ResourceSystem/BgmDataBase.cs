﻿///<summary>
/// 概要：BGMのデータをシーンによって読み込みと保存を行うためのもの。
///
/// <filename>
/// BgmDataBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using System;
using SceneRelation;
using System.Collections;

namespace SceneRelation
{
    public class BgmDataBase:IDisposable
    {

        #region resource_path

        /// <summary>
        /// リソースのパス
        /// </summary>Assets/Resources/.mp3
        private string perfect_path = "Audio/Bgm/bgm_maoudamashii_8bit28";
        private string resoucePath =  "Resources/Audio/";
        private string bgmPath = "Bgm/";

        //TODO：ここに読み込みを行う必要があるリソースを全部保持しておく。
        private Dictionary<SceneDefinition, string[]> scene_resource_path = new Dictionary<SceneDefinition, string[]>()
        {
            ///<summary>タイトル</summary>
            {
                SceneDefinition.Title,new string[]
                {
                    "a",
                    "b",
                    "c",
                    "d",
                    "e",
                    "f",
                    "g",
                    "h",
                    "i",
                }
            },
            ///<summary>ゲームメイン</summary>
            {
                SceneDefinition.MainGame,new string[]
                {
                    "bgm_maoudamashii_8bit28.mp3",
                }
            },
            ///<summary>設定</summary>
            {
                SceneDefinition.Setting,new string[]
                {
                    "a",
                    "b",
                    "c",
                    "d",
                    "e",
                    "f",
                    "g",
                    "h",
                    "i",
                }
            },
            ///<summary></summary>
            {
                SceneDefinition.StafRoll,new string[]
                {
                    "a",
                    "b",
                    "c",
                    "d",
                    "e",
                    "f",
                    "g",
                    "h",
                    "i",
                }
            },
            {
                SceneDefinition.ScecretSetting,new string[]
                {
                    "a",
                    "b",
                    "c",
                    "d",
                    "e",
                    "f",
                    "g",
                    "h",
                    "i",
                }
            },

        };
        #endregion

        #region field
        //TODO：BGMか何かを基データ作成したら、作成してくれるソースがほしい
        //private Dictionary

        private bool isLoaded = false;
        private Dictionary<string, AudioClip> dic_audio_resouces;

        #endregion

        /// <summary>
        /// ロードが終わっているかどうか？
        /// </summary>
        public bool isUseOk { get { return isLoaded; } }

        /// <summary>
        /// 指定されたシーンに応じて、必要なリソースを読み込む
        /// </summary>
        /// <param name="_read_scene">選択したシーンでの読み込み</param>
        public BgmDataBase(SceneDefinition _read_scene)
        {
            if(false == scene_resource_path.ContainsKey(_read_scene))
            {
                HackDebug.Console.LogWarning("このシーンでは読み込むBGMがない設定になっています。");
            }

            string[] _resouce_paths = null;
            scene_resource_path.TryGetValue(_read_scene, out _resouce_paths);
            if(null == _resouce_paths)
            {
                HackDebug.Console.LogWarning("確認したはずなのに、Sceneキーが存在しない");
            }
            isLoaded = false;

            resource_load(_resouce_paths);
        }

        /// <summary>
        /// シーンの切り替えによって、リソースも変更を行うためのもの
        /// </summary>
        /// <param name="_scene_define"></param>
        public void ChangeResouces(SceneDefinition _scene_define)
        {

        }

        private void resource_load(string[] _resouce_path)
        {
            
            if(null == dic_audio_resouces)
            {
                //Debug.Log("null");
                dic_audio_resouces = new Dictionary<string, AudioClip>();
            }

            //Debug.Log("いよいよロードが始まるぞ");
            var _paths = Resources.FindObjectsOfTypeAll(typeof(AudioClip));

            for (int _i = 0;_i < _resouce_path.Length;_i++)
            {
                string _path = _resouce_path[_i];
                
                var _bgm_object = Resources.Load(perfect_path) as AudioClip;// + bgmPath + _path);

                if(null == _bgm_object)
                {
                    Debug.Log("そもそも読み込めてない");
                }
                var _bgm_clip = _bgm_object as AudioClip;
                if (null != _bgm_clip)
                {
                    Debug.Log("読み込むの成功慣らせるぞ");
                    dic_audio_resouces.Add(_path, _bgm_clip);
                }
                //TODO：消す
                else
                {
                    Debug.Log("読み込み失敗何かミスっている。");
                }



               // yield return null;
            }
            isLoaded = true;

           // yield break;
        }

        /// <summary>
        /// bgmを取得する。
        /// </summary>
        /// <param name="_bgm_name"></param>
        /// <returns></returns>
        public AudioClip getBGM(string _bgm_name)
        {
            AudioClip _clip = null;
            if(false == dic_audio_resouces.ContainsKey(_bgm_name))
            {
                //return _clip;
                
            }
foreach(var _key in  dic_audio_resouces.Keys)
                {
                    _bgm_name = _key;
                    Debug.Log("key_name:" + _bgm_name);
                    break;
                }

            dic_audio_resouces.TryGetValue(_bgm_name,out _clip);


            return _clip;
        }

        /// <summary>
        /// 破棄は読み込んだ内容に沿って、読み込む。
        /// </summary>
        public void Dispose()
        {
            
        }
    }
}
