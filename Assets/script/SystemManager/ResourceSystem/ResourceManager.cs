﻿///<summary>
/// 概要：リソースに関わるものを管理するデータベース的な役割を持ってるリソースの
/// 参照を持っているもの
/// Singleton？場合によっては何か挟むという形式でも良いかもしれないが、、、
/// って感じ。
/// 
/// 基本的に、ここが、リソース系のモノを色々持っている状態。
///
/// <filename>
/// ResourceManager.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using SceneRelation;

public class ResourceManager : Singleton<ResourceManager> ,IUpdater
{
    #region field
    private BgmDataBase bgm_data_base;

    private AudioSource karino_source;
    AudioClip clip;
    #endregion


    #region IUpdaterMethod
    public void Destroy()
    {
        
    }

    public void Init()
    {
       // karino_source = new AudioSource();
        //TODO：読み込むシーンはひとまず、仮で読み込む
        bgm_data_base = new BgmDataBase(SceneDefinition.MainGame);
        clip = bgm_data_base.getBGM("bgm_maoudamashii_8bit28.mp3");
        //TODO:AudioSourceがnullっている
        
    }
    public void setAudioSource(AudioSource _souce)
    {
        karino_source = _souce;
        karino_source.clip = clip;

        //なぜかならない。
        karino_source.PlayOneShot(clip);
    }

    public void LateUpdateMine()
    {

    }

    public float UnityGUIDraw(Rect _rect)
    {
        return 0;
    }

    public void UpdateMine()
    {

    }

    #endregion
    ///private でbgmとかのデータを取得できるように参照を持っている。
    ///必要な時にそれを操作するところで呼び出して、取得を行う。
    ///

    ///<summary>
    ///bgmのリソースを取得する。
    ///<params = _bgm_resouce_name>bgmのリソース名</params>
    /// </summary>
    public AudioClip GetBgmResourceData(string _bgm_resouce_name)
    {
        var _audio = karino_method();
        if(null == _audio)
        {
            HackDebug.Console.LogWarning(_bgm_resouce_name + "の名前のリソースは存在しません");
        }
        return _audio;
    }


    /// <summary>
    /// SEのリソースを取得する。
    /// </summary>
    /// <param name="_se_resouce_name">seのリソース名を取得する。</param>
    /// <returns></returns>
    public AudioClip GetSeResouceData(string _se_resouce_name)
    {
        //ここで取得する先のメソッドは変更する必要がある
        var _audio = karino_method();
        if (null == _audio)
        {
            HackDebug.Console.LogWarning(_se_resouce_name + "の名前のリソースは存在しません");
        }
        return _audio;
    }

    

    /// <summary>
    /// TODO:かりのメソッドなのでそのうち消す
    /// </summary>
    /// <returns></returns>
    private AudioClip karino_method()
    {
        AudioClip _source = null;


        if (false ==  bgm_data_base.isUseOk)
        {
            return _source;
        }

        _source = bgm_data_base.getBGM("bgm");


        return _source;
    }

}
