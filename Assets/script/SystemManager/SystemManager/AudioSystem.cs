﻿///<summary>
/// 概要：音楽をならすためのシステム。
/// 外部によって、更新や初期化などを行ってもらう必要がある。
/// 外部から、この効果音をならせーとかきたらならすためのもの。
/// 
/// TODO：
/// 効果音を鳴らす際には、あらかじめ展開してある音楽リソースのクラスから、
/// 指定の効果音を取得してそれに対してならす登録を行い、Updateのタイミングで、ならす。
/// 
/// 
/// 
/// 注意事項
/// AudioListenerに関しては、
///
/// <filename>
/// AudioSystem.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using SceneRelation;

public class AudioSystem : Singleton<AudioSystem>, IUpdater
{
    #region field
    //カメラから、この参照を取得する。
    private AudioListener Listener;

    private AudioSource source;
    private GameObject AudioGameObject;
    //予約の一覧
    private List<ReserveSeData> reservation_se_list;

    

    #region innnerclass

    private class ReserveSeData
    { 
        /// <summary>SEのID、Enumを変換</summary>
        public int seId;
        /// <summary>ならすSEの参照</summary>
        public AudioClip se_clip;
        /// <summary>ならすボリューム</summary>
        public float volume;
        /// <summary>
        /// ならし始めたタイミング
        /// </summary>
        public float StartTime;

        /// <summary>
        /// データを格納するためだけのコンストラクタ
        /// </summary>
        /// <param name="_se_id"></param>
        /// <param name="_clip"></param>
        /// <param name="_volume"></param>
        public ReserveSeData(int _se_id, AudioClip _clip,float _volume = 1.0f)
        {
            seId    = _se_id;
            se_clip = _clip;
            volume  = _volume;
        }
    }

    #endregion


    #endregion

    #region IUpdaterMethod
    public void Destroy()
    {

    }

    public void Init()
    {
        reservation_se_list = new List<ReserveSeData>();
        AudioGameObject = new GameObject("AudioSystemActing");
        source = AudioGameObject.AddComponent<AudioSource>();
    }

    /// <summary>
    /// Debug用のものUGUIに対して、表示する
    /// </summary>
    /// <param name="_rect"></param>
    /// <returns>これに対して、使用した高さの位置</returns>
    public float UnityGUIDraw(Rect _rect)
    {
        return 0;
    }

    /// <summary>
    /// 更新を行う為のもの
    /// </summary>
    public void UpdateMine()
    {
        
    }

    public void LateUpdateMine()
    {
        
    }
    #endregion

    
    /// <summary>
    /// 指定のBGMを指定のボリュームをならす。
    /// </summary>
    /// <param name="_bgm_name"></param>
    /// <param name="volume"></param>
    public void StartBgm(string _bgm_name,float volume)
    {
        if(false == ResourceManager.ExistInstance)
        {
            return;
        }

    }

    /// <summary>
    /// Seならすのを予約を行う。
    /// </summary>
    /// <param name="_se_name"></param>
    /// <param name="volme"></param>
    public void ReservationSeList(string _se_name,float volme)
    {
        if(null == reservation_se_list)
        {
            return;
        }

        //TODO:ここで、ひつようなSEをひぱっってくる
        AudioClip _clip = new AudioClip();
        ReserveSeData _resurve = new ReserveSeData(1,_clip);
        reservation_se_list.Add(_resurve);
    }




}
