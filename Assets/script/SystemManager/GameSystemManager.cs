﻿///<summary>
/// 概要：あらかじめオブジェクトとして配置した上で使用を行う。
/// このManagerが、あらゆるシステムに対して、色々を生成したりを行う。
/// 
/// 今の所全知全能感ただよう立ち位置だが、そのうちGameSceneのみでの動作予定。
/// GameSceneにおいての色々をここで全て生成を行ったりする。
/// 
///
/// <filename>
/// GameSystemManager.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class GameSystemManager : MonoBehaviour 
{

    //ひとまず参照を取得しておく
    private InputSystemManager inputSystem;

    private List<IUpdater> need_update_system;

    // Use this for initialization
    void Start () 
	{
        need_update_system = new List<IUpdater>();

        inputSystem = InputSystemManager.CreateInstance();
        GameObject _enemy_pop_manager_obj = new GameObject("EnemyPopManager");
        EnemyPopManager _enemy_pop_manager = _enemy_pop_manager_obj.AddComponent<EnemyPopManager>();
        UnitManagerSystem _unit_manager = UnitManagerSystem.CreateInstance();
        need_update_system.Add(_unit_manager);
        UIManager _ui_manager = UIManager.CreateInstance();
        need_update_system.Add(_ui_manager);
        ResourceManager _resource_manager = ResourceManager.CreateInstance();
        need_update_system.Add(_resource_manager);
        //TODO:ここはこのつくり良くない
        var _audio = this.gameObject.AddComponent<AudioSource>();
        _resource_manager.setAudioSource(_audio);
        CallSystemInitMethod();
	}

    /// <summary>
    /// Listに入っている物を対象に、初期化を行う。
    /// </summary>
    private void CallSystemInitMethod()
    {
        if(need_update_system.Count == 0)
        {
            Debug.LogError("GameSystemManager　dont valid operation");
            return;
        }

        //必要な更新が必要なものの初期化を行う
        for(int _i = 0;_i < need_update_system.Count;_i++)
        {
            need_update_system[_i].Init();
        }
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (need_update_system.Count == 0)
        {
            Debug.LogError("ameSystemManager　dont valid operation");
            return;
        }

        //必要な更新が必要なものの初期化を行う
        for (int _i = 0; _i < need_update_system.Count; _i++)
        {
            need_update_system[_i].UpdateMine();
        }
    }

    private void LateUpdate()
    {
        if (need_update_system.Count == 0)
        {
            Debug.LogError("ameSystemManager　dont valid operation");
            return;
        }

        //必要な更新が必要なものの初期化を行う
        for (int _i = 0; _i < need_update_system.Count; _i++)
        {
            need_update_system[_i].LateUpdateMine();
        }
    }



#if DEBUG
    private void OnGUI()
    {
        
    }
#endif
}
