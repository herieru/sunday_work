﻿///<summary>
/// 概要：ユニットの管理を行うそのオブジェクト
/// プレイヤーと敵の管理の数のオブジェクトの参照を持つ。
///
/// <filename>
/// UnitManagerSyatem.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class UnitManagerSystem : Singleton<UnitManagerSystem>,IUpdater 
{
    #region field
    #region　player
    /// <summary>
    /// プレイヤーであるユニット
    /// </summary>
    private GameObject player_unit;

    /// <summary>
    /// プレイヤーに対してのオブジェクトを設定する。
    /// </summary>
    public GameObject Player
    {
        get { return player_unit; }
        set { player_unit = value; }
    }

    /// <summary>
    /// PlayerからPlayerUnit型のコンポーネントを作成している。
    /// </summary>
    public PlayerUnit PlayerUnit
    {
        get
        {
            if(null == player_unit)
            {
                return null;
            }

            var _player_unit_comp = player_unit.GetComponent<PlayerUnit>();
            return _player_unit_comp;
        }
    }
    #endregion

    #region enemy_unit

    int count = 0;
    
    /// <summary>
    /// 敵ユニットの保管場所 値として生成がされていなければ、０
    /// </summary>
    private List<GameObject> enemy_unit = new List<GameObject>();

    /// <summary>
    /// 対象の通知が必要なオブジェクト
    /// FIX:ここはかなりくそな作り、できればP->Eと生成される処理ができたらいらない
    /// </summary>
    private List<System.Action<GameObject>> notification_target = new List<System.Action<GameObject>>();


    /// <summary>
    /// 出現している敵の数
    /// </summary>
    public int PopEnemyCount
    {
        get
        {
            if(null == enemy_unit)
            {
                return 0;
            }
            return enemy_unit.Count;
        }
    }

    /// <summary>
    /// エネミーユニットの参照を取得する。
    /// </summary>
    public List<GameObject> EnemyUnitList
    {
        get { return enemy_unit; }
    }



    #endregion end enemy_unit

    #endregion  end field
    #region MonobeheviorMethod


    // Update is called once per frame
    void Update () 
	{
        enemy_pop_data_update();
        
        if(notification_target.Count > 0 && null != player_unit)
        {
            foreach(var _notfi_action in notification_target)
            {
                _notfi_action(player_unit);
            }
            notification_target.Clear();
        }
	}
    #endregion

    #region public method

    /// <summary>
    /// 出現した敵のオブジェクトを保存しておく
    /// </summary>
    /// <param name="_object">敵が出現する際に持っておく</param>
    /// <param name="_target_call_back">ターゲットを設定する為の関数デリゲート</param>
    public void SetPopEnemyNotification(GameObject _object,System.Action<GameObject> _target_call_back)
    {
        if(null == enemy_unit)
        {
            return;
        }
        enemy_unit.Add(_object);
        //TODO:死んだ時に通知する内容を渡す。

        if(null == player_unit)
        {
            notification_target.Add(_target_call_back);
            return;   
        }
        _target_call_back(player_unit);

    }

    /// <summary>
    /// COMMENT：これは一回予約形式にした方がいいかもしれない。
    /// </summary>
    /// <param name="_enemy_object"></param>
    public void DeadEnemy(GameObject _enemy_object)
    {
        Vector3 _pos = _enemy_object.transform.position;
        UnityEngine.Object.Destroy(_enemy_object);
        var _obj = Resources.Load("prefab/Particle/Explosion",typeof(GameObject)) as GameObject;
        if(null == _obj)
        {
            Debug.Log("生成失敗");
            return;
        }
        GameObject.Instantiate(_obj,_pos,Quaternion.identity);
    }
    #endregion

    /// <summary>
    /// 保持しているEnemyがnullになっていないかをチェックして、
    /// nullになった時点で、それをリストから外す。
    /// </summary>
    private void enemy_pop_data_update()
    {
        enemy_unit.RemoveAll((x) => null == x);
    }

    #region 

    /// <summary>
    /// 初期化するものを入れる
    /// </summary>
    public void Init()
    {
        enemy_unit = new List<GameObject>();
    }

    /// <summary>
    /// 
    /// </summary>
    public void UpdateMine()
    {
        if(null == enemy_unit)
        {
            return;
        }
        //nullの参照になっていたら消す。
        enemy_unit.RemoveAll((x) => (x == null));

    }

    /// <summary>
    /// 少し遅くなる更新
    /// </summary>
    public void LateUpdateMine()
    {

    }

    public void Destroy()
    {
        
    }


    /// <summary>
    /// MonoBeheiviorを持っているクラスから、onGUIで呼び出す。
    /// デバック機能として使用
    /// </summary>
    /// <param name="_rect"></param>
    public float UnityGUIDraw(Rect _rect)
    {
        float _height = 0;

        return _height;
    }

    #endregion
}
