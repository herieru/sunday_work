﻿///<summary>
/// 概要：PlayerPrefsに対してアクセスを行えるようにするためのものです。
/// 最終的にデバックによるキーがTrueになっていたら、
/// PlayerPrefsに対して、自由に変更を加えられる
/// 権限を与えれるようにします。
/// どのように権限を与えれるかはわかりませんが、そのうち権限を与えます。
/// 
/// どういった保存方法があるか？
/// JSON、PlayerPrefs（たぶん楽だけど、キーが多くなるとめんどい）
/// バイナリ―
/// 
/// JSONとバイナリーの場合別途、格納する為の処理をおこなう必要がある。
/// チート？対策する上ではどういった形式が一番有効だろうか？
/// 
/// どれくらいコストがかかるか？
/// 
/// 簡単に編集などは可能か？
/// 
/// キーをどこかで持っていて、それを取得した上で持っておく必要とかはあるか？
/// その辺の考慮はしておいた方がいいかもしれない。
/// 
/// 
/// <filename>
/// SaveManager.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class SaveManager : Singleton<SaveManager> 
{
    public void DataSaveInt()
    {

    }


}
