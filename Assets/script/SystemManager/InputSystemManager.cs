﻿///<summary>
/// 概要：入力系の管理を行う為のもの
/// Inputに対して、アクセスを行い、その時の情報を取得を行う。
/// GameSystemManagerなどのSingletonから生成が行われる。
/// 
/// 
/// TODO：まだ未定だが、UIがここに対して、当たり判定とってくれ的なものを投げてきて
/// それに対して当たっているかを判断した上で、色々したりしなかったり？
/// 
/// もしくはMonoBeheiviorを継承を行い、Inputの情報部分で、色々を行ったり＞
/// まだ未定
/// 
/// ここにいれるかわからんがそのタッチが、UIに当たっているかの情報まで処理した上で、
/// 処理した後のタッチ情報しか持っていないくらいが理想
/// 
/// <filename>
/// InputSystemManager.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class InputSystemManager : Singleton<InputSystemManager>
{
    private Camera mainCamera;

    private TouchInfoProcesser InputSystem;

    /// <summary>
    /// タッチ情報が存在するかを取得を行う。存在していたらtrue
    /// </summary>
    public bool IsExistTouchInfo
    {
        get { return InputSystem.isExistTouchInfo; }
    }

    public InputSystemManager()
    {
        GameObject _input_system_object = new GameObject("TouchSystem");
        InputSystem = _input_system_object.AddComponent<TouchInfoProcesser>();
        mainCamera = Camera.main;
    }


    /// <summary>
    /// UIに引っかかっていないタッチの情報を更新する。
    /// </summary>
    /// <returns></returns>
    public Vector3 GetTouchDir()
    {
        if (false == IsExistTouchInfo && null == mainCamera)
        {
            return Vector3.zero;
        }

        //TODO:UI以外に触れているタッチ情報として、処理しているタッチ情報の取得

        Vector3 _touch_pos = fetch_touch_valid();
        Ray _ray = mainCamera.ScreenPointToRay(_touch_pos);

        RaycastHit _hit;

        if (Physics.Raycast(_ray, out _hit))
        {
            Debug.Log("HitPoint:" + _hit.point + "hitObjectname:" + _hit.collider.gameObject);
            return _hit.point;
        }

        return Vector3.zero;
    }

    /// <summary>
    /// 有効なタッチ情報を取得を行う。
    /// 必ずタッチ情報が存在するかどうかの判定をしてから呼び出してください。
    /// </summary>
    /// <returns>タッチしている情報。</returns>
    private Vector3 fetch_touch_valid()
    {
        if (false == IsExistTouchInfo)
        {
            return Vector3.zero;
        }

        //現状ただの先頭のタッチ情報を取得しているだけ。
        var _touch_info = InputSystem.GetDictionaryIndex();
        return _touch_info.touch_info.position;
    }

    /// <summary>
    /// タッチIDによって、その位置情報を取得を行う。
    /// </summary>
    /// <param name="_is_valid">その情報が有効か</param>
    /// <param name="_finger_id">取得を行いたいタッチID</param>
    /// <returns></returns>
    public Vector3 GetTouchPos(out bool _is_valid, int _finger_id)
    {
        _is_valid = false;

        if (false == IsExistTouchInfo)
        {
            Debug.Log("is not exitst touch info");
            return Vector3.zero;
        }

        bool _is_exist = false;

        var _touch_info = InputSystem.UseFingerIDGetDictionaryInfomation(out _is_exist, _finger_id);

        if (false == _is_exist)
        {
            return Vector3.zero;
        }

        _is_valid = true;
        return _touch_info.touch_info.position;
    }


    /// <summary>
    /// タッチ情報自体の取得
    /// </summary>
    /// <param name="_is_valid">その情報が有効か？</param>
    /// <param name="_finger_id">取得を行いたいタッチID</param>
    /// <returns></returns>
    public TouchFirstProcessInfomation GetTouchInfo(out bool _is_valid, int _finger_id)
    {
        _is_valid = false;

        if (false == IsExistTouchInfo)
        {
            Debug.Log("is not exitst touch info");
            return null;
        }

        bool _is_exist = false;

        var _touch_info = InputSystem.UseFingerIDGetDictionaryInfomation(out _is_exist, _finger_id);

        if (false == _is_exist)
        {
            Debug.Log(" touch_info_is null");
            return null;
        }

        _is_valid = true;
        return _touch_info;
    }

    /// <summary>
    /// 処理が正しいかを確かめれるためにここに仮にポジションをCameraのRayの位置に変換して、処理を行う。
    /// </summary>
    /// <param name="_screen_pos">確かめたいスクリーン座標</param>
    /// <returns></returns>
    public Vector3 GetPositionScreenToHitPosition(Vector3 _screen_pos)
    {

        Ray _ray = mainCamera.ScreenPointToRay(_screen_pos);

        RaycastHit _hit;
        if (Physics.Raycast(_ray, out _hit))
        {
            return _hit.point;
        }

        return Vector3.zero;
    }

    /// <summary>
    /// 処理が正しいかを確かめれるためにここに仮にポジションをCameraのRayの位置に変換して、処理を行う。
    /// </summary>
    /// <param name="_screen_pos">確かめたいスクリーン座標</param>
    /// <returns></returns>
    public Vector3 GetPositionScreenToHitPosition(out bool _is_hit,Vector3 _screen_pos)
    {
        _is_hit = false;

        Ray _ray = mainCamera.ScreenPointToRay(_screen_pos);

        RaycastHit _hit;
        if (Physics.Raycast(_ray, out _hit))
        {
            _is_hit = true;

            Debug.Log("あたったのは：" + _hit.collider.gameObject.name);
            return _hit.point;
        }

        return Vector3.zero;
    }
}
