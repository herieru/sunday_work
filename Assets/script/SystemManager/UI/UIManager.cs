﻿///<summary>
/// 概要：基本的にUIを操作する上で切っても切り離せない内容になる予定。
///
/// <filename>
/// UIManager.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using ClickRecive;
using UnityEngine;


public class UIManager : Singleton<UIManager> ,IUpdater
{
    //反応が必要なオブジェクトを設定していく
    private Dictionary<GameObject, IClickReceive> need_dic_reflection_object;

    //反応を示したゲームオブジェクト
    private List<ReactionObjectInfo> reaction_obj_list;


    /// <summary>
    /// 反応を示す為のタッチ情報
    /// </summary>
    private class ReactionObjectInfo
    {
        public ReactionObjectInfo(int _touch_id , GameObject _obj)
        {
            touch_id = _touch_id;
            reaction_object = _obj;
        }

        //タッチされたであろうID
        public int touch_id;
        //反応を示したGameObject
        public GameObject reaction_object;
    }

   
    void IUpdater.Init()
    {
        need_dic_reflection_object = new Dictionary<GameObject, IClickReceive>();
        reaction_obj_list = new List<ReactionObjectInfo>();
    }

    public void UpdateMine()
    {
       
    }

    public void LateUpdateMine()
    {
         //ひとまずめんどくさいので、これで実装
        LateUpdate();
    }

    // Update is called once per frame
    private void LateUpdate () 
	{
        
        //new されていなかったら何もしない。
        if(null == reaction_obj_list || null == need_dic_reflection_object)
        {
            Debug.Log("nullってるよ");
            return;
        }
        //ここに反応があったオブジェクトに対して、処理を行っていくものを行う。
        //主にそのオブジェクトに対してついているであろうイベント受け取りinterfaceに対して、
        //タッチの状態に合わせて、通知を行う。

        if(false == InputSystemManager.ExistInstance || 
            false ==InputSystemManager.Instance.IsExistTouchInfo)
        {
            
            //まだinputsystemが準備整ってないので、このフレーム内のイベントは破棄
            reaction_obj_list.Clear();
            return;
        }

        transmission_notification();
	}

    /// <summary>
    /// その反応が正しいという事を対象のGameObjectに対して伝えてあげる。
    /// 今反応来ている内容を見て、タッチが情報として使用しているものかなどの更新を行う。
    /// </summary>
    private void transmission_notification()
    {
        
        for(int _i = 0;_i < reaction_obj_list.Count;_i++)
        {
            bool _is_exist = false;
            var _touch_info = InputSystemManager.Instance.GetTouchInfo(out _is_exist, reaction_obj_list[_i].touch_id);

            //存在しなかったので、おかしいけど無視
            if(false == _is_exist)
            {
                Debug.LogError("反応がタッチと整合性あってない。");
                continue;
            }
            GameObject _obj = reaction_obj_list[_i].reaction_object;
            _is_exist = need_dic_reflection_object.ContainsKey(_obj);

            if(false == _is_exist)
            {
                Debug.Log("反応を示す為のオブジェクトが存在しない。");
                continue;
            }

            IClickReceive _receve = null;
            need_dic_reflection_object.TryGetValue(_obj,out _receve);

            if(null == _receve)
            {
                Debug.Log("Reciveのオブジェクトがない");
                continue;
            }
            _receve.ClickRecever(exchange_touchphase(_touch_info),_touch_info);
        }
        reaction_obj_list.Clear();
    }

    /// <summary>
    /// phaseに合わせた、Typeを選択する。
    /// </summary>
    /// <param name="_touch_info"></param>
    /// <returns></returns>
    private ReciveType exchange_touchphase(TouchFirstProcessInfomation _touch_info)
    {
        switch (_touch_info.touch_info.phase)
        {
            case TouchPhase.Began:
                return ReciveType.Begin;
                break;
            case TouchPhase.Moved:
                return ReciveType.Drag;
                break;
            case TouchPhase.Ended:
                return ReciveType.Ended;
                break;
            default:
                return ReciveType.None;
                break;
        }
    }



    /// <summary>
    /// 様々なオブジェクトから、タッチIDとGameObjectが通知として飛んでくる。
    /// </summary>
    /// <param name="_touch_id">タッチのID</param>
    /// <param name="_reaction_object">タッチに対して、反応があった、オブジェクト</param>
    public void SetReactionGameObject(int _touch_id,GameObject _reaction_object)
    {

        if (null == reaction_obj_list)
        {
            Debug.LogWarning("まだreactionインスタンスが作られていない。");
            return;
        }
#if UNITY_EDITOR
        //マウスの左クリックに合わせてのID設定
        _touch_id = 0;
#endif
        Debug.Log("反応が来たオブジェクトは" + _reaction_object.name);
        ReactionObjectInfo _info = new ReactionObjectInfo(_touch_id, _reaction_object);
        reaction_obj_list.Add(_info);

        Debug.Log("ついかされている数は" + reaction_obj_list.Count);
    }


    /// <summary>
    /// リアクションが必要なゲームオブジェクトを生成する
    /// </summary>
    /// <param name="_need_notification_obj"></param>
    /// <param name="_game_object"></param>
    public bool SetNotificationGameObject(GameObject _need_notification_obj )
    {
        if(null == need_dic_reflection_object)
        {
            Debug.LogWarning("まだインスタンスが作られていない。");
            return false;
        }
        var _recive = _need_notification_obj.GetComponent<IClickReceive>();
        if(null == _recive)
        {
            Debug.LogWarning("GamelObject：" +  _need_notification_obj.name + "はIClickReceiveがついていません" );
            return false;
        }
        Debug.Log("登録されてオブジェクトは" + _need_notification_obj.name);

        ///すでに登録済みだったため。
        if(need_dic_reflection_object.ContainsKey(_need_notification_obj))
        {
            return false;
        }


        need_dic_reflection_object.Add(_need_notification_obj, _recive);

        return true;
    }

    

    public void Destroy()
    {
        
    }

    public float UnityGUIDraw(Rect _rect)
    {
        throw new System.NotImplementedException();
    }
}
