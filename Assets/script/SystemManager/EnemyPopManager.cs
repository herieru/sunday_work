﻿///<summary>
/// 概要：Enemyが出現する為の位置や、出現しろと指示などを出す為の
/// 一種のレベルデザイン的AI
/// GameSystemManagerから生成がされる。
/// 
/// レベルに合った敵の数と、総レベルなどを計算して、うまいこと調整を行うもの。
/// 
/// TODO:出現位置とかは、プレイヤー位置を中心として、カメラの高さ（紐の長さで変動）したものを
/// 元に、位置を計算して、その位置から更新によって出現を促す。
/// 
/// 
/// 
///
/// <filename>
/// EnemyPopManager.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>

using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 受け渡しを行う為のクラス
/// </summary>
public class InitEnemyData
{
    public InitEnemyData(EnemyPopManager.PopEnemyType _type,GameObject _prefab)
    {
        prefabData = _prefab;
        type = _type;
    }
    //生成するPrefab
    public GameObject prefabData;
    //それに対応する為の、Type
    public EnemyPopManager.PopEnemyType type;
}

public class EnemyPopManager : MonoBehaviour 
{
    //出現位置個数　　COMMENT：レベルによって変える？　たぶん必要ない。
    private const int ARRIVALPOINTNUM = 4;


    /// <summary>
    /// プレイヤーの位置情報を取得する為のもの
    /// </summary>
    private GameObject player_object;

    /// <summary>
    /// 
    /// </summary>
    private GameObject Camera;

    /// <summary>
    /// Enemyの数だけ取得してくる。
    /// </summary>
    private string resource_path = "prefab/Character/Enemy/";


    /// <summary>
    /// 出現位置を取得するためのもの
    /// </summary>
    private List<EnemyPopper> enemy_popper;

    /// <summary>
    /// Prefab名と同期しなければいけない。
    /// </summary>
    public enum PopEnemyType
    {
        Tracer,            //追従のタイプ
        Diffencer,          //Hpが多いやつ
        Attacker,           //攻撃してくるやつ
    }

    /// <summary>
    /// 出現の塊の出し方。
    /// </summary>
    private enum ChankType
    {

    }


	// Use this for initialization
	void Start () 
	{
        enemy_popper = new List<EnemyPopper>(); 

        
        Initalize();

    }

    /// <summary>
    /// 初期化のための関数を行う。
    /// </summary>
    private void Initalize()
    {
        load_now_player_skill_level();
        enemy_pop_initalize();
    }


    /// <summary>
    /// 現在のプレイヤーレベルを取得します。
    /// </summary>f
    private void load_now_player_skill_level()
    {
        //TODO：何かしらのセーブデータに対して、
        //レベルに当たる部分を取得を行う必要がある
        //そもそもセーブデータをどういう形式で管理を行う必要があるかを考える必要がある。

    }

    private void enemy_pop_initalize()
    {
        enemy_pop_born_initalize(init_enemy_prefab_data());

    }
 
    /// <summary>
    /// EnemyのPrefabを初期化行う。
    /// </summary>
    /// <returns></returns>
    private InitEnemyData[] init_enemy_prefab_data()
    {
        List<InitEnemyData> _list_data = new List<InitEnemyData>();

        //TODO:必要なデータを取得する。
        foreach(var _enum in Enum.GetValues(typeof(PopEnemyType)))
        {
            string _resource_path = resource_path + _enum.ToString();
            Debug.Log("get resouce path :" + _resource_path);
            //リソースの中身を取得する。
            GameObject _prefab = Resources.Load(_resource_path) as GameObject;
            if(null == _prefab)
            {
                HackDebug.Console.LogError("Error get resources " + _resource_path);
            }
            else
            {
                _list_data.Add(new InitEnemyData((PopEnemyType)_enum,_prefab));
            }
        }

        return _list_data.ToArray();
    }

    /// <summary>
    /// EnemyPopperの初期化
    /// </summary>
    /// <param name="_prefab_data">生成を行う予定のPrefab</param>
    private void enemy_pop_born_initalize(InitEnemyData[] _prefab_data)
    {
        //出現位置の設置　これを基準に回転等を行う場合、親オブジェクトを回転させる。
        for (int _i = 0; _i < ARRIVALPOINTNUM; _i++)
        {
            GameObject _obj = new GameObject("EnemyPopper" + _i);/// GameObject.CreatePrimitive(PrimitiveType.Cube);//new GameObject("EnemyPopeer" + _i);

            Vector3 _dir = Quaternion.AngleAxis(_i * 90.0f, Vector3.up) * (Vector3.forward * 10);
            _obj.transform.localPosition = _dir;
            _obj.transform.parent = this.transform;
            var _enemy_popper = _obj.AddComponent<EnemyPopper>();
            _enemy_popper.SetPrefabData(_prefab_data);
            enemy_popper.Add(_enemy_popper);
        }
    }

    // Update is called once per frame
    void Update () 
	{
        Quaternion _rotate = this.transform.localRotation;
        _rotate = _rotate * Quaternion.Euler(0, 1, 0);
        this.transform.localRotation = _rotate;
        float _pop_time = 1.0f;
#if UNITY_EDITOR
        //TODO；ここは、そのうち、自動的に判断を行った上で敵が出現するようになる。
        if(Input.GetKeyDown(KeyCode.F))
        {
            int _num = UnityEngine.Random.RandomRange(0, 4);
            enemy_popper[_num].SetSinglePopEnemyData(_pop_time, PopEnemyType.Tracer);
        }

        if(Input.GetKeyDown(KeyCode.D))
        {
            int _num = UnityEngine.Random.RandomRange(0, 4);
            enemy_popper[_num].SetSinglePopEnemyData(_pop_time, PopEnemyType.Attacker);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            int _num = UnityEngine.Random.RandomRange(0, 4);
            enemy_popper[_num].SetSinglePopEnemyData(_pop_time, PopEnemyType.Diffencer);
        }
#endif

        _time += Time.deltaTime;
        if(_time < 3.0f)
        {
            return;
        }
        _time = 0.0f;
        //FIX:とりあえず、今は時間で実装を行うが、最終的に、状況をみて、そこから、通知が来て
        //どこに何が出現を行うかを設定する。
#if UNITY_ANDROID
        int _nums = UnityEngine.Random.RandomRange(0, 4);
        int _prefab_number = UnityEngine.Random.RandomRange(0, 2);
        enemy_popper[_nums].SetSinglePopEnemyData(_pop_time, (PopEnemyType)_prefab_number);
#endif
    }

    float _time = 0.0f;
    
}
