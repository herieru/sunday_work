﻿///<summary>
/// 概要：あらゆるSceneに対しての、
///
/// <filename>
/// EnumScene.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

namespace SceneRelation
{
    public enum SceneDefinition
    {
        /// <summary>
        /// タイトル画面
        /// </summary>
        Title,   
        /// <summary>
        /// ゲームメイン
        /// </summary>
        MainGame,  
        /// <summary>
        /// 色々な設定
        /// </summary>
        Setting,            
        /// <summary>
        /// スタッフロール等
        /// </summary>
        StafRoll,
        /// <summary>
        /// 秘密のデバッグルーム
        /// </summary>
        ScecretSetting,
    }
}
