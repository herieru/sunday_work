﻿///<summary>
/// 概要：
///
/// <filename>
/// EnemyPopper.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

public class EnemyPopper : MonoBehaviour 
{
    /// <summary>
    /// 経過時間
    /// </summary>
    private float elapsed_time;

    /// <summary>
    /// 生産の必要があるか？
    /// </summary>
    private bool is_create_update;

    /// <summary>
    /// 出現の予約をしているもの
    /// </summary>
    private List<ResurveData> reserve_pop_list;

    /// <summary>
    /// 生成を行う為のPre
    /// </summary>
    private Dictionary<EnemyPopManager.PopEnemyType, GameObject> prefab_data_dic;

    /// <summary>
    /// 予約する為のデータ
    /// </summary>
    private class ResurveData
    {
        /// <param name="_interval">予約する為のデータ</param>
        /// <param name="_type">敵の型</param>
        public ResurveData(float _interval, EnemyPopManager.PopEnemyType _type)
        {
            type = _type;
            next_interval_time = _interval;
        }

        //予約対象
        public EnemyPopManager.PopEnemyType type;
        //次に出現するまでの時間
        public float next_interval_time;
    }

    private void Awake()
    {
        reserve_pop_list = new List<ResurveData>();
    }

    // Use this for initialization
    void Start () 
	{
        elapsed_time = 0;
        is_create_update = false;

    }
	
	// Update is called once per frame
	void Update () 
	{
		//先頭に入っているタイムのインターバルを過ぎていたら、生成を行う。
        if(false == is_create_update)
        {
            return;
        }
        //更新時間を毎回足す
        elapsed_time += Time.deltaTime;

        if(reserve_pop_list[0].next_interval_time < elapsed_time)
        {
            //必要時間分現在の経過時間を削る。
            elapsed_time -= reserve_pop_list[0].next_interval_time;

            born_next_enemy();

            if(reserve_pop_list.Count == 0)
            {
                elapsed_time = 0;
                is_create_update = false;
            }

        }


	}

    /// <summary>
    /// Prefabとして出現させるためのもの
    /// </summary>
    /// <param name="_prefab_data"></param>
    public void SetPrefabData(InitEnemyData[] _prefab_data)
    {
        if(null == prefab_data_dic)
        {
            prefab_data_dic = new Dictionary<EnemyPopManager.PopEnemyType, GameObject>();
        }

        ///Prefabのデータを格納を行う。
        for(int _index = 0;_index < _prefab_data.Length;_index++)
        {
            prefab_data_dic.Add(_prefab_data[_index].type, _prefab_data[_index].prefabData);
        }

    }

    /// <summary>
    /// 出現情報を取得する interval部分をまとめて同じものを入れる。
    /// </summary>
    /// <param name="_pop_interval">出現時間</param>
    /// <param name="_pop_types">出現させる敵の種類</param>
    public void SetGroupPopEnemyData(float _pop_interval,params EnemyPopManager.PopEnemyType[] _pop_types)
    {
        for(int _i = 0; _i < _pop_types.Length;_i++)
        {
            reserve_pop_list.Add(new ResurveData(_pop_interval, _pop_types[_i]));
        }
        UpdateStart();
    }

    /// <summary>
    /// 単体で予約を行う。
    /// </summary>
    /// <param name="_pop_interval">経過時間</param>
    /// <param name="_pop_type">出現する敵のタイプ</param>
    public void SetSinglePopEnemyData(float _pop_interval,EnemyPopManager.PopEnemyType _pop_type)
    {
        reserve_pop_list.Add(new ResurveData(_pop_interval,_pop_type));
        UpdateStart();
    }

    /// <summary>
    /// 更新が止まっていたら、経過時間を初期化して、生産開始
    /// </summary>
    private void UpdateStart()
    {
        //すでに生産中なら、何もしない
        if(is_create_update)
        {
            return;
        }
        elapsed_time = 0.0f;
        is_create_update = true;
    }

    /// <summary>
    /// 格納されている先頭のデータを取得して、それを元に、出現を促す
    /// </summary>
    private void born_next_enemy()
    {
        //取得されたデータを元に生成を行う
        var _reserve_data = reserve_pop_list[0];
        var _type = _reserve_data.type;
        if(false == prefab_data_dic.ContainsKey(_type))
        {
            return;
        }

        GameObject _prefab = null;
        prefab_data_dic.TryGetValue(_type, out _prefab);
        GameObject _pop_enemy = GameObject.Instantiate(_prefab, transform.position, transform.rotation);
        reserve_pop_list.Remove(_reserve_data);

        EnemyUnitBase _base = _pop_enemy.GetComponent<EnemyUnitBase>();
        if(null == _base)
        {
            Debug.LogError("エラー：EnemyUniteBaseがついていないPrefabを製造しようとしています");
        }
       

        //ユニットを管理している所に通知を飛ばす
        UnitManagerSystem.Instance.SetPopEnemyNotification(_pop_enemy,_base.SetTarget);
    }


}
