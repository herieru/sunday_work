﻿///<summary>
/// 概要：
///
/// <filename>
/// IUpdater.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

interface IUpdater 
{
    /// <summary>
    /// 何かしらの初期化を行う為のモノ
    /// </summary>
    void Init();

    /// <summary>
    /// MonoBeheiviorでない方法で更新を行う
    /// </summary>
    void UpdateMine();

    /// <summary>
    /// MonoBeheibiorではない遅れた更新
    /// </summary>
    void LateUpdateMine();


    /// <summary>
    /// 継承先のクラスだけを消す必要がある。
    /// </summary>
    void Destroy();

#if DEBUG

    /// <summary>
    /// かならず実装の際は、#DEBUGでくくってください
    /// 呼び出し元から呼び出す際に必要
    /// </summary>
    /// <param name="_rect"></param>
    float UnityGUIDraw(Rect _rect);

#endif
}
