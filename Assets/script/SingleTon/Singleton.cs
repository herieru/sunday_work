﻿///<summary>
/// 概要:シングルトンのためのクラス
///
/// <filename>
/// ファイル名:Singleton
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> where T :class,new()
{
    private static T instance = new T();

    protected Singleton()
    {
    }

    public static T Instance
    {
        get
        {
            if(null == instance)
            {
                instance = new T();
            }

            return instance;
        }
    }

    /// <summary>
    /// インスタンスの作成
    /// </summary>
    public static T CreateInstance()
    {
        if(null == instance)
        {
            instance = new T(); 
        }
        return instance;
    }

    
    /// <summary>
    /// インスタンスが存在しているかの確認
    /// 存在していれば、true
    /// </summary>
    public static bool ExistInstance
    {
        get{ return null != instance;}
    }

}
