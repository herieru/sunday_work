﻿///<summary>
/// 概要：シングルトンのMonoBeheivior継承したもの
/// 更新とか起動時に、色々を行えるようにしているものの基底
///
/// Comment:
/// GameObjectとして存在した上で、そこに対して、コンポ―ネントとして、値を保持等を行っていても良いかもしれない。
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMonoBehavior<T> : MonoBehaviour where T :MonoBehaviour
{
    private static T instance;

    protected SingletonMonoBehavior()
    {
    }

    public static T Instance
    {
        get
        {
            if (null == instance)
            {
                instance = FindObjectHaveComponent();
            }
            return instance;
        }
    }

    /// <summary>
    /// インスタンスを明示的に作成
    /// </summary>
    public static T CreateInstance()
    {
        if (null == instance)
        {
            instance = FindObjectHaveComponent();
        }
        return instance;
    }

    
/// <summary>
    /// ついているオブジェクトを見つける。
    /// </summary>
    /// <returns></returns>
    private static T FindObjectHaveComponent()
    {
        return FindObjectOfType<T>();
    }

    /// <summary>
    /// インスタンスが存在するか？の確認。
    /// 存在すればtrue
    /// </summary>
    public static bool ExistInstance
    {
        get { return null != instance; }
    }
}
