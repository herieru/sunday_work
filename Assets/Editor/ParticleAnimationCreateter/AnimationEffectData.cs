﻿///<summary>
/// 概要:
///
///
/// <filename>
/// ファイル名:AnimationEffectData
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アニメーションのコントローラーをタイムラインで操作する為のモノ
/// あくまで、データ部分に関してのもの
/// </summary>
public class AnimationEffectData
{

    public bool is_read
    {
        get
        {
            if(particle_systems.Count <= 0
                && line_renderer.Count <= 0
                && trail_render.Count <= 0)
            {
                return false;
            }
            return true;
        }
    }

    /// <summary>
    /// プレファブとなるルートの部分になる。
    /// </summary>
    private GameObject prefab_root;

    //ひとまず、パーティクルシステムのみを実装する
    private List<ParticleSystem> particle_systems;

    public List<ParticleSystem> ParticleSyastems
    {
        get
        {
            return particle_systems;
        }
    }

    /// <summary>
    /// ３D空間上に線を描画する為のモノ
    /// </summary>
    private List<LineRenderer> line_renderer;

    public List<LineRenderer> LineRenderers
    {
        get
        {
            return line_renderer;
        }
    }
    //TrailRendererの保存場所
    private List<TrailRenderer> trail_render;

    public List<TrailRenderer> TrailRenderer
    {
        get
        {
            return trail_render;
        }
    }


    public AnimationEffectData(GameObject _prefab)
    {
        if (null == _prefab)
        {
            return;
        }
        particle_systems = new List<ParticleSystem>();
        line_renderer = new List<LineRenderer>();
        trail_render = new List<TrailRenderer>();

        prefab_root = _prefab;
        init_read_component(prefab_root);
    }
    #region コンポーネントを読み込む
    /// <summary>
    /// コンポ―ネントを読み取る。
    /// </summary>
    private void init_read_component(GameObject _root)
    {
        init_particle_system(_root);

    }

    /// <summary>
    /// パーティクルシステムのコンポーネントを取得して、メンバーに保存する。
    /// </summary>
    /// <param name="_root_obj"></param>
    private void init_particle_system(GameObject _root_obj)
    {
        ParticleSystem[] _particle_systems = _root_obj.GetComponentsInChildren<ParticleSystem>();
        for (int _i = 0; _i < _particle_systems.Length; _i++)
        {
            particle_systems.Add(_particle_systems[_i]);
        }
    }

    /// <summary>
    /// LineRendererのコンポーネントを読み込む
    /// </summary>
    /// <param name="_root_obj"></param>
    private void init_line_rederer(GameObject _root_obj)
    {
        ParticleSystem[] _particle_systems = _root_obj.GetComponentsInChildren<ParticleSystem>();
        for (int _i = 0; _i < _particle_systems.Length; _i++)
        {
            particle_systems.Add(_particle_systems[_i]);
        }
    }

    /// <summary>
    /// TrailRendererのコンポーネントを読み込む
    /// </summary>
    /// <param name="_root_obj"></param>
    private void init_trail_renderer(GameObject _root_obj)
    {
        ParticleSystem[] _particle_systems = _root_obj.GetComponentsInChildren<ParticleSystem>();
        for (int _i = 0; _i < _particle_systems.Length; _i++)
        {
            particle_systems.Add(_particle_systems[_i]);
        }
    }
    #endregion//コンポーネントの読み込み
}
