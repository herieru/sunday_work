﻿///<summary>
/// 概要:パーティクルシステムを複数使った、
/// Prefabをセットして読み込むことで、そのパーティクルシステムを
/// 簡単にタイムラインのように調整を行うことが出来る様になる。
///
///
/// <filename>
/// ファイル名:ParticleAnimationEditor
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>

#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class ParticleAnimationEditor : EditorEasyBase {

    private Controler_AnimationEffectData anime_controller;
    private Vector2 timeline_view_scroll_pos = new Vector2();

    [MenuItem("Tools/ParticleTimeLine")]
    public static void Done()
    {
        GetWindow<ParticleAnimationEditor>();
    }
    
    //データの読み込みを行ったかどうか？　最終的には消す
    private GameObject read_object;
    /// <summary>
    /// 描画部分
    /// </summary>
    private void OnGUI()
    {
        read_object = EditorGUILayout.ObjectField(read_object, typeof(GameObject), GUILayout.ExpandWidth(true))as GameObject;
        if (null != anime_controller && anime_controller.is_read_success)
        {
            state_exist_data();
        }
        else 
        {
            state_non_read();
            
        }
    }
   
    /// <summary>
    /// UnityによるUpdate
    /// 高速な処理を要する描画系をここで行う。
    /// </summary>
    private void Update()
    {
        
    }

    #region 読み込み部分
    /// <summary>
    /// 読み込む前の画面
    /// </summary>
    private void state_non_read()
    {
        Button("読み込み", read_effect_data, GUILayout.Width(200));
    }

    /// <summary>
    /// 現在選択している物に対して、読み込みを行う
    /// </summary>
    private void read_effect_data()
    {
        if (null == read_object)
        {
            DisplayDialog("警告", "読み込むオブジェクトが設定されていません。", null, null);
            return;
        }
        
        //シーン内に配置されていなければ、配置を促す
        if(false == read_object.activeInHierarchy)
        {
            DisplayDialog("警告", "読み込むオブジェクトがシーンに配置されていません。\n読み込んでよろしいでしょうか？",
                ()=> 
                {
                    read_object = Instantiate(read_object);
                },
                ()=>
                {
                    read_object = null;
                    return;
                }
                );
        }
        //ヒエラルキーに配置していないときに配置を断った際に中身がなくなるためのチェック
        if (null == read_object)
        {
            return;
        }
        anime_controller = new Controler_AnimationEffectData(new AnimationEffectData(read_object));

    }
#endregion //読み込み部分


    /// <summary>
    /// 読み込んだ後の画面
    /// </summary>
    private void state_exist_data()
    {
        Label("読み込み完了");
        ScrollVirticalBlock(ref timeline_view_scroll_pos, timeline_view, GUILayout.ExpandWidth(true),GUILayout.ExpandHeight(true));
    }
#region 描画部分
    /// <summary>
    /// タイムラインを描画する箇所
    /// </summary>
    private void timeline_view()
    {
        particlesystem_timeline_view();
        linerenderer_timeline_view();
        trailrenderer_timeline_view();
    }

    /// <summary>
    /// パーティクルのためのタイムラインビュー
    /// </summary>
    private void particlesystem_timeline_view()
    {
        List<ParticleSystem> _particles = anime_controller.ParticleSystems;
        if(_particles.Count <= 0)
        {
            return;
        }

        for(int _i = 0; _i < _particles.Count;_i++)
        {
           
            float _now_time = TimeLineSliderFixedMemory(
                _particles[_i].time,
                0.0f,
                _particles[_i].duration,
                1.0f,
                GUILayout.ExpandWidth(true), GUILayout.Height(50.0f)
                );
            _particles[_i].Simulate(_now_time);
           

        }
    }

    /// <summary>
    /// ラインレンダラーのタイムラインビュー
    /// 現状は、パーティクルアニメーションだけのビューのみ
    /// </summary>
    private void linerenderer_timeline_view()
    {

    }


    /// <summary>
    /// トレイルのためのビュー
    /// 現状は、パーティクルアニメーションだけのビューのみ
    /// </summary>
    private void trailrenderer_timeline_view()
    {

    }

#endregion //描画部分


}


/// <summary>
/// アニメーションをコントロールする為のもの
/// </summary>
public class Controler_AnimationEffectData
{
    /// <summary>
    /// オブジェクトに読み込みされているものがあるかどうか？
    /// </summary>
    public bool is_read_success
    {
        get
        {
            if(null == effect_data)
            {
                return false;
            }
            return effect_data.is_read;
        }
    }

    /// <summary>
    /// パーティクルシステムを取得する為のもの
    /// </summary>
    public List<ParticleSystem> ParticleSystems
    {
        get
        {
            return effect_data.ParticleSyastems;
        }
    }

    /// <summary>
    /// ラインレンダラーを取得する為のプロパティ
    /// </summary>
    public List<LineRenderer> Linerenderers
    {
        get
        {
            return effect_data.LineRenderers;
        }
    }

    /// <summary>
    /// トレイルレンダラーを取得する為のプロパティ
    /// </summary>
    public List<TrailRenderer> Trailrenderers
    {
        get
        {
            return effect_data.TrailRenderer;
        }
    }

    //読み込んだアニメーションのデータ群
    AnimationEffectData effect_data;

    public Controler_AnimationEffectData(AnimationEffectData _data)
    {
        effect_data = _data;
    }

    /// <summary>
    /// 再びの読み込み　
    /// TODO:再読み込みの際にデータなどを一度綺麗にする処理が必要　 
    /// </summary>
    /// <param name="_root"></param>
    public void RereadEffectData(GameObject _root)
    {

    }

    /// <summary>
    /// ユニティによるアップデート内で呼び出される。
    /// </summary>
    public void EffectUpdate()
    {
        if(null == effect_data)
        {
            return;
        }

        List<ParticleSystem> _list = ParticleSystems;
        float _now_time = 0;
        for(int _i = 0;_i < _list.Count;_i++)
        {
            _now_time = _list[_i].time;
            _list[_i].Simulate(_now_time);
            _list[_i].Pause();
        }

        //TODO:LineRendererとかも行う。

    }
}



#endif