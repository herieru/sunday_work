﻿///<summary>
/// 概要：コンソール部分の自分用の便利に使用する為のクラス。
/// 暇な時にでもやってください。
/// 
/// 
///　タグが追加出来て、その中でもこの区間みたいな作り物を作成したいお
///
/// <filename>
/// 
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
namespace HackDebug
{
    public class ConsolDebugger : EditorEasyBase
    {
        [MenuItem("Tools/DebugLogWindow")]
        public static void OpenWindow()
        {
            var _window = GetWindow<ConsolDebugger>();
        }
        GameObject _obj = null;

        private void polly_test()
        {
            if (null != _obj)
            {
                return;
            }

            _obj = new GameObject("test_debug");
            var _mesh_renderer = _obj.AddComponent<MeshRenderer>();
            var _mesh_filter = _obj.AddComponent<MeshFilter>();
            Mesh _mesh = new Mesh();
            _mesh_filter.mesh = _mesh;

            _mesh.vertices = new Vector3[3]
            {
            new Vector3(-10, 100, 0),
            new Vector3(0, 110, 0),
            new Vector3(10, 100, 0),
            };

            _mesh.SetIndices(new int[3] { 0, 1, 2 }, MeshTopology.Triangles, 0);

            // 頂点データより法線を再計算する
            _mesh.RecalculateNormals();

            _mesh_renderer.material.SetColor("_TintColor", Color.white);
        }

        public ConsolDebugger()
        {
 
        }

        #region インナ‐クラス
        private class TagData
        {
            //デストラクタ
            ~TagData()
            {
                log.Clear();
            }

            //タグとしての表示・非表示
            public bool tag_display { get; set; }

            private List<LogData> log = new List<LogData>();

            public List<LogData> LogDatas { get { return log; } }
            public LogData AddLog { set { log.Add(value); } }

            /// <summary>
            /// ディスプレイしているかどうかの情報をクリアする。
            /// </summary>
            public void DisplayinfoClear()
            {

            }
        }

        //ログのデータ
        private class LogData
        {

            public LogData(string[] _tags, string _log)
            {
                foreach (var _tag in _tags)
                {
                    tags.Add(_tag);
                }

                LogStr = _log;

                AlreadyDisplay = false;
            }

            //同じログとしてすでに表示をしているか？
            public bool AlreadyDisplay { get; set; }

            public string LogStr { get; set; }

            //これはいらないかも？
            public List<string> tags = new List<string>();

        }

        #endregion

        /// <summary>
        /// ＜タグ名、そのタグに沿ったログ文章＞　　ログの文章とかも、もしかしたら、Dictionaryの方がいいかも？
        /// </summary>
        Dictionary<string, TagData> dic_bytag = new Dictionary<string, TagData>();
        private Vector2 log_scroll = new Vector2();
        private Vector2 tag_scroll = new Vector2();
        private bool reset = true;

        private void EasySetTag(string _log, params string[] _tags)
        {
            SetLog(_tags, _log);
        }

        private bool debug_mode = false;
        private string serch_window = "";

        private void Update()
        {

        }

        private int _selected = 0;
        private void OnGUI()
        {
            ToolbarCheakChanges(ref _selected, new string[] { "A","B"}, sample_button, tool_display);


            if (false == DebugPoster.Instance.ExitstNotification() &&
                (EditorApplication.isPlaying || EditorApplication.isPaused))
            {
                Button("通知を行う", () => { DebugPoster.Instance.SetNotification(SetLog); }, GUILayout.ExpandWidth(true));
            }
            else
            {
                tool_display();
                SimpleBorderLineHorizontal();
                BlockVertical("", display_log_and_tag);
                display_log_button();
            }
        }

        private void sample_button()
        {
            Button("ポリゴン生成できるかテスト", polly_test, GUILayout.ExpandWidth(true));
        }

        /// <summary>
        /// ツールや、変更系のボタンに関するまとめているものです。
        /// </summary>
        private void tool_display()
        {
            EditorGUILayout.BeginHorizontal();
            {
                serch_window = SerchWindow(serch_window, GUILayout.ExpandWidth(true));
                Button("ログのリセット", log_reset, GUILayout.ExpandWidth(true));
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// ログとタグのセットを表示する。　横向き
        /// </summary>
        private void display_log_and_tag()
        {
            EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
            {
                BlockVertical("", scroll_tags_display);
                ScrollVirticalBlock(ref log_scroll, scroll_log_display, GUILayout.ExpandWidth(true));
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// セットされたログを全て除去していく。
        /// </summary>
        private void log_reset()
        {
            foreach (var _key in dic_bytag.Keys)
            {
                TagData _data = null;
                if (false == dic_bytag.TryGetValue(_key, out _data))
                {
                    continue;
                }
                _data.LogDatas.Clear();

            }
            dic_bytag.Clear();
            display_naiyou = "";
        }

        /// <summary>
        /// スクロールの中身描画
        /// タグの中にあるログデータがまだ1度も表示されていなければ表示を行う。
        /// </summary>
        private void scroll_log_display()
        {

            log_display_flg_clear();
            
            //実際の表示部分
            foreach (var _key in dic_bytag.Keys)
            {
                TagData _data = null;
                if (false == dic_bytag.TryGetValue(_key, out _data))
                {
                    continue;
                }

                //ログ表示を行わないってなったら、表示しない。
                if (false == _data.tag_display)
                {
                    continue;
                }

                //ログの表示
                foreach (var _log in _data.LogDatas)
                {
                    //既に表示済みだったら、表示しない
                    if (_log.AlreadyDisplay)
                    {
                        continue;
                    }

                    //検索状態
                    if (serch_window.Length > 0)
                    {//検索を行う内容が存在するか？
                        if (false == _log.LogStr.Contains(serch_window))
                        {
                            continue;
                        }
                    }

                    //実際の表示部分
                    //特に問題がなければ、そのまま出力
                    EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
                    {
                        Button(_log.LogStr, () => { display_naiyou = _log.LogStr; Repaint(); }, GUILayout.ExpandWidth(true));
                        _log.AlreadyDisplay = true;
                    }
                    EditorGUILayout.EndVertical();
                }
            }
        }

        /// <summary>
        /// ログを一度でも表示を行ったというものの
        /// </summary>
        private void log_display_flg_clear()
        {
            //一度全部の表示したという記録を全て消す
            //->同じログデータを複数のタグの所で保管しているため。
            foreach (var _key in dic_bytag.Keys)
            {
                TagData _data = null;
                if (false == dic_bytag.TryGetValue(_key, out _data))
                {
                    continue;
                }
                //ログの表示設定をクリア
                foreach (var _log in _data.LogDatas)
                {
                    _log.AlreadyDisplay = false;
                }
            }
        }

        /// <summary>
        /// タグ名などのディスプレイ表示・非表示部分の制御
        /// </summary>
        private void scroll_tags_display()
        {
            EditorGUILayout.BeginVertical(GUILayout.Width(200.0f));
            {
                switting_tag_button();//この関数は表示部分じゃない。
                ScrollVirticalBlock(ref tag_scroll, display_tags, GUILayout.ExpandWidth(true));
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// タグの全消し、全表示の切り替え
        /// </summary>
        private void switting_tag_button()
        {

            if (cheak_tag_status())
            {
                reset = false;
            }

            //全て表示を行う。
            bool _reset = Toggle(reset, reset ? "全て隠す" : "全て表示", "Button", null, GUILayout.ExpandWidth(true));
            if (_reset != reset)
            {
                ChangeDisplay(_reset);
            }
        }

        private void ChangeDisplay(bool _reset)
        {
            reset = _reset;
            foreach (var _tag_name in dic_bytag.Keys)
            {
                TagData _data = null;
                if (false == dic_bytag.TryGetValue(_tag_name, out _data))
                {
                    continue;
                }
                _data.tag_display = reset;
            }
        }

        /// <summary>
        /// 全てのタグについているチェックボックスの状態を確認して、一つでも、
        /// TODO:クラスにデータ部分を分ける際には、この部分は分けることが出来る。
        /// </summary>
        /// <returns></returns>
        private bool cheak_tag_status()
        {
            foreach (var _tag_name in dic_bytag.Keys)
            {
                TagData _data = null;
                //ここはあまり意味ないけど、一応の措置
                if (false == dic_bytag.TryGetValue(_tag_name, out _data))
                {
                    continue;
                }

                if (false == _data.tag_display)
                {
                    return true;
                }

            }
            return false;
        }

        /// <summary>
        /// タグによる表示・非表示
        /// </summary>
        private void display_tags()
        {
            foreach (var _tag_name in dic_bytag.Keys)
            {
                TagData _data = null;
                if (false == dic_bytag.TryGetValue(_tag_name, out _data))
                {
                    continue;
                }
                _data.tag_display = Toggle(_data.tag_display, _tag_name, null, GUILayout.ExpandWidth(true));
            }
        }


        /// <summary>
        /// 外部から、ログが仕込まれるところの箇所
        /// </summary>
        /// <param name="_logs_name"></param>
        /// <param name="_log"></param>
        private void SetLog(string[] _tags, string _log)
        {
            Debug.LogWarning("タグの数は" + _tags.Length);
            if (_tags.Length == 0)
            {
                _tags = new string[1] { "タグ無し" };
            }


            LogData _log_data = new LogData(_tags, _log);

            //まず、そのログが存在するかの確認
            foreach (var _tag_name in _tags)
            {
                TagData _tag_data = null;

                if (dic_bytag.ContainsKey(_tag_name))
                {
                    //見つかった場合
                    if (false == dic_bytag.TryGetValue(_tag_name, out _tag_data))
                    {//まぁ、たぶん来ない
                        continue;
                    }
                    _tag_data.AddLog = _log_data;

                }
                else
                {
                    //新規にタグデータを作成してそこに入れる。
                    _tag_data = new TagData();
                    _tag_data.AddLog = _log_data;
                    //辞書に保管
                    dic_bytag.Add(_tag_name, _tag_data);

                }

            }
            ChangeDisplay(true);
            Repaint();

        }

        private string display_naiyou = "";

        /// <summary>
        /// ログのボタンの内容が押された時に表示される。
        /// </summary>
        private void display_log_button()
        {
            display_naiyou = EditorGUILayout.TextField(display_naiyou, GUILayout.ExpandWidth(true), GUILayout.Height(100.0f));
        }

    }
}
#endif
