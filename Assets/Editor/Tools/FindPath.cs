﻿///<summary>
/// 概要：
///
/// <filename>
/// FindPath.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FindPath : EditorEasyBase 
{
    [MenuItem("Tools/FindPaths")]
    private static void open_tool()
    {
        GetWindow<FindPath>();
    }

    Object serch_object = null;
    string path = "";

    private void OnGUI()
    {
        Label("パスを調べたいオブジェクトを参照として入れてください");
        var _new_set_object = EditorGUILayout.ObjectField(serch_object, typeof(Object), true, GUILayout.ExpandWidth(true));
        EditorGUILayout.TextField(path, GUILayout.ExpandWidth(true));

        if(GUI.changed)
        {
            serch_object = _new_set_object;
            path = AssetDatabase.GetAssetPath(serch_object);
        }
    }

}
