﻿///<summary>
/// 概要：ツールの最初の見える部分、
/// この部分に関しては、本当にシンプルに
/// SwitchingDisplayクラスにビューの選択等を行ってもらう
/// 
///
/// <filename>
/// NortsCreaterView.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace NortsCreator
{

    public class NortsCreaterView : EditorEasyBase
    {
        [MenuItem("Tools/Music/NortsCreator")]
        private static void open_tool()
        {
            var _window = GetWindow<NortsCreaterView>();
            _window.Init();
        }

        private StateSwithing switch_display = null;

        private void Init()
        {
            switch_display = new StateSwithing();
        }

        private void OnGUI()
        {
            if(null == switch_display)
            {
                return;
            }
            switch_display.Display();
        }
    }
}
