﻿///<summary>
/// 概要：一つの個体のビュー
/// EditNortsの一つの単位を編集するためのもの
/// ここでは、秒数を細かくいじったり、タイプを変更したり、
/// ロングノーツの間を編集したりする。
/// 
/// このビューはあくまでも、補助的な役割として、存在しているため
/// メニューから開く事はない。
///
/// <filename>
/// PartsEditView.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace NortsCreator
{

    public class PartsEditView : EditorEasyBase
    {
        /// <summary>
        /// 編集を行うためのエディットノーツデータ
        /// </summary>
        private EditNortsData edit_norts_data = null;
        private DataAccesser data_accesser = null;
        private int between_add_num = 0;
        private Vector2 scroll_pos;

        private int undo_num = 0;

        /// <summary>
        /// 初期化　これを呼ぶ必要がかならずある
        /// </summary>
        /// <param name="_edit_data">編集する、ノーツデータ</param>
        /// <param name="_access">データへのアクセスするもの主にスクロール情報等</param>
        public void Init(EditNortsData _edit_data, DataAccesser _access)
        {
            undo_num = 0;
            scroll_pos = Vector2.zero;
            edit_norts_data = _edit_data;
            data_accesser = _access;
        }

        private void undo_callback()
        {
            Debug.Log("呼ばれている");
            undo_num++;
        }

        public void OnGUI()
        {
            if (null == edit_norts_data)
            {
                return;
            }
            EditorGUI.BeginChangeCheck();
            ScrollVirticalBlock(ref scroll_pos,
            () => 
            {
                menu_place();
                touch_id_display();
                change_lane_edit();
                start_time_edit_display();
                end_time_edit_display();
                between_edit_display();
            });
            EditorGUI.EndChangeCheck();
        }


        /// <summary>
        /// メニューを表示する場所
        /// </summary>
        private void menu_place()
        {
            BlockHorizontal("", () => 
            {
                //Button("適用する", data_update, GUILayout.ExpandWidth(true));
                Button("削除する", data_delete, GUILayout.Width(50f));
            },
            GUILayout.ExpandWidth(true));
        }

        /// <summary>
        /// タッチタイプを描画する
        /// </summary>
        private void touch_id_display()
        {
            Label("タッチタイプ:" + edit_norts_data.Norts.NortsTypeId);
        }

        /// <summary>
        /// 最初のレーンの切り替え
        /// </summary>
        private void change_lane_edit()
        {
            Label("レーンID");
            int _lane_id = EditorGUILayout.IntSlider(edit_norts_data.Norts.StartLaneID, 0, data_accesser.NowLaneMax - 1);
            if (GUI.changed)
            {
                edit_norts_data.Norts.StartLaneID = _lane_id;
                data_accesser.UpdateEditData(UpdateDataType.START_DATA, edit_norts_data);
            }
        }

        /// <summary>
        /// スタート位置の調整
        /// </summary>
        private void start_time_edit_display()
        {
            float _new_start_time = EditorGUILayout.FloatField("開始時間",edit_norts_data.Norts.StartTime, GUILayout.ExpandWidth(true));
            if (GUI.changed)
            {
                if(_new_start_time <= 0)
                {
                    return;
                }
                edit_norts_data.Norts.StartTime = _new_start_time;
                data_accesser.UpdateEditData(UpdateDataType.START_DATA, edit_norts_data);
            }

        }


        /// <summary>
        /// エンド側の編集
        /// </summary>
        private void end_time_edit_display()
        {
            if(false == edit_norts_data.Norts.isLongNorts)
            {
                return;
            }
            float _new_end_time = 0;
            int _lane_id = 0;
            BlockHorizontal("", () =>
            {
                Label("終端レーンID");
                _lane_id = EditorGUILayout.IntSlider(edit_norts_data.Norts.EndLaneID, 0, data_accesser.NowLaneMax - 1);
            });
            
            _new_end_time = EditorGUILayout.FloatField("終了時間",edit_norts_data.Norts.EndTime, GUILayout.ExpandWidth(true));
           

            if (GUI.changed)
            {
                edit_norts_data.Norts.EndLaneID = _lane_id;
                if (edit_norts_data.Norts.StartTime <= _new_end_time)
                {
                    edit_norts_data.Norts.EndTime = _new_end_time;
                }
                data_accesser.UpdateEditData(UpdateDataType.END_DATA,edit_norts_data);
            }

        }


        /// <summary>
        /// 中間地点の位置の調整
        /// </summary>
        private void between_edit_display()
        {
            if(false == edit_norts_data.Norts.isLongNorts &&
                false == edit_norts_data.Norts.isExistBetWeenData)
            {
                return;
            }
            Space();

            //これはただのかざり
            //Toggle(edit_norts_data.Norts.isExistBetWeenData, "中間ノーツ", null, GUILayout.ExpandWidth(true));
            var _between_list = edit_norts_data.Norts.BetWeenData;

            int _ptr = 0;

            foreach (var _list in _between_list)
            {
                BlockHorizontal("Lane" + _ptr, () => 
                {
                    Label("レーンID");
                    int _lane_id = EditorGUILayout.IntSlider(_list.LaneID, 0, data_accesser.NowLaneMax - 1);
                    Label("タイミング");
                    float _new_end_time = EditorGUILayout.FloatField(_list.exist_point_time, GUILayout.ExpandWidth(true));
                    if (GUI.changed)
                    {
                        //ここは本当はコントローラか何か使った方がいい気がする。
                    }

                    Button("削除",()=> { edit_norts_data.DeleteBetWeenNortData(_ptr); }, GUILayout.ExpandWidth(true));
                    _ptr++;
                },
                GUILayout.ExpandWidth(true));

                SimpleBorderLineHorizontal();

            }



            add_between_command();
        }

        /// <summary>
        /// 中間部分を生成するためのもの
        /// </summary>
        private void add_between_command()
        {
            BlockHorizontal("",
            () => 
            {
                Label("追加個数");
                between_add_num = EditorGUILayout.IntField(between_add_num, GUILayout.ExpandWidth(true));
                Button("追加する", add_between_norts, GUILayout.ExpandWidth(true));
            },
            GUILayout.ExpandWidth(true));
        }

        /// <summary>
        /// 中間のノーツを追加する。
        /// </summary>
        private void add_between_norts()
        {
            data_accesser.AddBetWeenData(edit_norts_data, between_add_num, DisplayErrorMessage);
        }

        /// <summary>
        /// 中間ノーツの作成を失敗した時のメッセージ
        /// </summary>
        private void DisplayErrorMessage()
        {
            DisplayDialog("追加失敗", "中間ノーツを配置するには、区間が短すぎます。", null, null);
        }

        /// <summary>
        /// フォーカスが離れた時の処理
        /// </summary>
        private void OnLostFocus()
        {
            close_window();
        }

        /// <summary>
        /// このデータを更新する。
        /// </summary>
        private void data_update()
        {
            close_window();
        }


        /// <summary>
        /// このデータを削除する。
        /// </summary>
        private void data_delete()
        {
            data_accesser.DeleteEditNorts(edit_norts_data);
            close_window();
        }


        /// <summary>
        /// このウィンドウを閉じる
        /// </summary>
        private void close_window()
        {
            GetWindow<PartsEditView>().Close();
        }
    }
}
