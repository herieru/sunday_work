﻿///<summary>
/// 概要:ここに関しては、
///
/// <filename>
/// ModeSelectView.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace NortsCreator
{
    public class ModeSelectView : ViewBase
    {
        /// <summary>
        /// 初期の遷移選択状態
        /// </summary>
        [SerializeField]
        private NortsToolState prev_current;

        public ModeSelectView()
        {
            prev_current = NortsToolState.None;
        }

        /// <summary>
        /// カレントの情報が変更する際に必要なもの
        /// </summary>
        /// <param name="action"></param>
        public override void SetNotice(Action<NortsToolState> _action)
        {
            change_state_notice = _action;
            //TODO：最終的に消す デバック時間短縮
            change_state_notice(NortsToolState.Creater);
        }

        public override void Init()
        {
           
        }


        /// <summary>
        /// Enumでの状態の選択を行う。
        /// </summary>
        public override void MenuSpace()
        {
            Label("何を行うか？");
            BlockHorizontal("", () => 
            {
                var _select = EditorGUILayout.EnumPopup(prev_current, GUILayout.ExpandWidth(true));

                if(GUI.changed)
                {
                    prev_current = (NortsToolState)_select;
               
                }
                Button("この作業を行う",()=> 
                {
                   if(null != change_state_notice)
                   {
                        change_state_notice(prev_current);
                   }
                }
                );
            });

        }


        public override void TimeLineSpace()
        {
            
        }

        public override void PlayMusicSpace()
        {
            
        }

        public override void EventUpdate()
        {

        }


    }
}
