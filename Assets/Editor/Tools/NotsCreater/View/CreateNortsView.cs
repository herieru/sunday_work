﻿///<summary>
/// 概要：ノーツを作成するためのViewです。
/// 主に作成などを全部司ります。
///
/// <filename>
/// CreateNortsView.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace NortsCreator
{
    public class CreateNortsView : ViewBase
    {
        ///<summary>スクロールの位置</summary>
        private Vector2 scroll_pos;
        /// <summary>
        /// スクロールの大きさなどを取得するためのもの
        /// </summary>
        private Rect scroll_rect;

        public override void Init()
        {
            scroll_pos = Vector2.zero;
        }

        public override void SetNotice(Action<NortsToolState> _action)
        {
            change_state_notice = _action;
        }

        /// <summary>
        /// データの変更に対して、色々設定する。
        /// </summary>
        /// <param name="_access"></param>
        public override void DataBankAccessSet(DataAccesser _access)
        {
            base.DataBankAccessSet(_access);
            data_access.ChangeStateNotice(repaint_gui);
        }

        /// <summary>
        /// 再描画を行う。
        /// </summary>
        private void repaint_gui()
        {
            Repaint();
        }

        /// <summary>
        /// 音源のデータを入れる所
        /// </summary>
        public override void MenuSpace()
        {
            BlockHorizontal("", () => 
            {
                Label("基準となる音楽");
                AudioClip _set_clip = (AudioClip)EditorGUILayout.ObjectField(data_access.AudioData, 
                    typeof(AudioClip),true, GUILayout.ExpandWidth(true));

                if(GUI.changed && null != _set_clip)
                {
                    data_access.AudioData = _set_clip;
                }

                Button("書き出し", WriteAssetData, GUILayout.ExpandWidth(true));
            },
            GUILayout.ExpandWidth(true));

            if (null == data_access.AudioData)
            {
                return;
            }

            BlockHorizontal((""),() => 
            {
                GUILayout.FlexibleSpace();
                int _bpm = SimpleEcrementParts("BPM", data_access.BPM, 1);
                int _lpb = SimpleEcrementParts("LPB", data_access.LPB, 1);
                
                if(GUI.changed)
                {
                    data_access.BPM = _bpm;
                    data_access.LPB = _lpb;
                }
            });

        }



        /// <summary>
        /// タイムラインでの値を設定
        /// </summary>
        public override void TimeLineSpace()
        {
            if (null == data_access.AudioData)
            {
                return;
            }
            timeline_scroll();
        }

        /// <summary>
        /// タイムラインのスクロール部分の処理
        /// </summary>
        private void timeline_scroll()
        {
            BorderLine(GUILayout.ExpandWidth(true), GUILayout.Height(1f));
            Rect _rect = ScrollHorizontalBlock(ref scroll_pos, () => 
            {
                //スクロールのサイズを横幅を大きくするためのもの
                Label("", GUILayout.Width(data_access.ScrollSize_X));
                
                draw_staff_score_on_scroll();
                draw_now_time_line_on_scroll();
                draw_edit_norts_on_scroll();

            },
            GUILayout.ExpandWidth(true), GUILayout.Height(300.0f));

            scroll_rect = _rect;
        }

        /// <summary>
        /// エディットノーツを描画する。
        /// </summary>
        private void draw_edit_norts_on_scroll()
        {
            //スクロールのノーツとかの中身。
            foreach (var _edit_nort_data in data_access.DisplayDatas)
            {
                draw_start_norts(_edit_nort_data);
                draw_long_norts(_edit_nort_data);
                draw_between_norts(_edit_nort_data);
                draw_line_on_longnorts(_edit_nort_data);
            }
        }

        /// <summary>
        /// スタートとなる位置の描画を行う。
        /// </summary>
        /// <param name="_edit_norts_data"></param>
        private void draw_start_norts(EditNortsData _edit_nort_data)
        {
            if (GUI.Button(new Rect(_edit_nort_data.EditorStartPos - (_edit_nort_data.EditNortsSize / 2), _edit_nort_data.EditNortsSize)
                    , "♪", get_style(_edit_nort_data.display_style)))
            {
                var _window = GetWindow<PartsEditView>();
                _window.Init(_edit_nort_data, data_access);
            }
        }

        /// <summary>
        /// ロングノーツの描画
        /// スタートー最後の描画と　それの間の線の描画
        /// </summary>
        /// <param name="_edit_norts_data"></param>
        private void draw_long_norts(EditNortsData _edit_norts_data)
        {
            if(false == _edit_norts_data.Norts.isLongNorts)
            {
                return;
            }

            //実質データが入っていない状態なので描画しない
            if(_edit_norts_data.Norts.StartTime == _edit_norts_data.Norts.EndTime)
            {
                return;
            }

            GUI.Button(
                new Rect(
                    _edit_norts_data.EditorEndPos - (_edit_norts_data.EditNortsSize / 2),
                    _edit_norts_data.EditNortsSize
                    ),
                "あ", get_style(_edit_norts_data.display_style));
        }

        /// <summary>
        /// 中間のノーツのデータ
        /// </summary>
        /// <param name="_edit_norts_data"></param>
        private void draw_between_norts(EditNortsData _edit_norts_data)
        {
            if (false == _edit_norts_data.Norts.isLongNorts)
            {
                return;
            }

            foreach (var _between_pos in _edit_norts_data.EditorBetweenNortsPosList)
            {
                GUI.Button(
               new Rect(
                   _between_pos - (_edit_norts_data.EditNortsSize / 2),
                   _edit_norts_data.EditNortsSize /2
                   ),
               "♬", get_style(_edit_norts_data.display_style));
            }
        }


        /// <summary>
        /// ロングノーツの際に、それらを線で結ぶための物
        /// </summary>
        /// <param name="_edit_norts_data"></param>
        private void draw_line_on_longnorts(EditNortsData _edit_norts_data)
        {
            if(false == _edit_norts_data.Norts.isLongNorts)
            {
                return;
            }


            Vector2 _start_pos = _edit_norts_data.EditorStartPos;
            Vector2 _next_pos = _start_pos;

            //中間ノーツが存在すれば、それに対して、表示を行う
            if(_edit_norts_data.Norts.isExistBetWeenData)
            {
                int _between_norts_count = _edit_norts_data.EditorBetweenNortsPosList.Count;

                if(_between_norts_count >= 2)
                {
                    Debug.Log("aaa");
                }

                for(int _i = 0; _i < _between_norts_count;_i++)
                {
                    _next_pos = _edit_norts_data.EditorBetweenNortsPosList[_i];
                    FreeDrawColorLine(_start_pos, _next_pos, Color.green);
                    _start_pos = _next_pos;
                }
            }

            //まだ入力途中のため
            if(false == _edit_norts_data.ProcessComplete)
            {
                return;
            }

            _next_pos = _edit_norts_data.EditorEndPos;
            FreeDrawColorLine(_start_pos, _next_pos, Color.green);
        }



        /// <summary>
        /// 五線譜的なものを描画する。  TODO:BPM LPBの設定にする。
        /// </summary>
        private void draw_staff_score_on_scroll()
        {
            float _start_bpm_line_x = 0.0f;

            //縦線の描画　BPM単位
            for (int _bpm_line_no = 0; _bpm_line_no < data_access.DrawCount_on_music_length; _bpm_line_no++)
            {
                //開始位置を定める
                _start_bpm_line_x = data_access.BPMWidth * _bpm_line_no;

                FreeDrawLine(
                    new Vector2(_start_bpm_line_x, 0f),
                    new Vector2(_start_bpm_line_x, data_access.ScrollSize_Y));

                
                
                
                //縦線の描画　LPM単位
                for(int _lpb_line_no = 1; _lpb_line_no <= data_access.LPB;_lpb_line_no++)
                {
                    FreeDrawColorLine(
                        new Vector2(_start_bpm_line_x + _lpb_line_no * data_access.LPB_Width, 0f),
                        new Vector2(_start_bpm_line_x + _lpb_line_no * data_access.LPB_Width, data_access.ScrollSize_Y),
                        Color.gray);
                }
               

            }

            



            //横線のもの
            for (int _i = 0; _i < data_access.NowLaneMax; _i++)
            {
                FreeDrawColorLine(
                    new Vector2(0, data_access.ScrollLineHeightWidth * _i + data_access.ScrollLineCorrectionWidth),
                    new Vector2(data_access.ScrollSize_X, data_access.ScrollLineHeightWidth * _i + data_access.ScrollLineCorrectionWidth),
                    Color.yellow
                    );
            }
        }

        /// <summary>
        /// 現在の再生位置を表示する。
        /// </summary>
        private void draw_now_time_line_on_scroll()
        {
            //再生位置の表示
            FreeDrawColorLine(
                    new Vector2(data_access.NowPlayTimeWidthPos, 0),
                    new Vector2(data_access.NowPlayTimeWidthPos, data_access.ScrollSize_Y),
                    Color.red
                    );
        }
        

        /// <summary>
        /// 音楽の再生等を行う。
        /// </summary>
        public override void PlayMusicSpace()
        {
            if(null == data_access.AudioData)
            {
                return;
            }

            float _play_now = data_access.play_now_time;
            _play_now = TimeLineSliderFixedMemory(_play_now, 0, data_access.AudioData.length,10.0f, GUILayout.ExpandWidth(true));

            BlockHorizontal("", () => 
            {
                GUILayout.FlexibleSpace();
                int _fun = (int)(_play_now / 60f);
                int _byou = (int)(_play_now % 60.0f);

                string _play_track_time = String.Format("{0}:{1}", _fun, _byou);
                Label(_play_track_time, GUILayout.ExpandWidth(true));
                GUILayout.FlexibleSpace();
            },
            GUILayout.ExpandWidth(true));


            if(GUI.changed)
            {
                data_access.play_now_time = _play_now;
            }

            music_controller();
        }

        /// <summary>
        /// 音楽などの再生などを行う為のコントローラ部分の描画
        /// </summary>
        private void music_controller()
        {
            Label("まだ未完成");
            BlockHorizontal("",
                () =>
                {
                    Button("<<", null, GUILayout.Width(100.0f));
                    Button("▷", null, GUILayout.Width(200.0f));
                    Button(">>", null, GUILayout.Width(100.0f));
                }, GUILayout.ExpandWidth(true));
        }

        /// <summary>
        /// マウスなどのイベントのアップデート
        /// </summary>
        public override void  EventUpdate()
        {

            if (null == data_access.AudioData)
            {
                return;
            }
            mouse_event();
        }

        /// <summary>
        /// マウスのイベント
        /// </summary>
        private void mouse_event()
        {
            if(Event.current.type != EventType.MouseDown)
            {
                return;
            }

            int _mouse_button = Event.current.button;
            //スクロールの範囲内のみのタッチで判定を行いたい溜め
            Vector2 _touch_pos = Event.current.mousePosition+ scroll_pos - scroll_rect.position;
            Debug.Log("mouse_button_type:" + _mouse_button);
            Debug.Log("mouse_pos :" + _touch_pos);
            
            //左０　右１　真ん中２
            data_access.CreateEditorNorts(_mouse_button, _touch_pos );

            Repaint();
        }

        /// <summary>
        /// アセットとして書き出す。
        /// </summary>
        private void WriteAssetData()
        {
            string _asset_name_base = data_access.AudioData.name;

            var _data = data_access.GetNeedPropatyOnly();

            IntegratedNortsData _big_norts_data = CreateAssetData();
            AssetDatabase.CreateAsset(_big_norts_data, "Assets/Resources/Audio/Bgm/" + _asset_name_base + ".asset");


            AssetDatabase.SaveAssets();
        }


        /// <summary>
        /// DataAccesserにあるデータを元に、アセットを作成する。
        /// </summary>
        /// <returns></returns>
        private IntegratedNortsData CreateAssetData()
        {
            IntegratedNortsData _big_norts_data = ScriptableObject.CreateInstance<IntegratedNortsData>();
            

            List<PopNortsData> _norts_datas = new List<PopNortsData>();

            var _actual_datas = data_access.DisplayDatas;
           

            for(int _i = 0;_i < _actual_datas.Length;_i++)
            {
                PopNortsData _pop_norts_data = new PopNortsData();
                _pop_norts_data = CreateNortData(_actual_datas[_i].Norts);
                _norts_datas.Add(_pop_norts_data);

            }

            _big_norts_data.norts_data = _norts_datas.ToArray();

            return _big_norts_data;
        }


        /// <summary>
        /// エディターのノーツから、ノーツのデータを作成する
        /// </summary>
        /// <param name="_edit_norts"></param>
        /// <returns></returns>
        private PopNortsData CreateNortData(NortsData _norts)
        {
            PopNortsData _data =  new PopNortsData();
            _data.NortsTypeId = _norts.NortsTypeId;
            _data.StartLaneId = _norts.StartLaneID;
            _data.StartTime = _norts.StartTime;
            _data.IsLongNorts = _norts.isLongNorts;
            _data.EndLaneID = _norts.EndLaneID;
            _data.EndTime = _norts.EndTime;
            _data.BetweenNortsData = CreatePopBetWeenData(_norts);

            return _data;
        }


        /// <summary>
        /// 中間ノーツとして吐き出している。
        /// </summary>
        /// <param name="_norts"></param>
        /// <returns></returns>
        private PopBetweenPopData[] CreatePopBetWeenData(NortsData _norts)
        {
            List<PopBetweenPopData> _data = new List<PopBetweenPopData>();
            var _edit_between_datas = _norts.BetWeenData;

            foreach (var _edit_between_data in _edit_between_datas)
            {
                PopBetweenPopData _pop_data = new PopBetweenPopData();
                _pop_data.BetWeenLineID = _edit_between_data.LaneID;
                _pop_data.TimingTime = _edit_between_data.exist_point_time;
                _data.Add(_pop_data);
            }

            return _data.ToArray();
        }




    }

    
}
