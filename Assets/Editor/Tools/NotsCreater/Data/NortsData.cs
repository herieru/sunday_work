﻿///<summary>
/// 概要：ノーツのデータ
/// 基本的には、データだけ。
///
/// <filename>
/// NortsData.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace NortsCreator
{
    [System.Serializable]
    public class NortsData:ScriptableObject
    {
        /// <summary>
        /// ひとつの間に置かれている中間のノーツ
        /// </summary>
        [System.Serializable]
        public class BetweenNortsData:ScriptableObject
        {
            /// <summary>
            /// レーンのID
            /// </summary>
            [SerializeField]
            public int LaneID { get; set; }

            [SerializeField]
            public float exist_point_time { get; set; }

        }

        /// <summary>
        /// ロングノーツの場合に足される値
        /// </summary>
        private readonly float CertainTime = 5.0f;

        

        [SerializeField]
        private int norts_type_id = -1;

        [SerializeField]
        private int start_lane_id;

        [SerializeField]
        private float start_time;

        [SerializeField]
        private bool is_long_norts;

        [SerializeField]
        private int end_lane_id;

        [SerializeField]
        private float end_lane_time;

        [SerializeField]
        private bool is_exist_between;

        /// <summary>
        /// 間に存在するノーツ
        /// </summary>
        [SerializeField]
        private List<BetweenNortsData> between_norts = null;

        #region nortspropaty
        /// <summary>
        /// ノーツのタイプ　int型で無理矢理保持している
        /// </summary>
        public int NortsTypeId{
            get
            {
                return norts_type_id;
            }
            set
            {
                norts_type_id = value;

                if((NortsType)norts_type_id== NortsType.LongNorts)
                {
                    is_long_norts = true;
                }
                else
                {
                    is_long_norts = false;
                }
            }
        }

        /// <summary>
        /// ラインのID　これでどのラインに出るかを計算している
        /// </summary>
        public int StartLaneID { get { return start_lane_id; } set { start_lane_id = value; } }

        public float StartTime { get { return start_time; } set { start_time = value; } }


        

        /// <summary>
        /// ロングノーツかどうか？
        /// </summary>
        public bool isLongNorts
        {
            get
            {
                return ((NortsType)NortsTypeId == NortsType.LongNorts);
            }
        }



        /// <summary>
        /// 最後のレーンか否か？
        /// </summary>
        public int EndLaneID { get { return end_lane_id; } set { end_lane_id = value; } }

        /// <summary>
        /// 最終的な地点
        /// </summary>
        public float EndTime { get { return end_lane_time; } set { end_lane_time = value; } }




        /// <summary>
        /// 中間地点にデータが存在しているかの判断。
        /// </summary>
        public bool isExistBetWeenData
        {
            get
            {
                if (null == between_norts || between_norts.Count <= 0)
                {
                    return false;
                }
                return true;
            }
        }


        /// <summary>
        /// 中間データの個数
        /// </summary>
        public int CountBetweenData
        {
            get
            {
                if (null == between_norts || between_norts.Count <= 0)
                {
                    return 0;
                }

                return between_norts.Count;
            }
        }

        /// <summary>
        /// 最後から見て一つ前のデータを取得する。
        /// </summary>
        public float OneAgoTime
        {
            get
            {
                if(isExistBetWeenData)
                {
                    return between_norts.Last().exist_point_time;
                }
                else
                {
                    return StartTime;
                }

            }
        }


        /// <summary>
        /// 最後と、その一つ前の差分を取得する。
        /// 中間データがある場合はそれの最後のものをデータとして入れる。
        /// </summary>
        public float DiffTimeLastandOneAgo
        {
            get
            {
                float _diff = 0;

                if(isExistBetWeenData)
                {
                    _diff = EndTime - between_norts.Last().exist_point_time;
                }
                else
                {
                    _diff = EndTime - StartTime;
                }

                return _diff;

            }
        }



       /// <summary>
       /// 中間ノーツ
       /// </summary>
       public List<BetweenNortsData> BetWeenData
        {
            get
            {
                return between_norts;
            }
        }
        
        #endregion
        

        /// <summary>
        /// ノーツのコンストラクタ
        /// </summary>
        /// <param name="_norts_type"></param>
        /// <param name="_line_id"></param>
        /// <param name="_norts_start_time_on_music">音源上の音楽時間</param>
        public NortsData(int _norts_type, int _line_id, float _norts_start_time_on_music)
        {
            NortsTypeId = _norts_type;
            StartLaneID = _line_id;

            StartTime = _norts_start_time_on_music;

            EndLaneID = StartLaneID;

            //if (false == isLongNorts)
            //{
            //    EndTime = StartTime;
            //}
            //else
            //{
            //    EndTime = StartTime + CertainTime;
            //}

            between_norts = new List<BetweenNortsData>();

        }



        /// <summary>
        /// 中間のデータを作成 個数単位で後ろの部分が決まってる際に入れる
        /// </summary>
        /// <param name="_add_num"></param>
        /// <param name="_between_time"></param>
        public void AddBetWeenData(int _add_num,float _between_time)
        {
            float _start = OneAgoTime;

            for(int _i = 0;_i < _add_num;_i++)
            {
                BetweenNortsData _data = new BetweenNortsData();
                _data.LaneID = EndLaneID;
                _data.exist_point_time = _start + _between_time;
                _start = _start + _between_time;

                between_norts.Add(_data);
            }
        }

        /// <summary>
        /// 筆のとる中間データをひとつ作成する。
        /// </summary>
        /// <param name="_lane_id"></param>
        /// <param name="_between_time"></param>
        public void AddOwnBetWeenData(int _lane_id,float _between_time)
        {
            BetweenNortsData _between_data = new BetweenNortsData();
            _between_data.LaneID = _lane_id;
            _between_data.exist_point_time = _between_time;
            between_norts.Add(_between_data);
        }



        /// <summary>
        /// マウスのボタンと対応させている　0-2
        /// </summary>
        public enum NortsType
        {
            TouchDown,      //押しただけ
            LongNorts,      //おしっぱなし
            Ping,           //はねる
            Between,        //中間ノーツ
            LongNortsEnd,   //ロングの終端
            Same            //何かと同時押し
        }

        

    }
}
