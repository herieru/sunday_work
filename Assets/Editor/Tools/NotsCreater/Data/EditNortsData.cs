﻿///<summary>
/// 概要：エディター上扱うデータのためのもの
/// 
///
/// <filename>
/// EditNortsData.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
namespace NortsCreator
{

    


    public class EditNortsData:ScriptableObject
    {
        /// <summary>描画する際の大きさ</summary>
        public readonly Vector2 EditNortsSize;

        private NortsData norts;

        /// <summary>ロングノーツだったときの作業状態</summary>
        private bool process_completed;

        /// <summary>
        /// エディタ上のID
        /// </summary>
        public int edit_id { get; private set; }
        
        /// <summary>ノーツのデータ</summary>
        public NortsData Norts { get { return norts; } }


        ///<summary>まだロングノーツなどが編集途中かどうか？</summary>
        public bool ProcessComplete
        {
            get
            {
                if(false == norts.isLongNorts)
                {
                    return true;
                }

                return process_completed;
            }
        }

        /// <summary>
        /// エディター上でのポジション
        /// </summary>
        public Vector2 EditorStartPos { get; set; }

        public Vector2 EditorEndPos { get; set; }

        private List<Vector2> editor_between_norts_pos_list;
        
        public List<Vector2> EditorBetweenNortsPosList
        {
            get { return editor_between_norts_pos_list; }
        }

        /// <summary>
        /// 描画をする、スタイル名定義 ひとまず、仮実装　できればオプションとかで選択出来るとよい。
        /// </summary>
        public string display_style
        {
            get
            {
                switch ((NortsData.NortsType)norts.NortsTypeId)
                {
                    case NortsData.NortsType.TouchDown:
                        return "flow node hex 1";
                    case NortsData.NortsType.LongNorts:
                        return "flow node hex 2";
                    case NortsData.NortsType.Ping:
                        return "flow node hex 3";
                    case NortsData.NortsType.Same:
                        return "flow_node hex 4";
                    case NortsData.NortsType.Between:
                        return "flow_node hex 5";
                    case NortsData.NortsType.LongNortsEnd:
                        return "flow_node hex 6";
                    default:
                        Debug.LogWarning("dont serch type");
                        break;
                }

                return "box";
            }
        }


        /// <summary>
        /// エディター用のノーツタイプ
        /// </summary>
        /// <param name="_edit_id">エディター用のID</param>
        /// <param name="_display_pos">ディスプレイ上の位置</param>
        /// <param name="_norts_type">ノーツのタイプ</param>
        /// <param name="_line_id">ライン上のID</param>
        /// <param name="_music_length">音楽の長さ</param>
        /// <param name="_scroll_size">スクロールの長さ</param>
        public EditNortsData(
            int _edit_id, Vector2 _display_pos, int _norts_type, int _line_id,
            float _music_length, Vector2 _scroll_size, int _lane_num)
        {
            edit_id = _edit_id;
            EditorStartPos = _display_pos;
            float _music_time = PosToTime(_scroll_size, _display_pos, _music_length);
            norts = new NortsData(_norts_type, _line_id, _music_time);

            //ここはロングノーツを入れた際に、同時に終端まで入れる場合の処理
            //if (norts.isLongNorts)
            //{
            //    Vector3 _end_pos = time_to_pos(_music_length, norts.EndTime, _scroll_size, _lane_num, _line_id);
            //    _end_pos.y = EditorStartPos.y;
            //    EditorEndPos = _end_pos;
            //}
            //else
            {
                EditorEndPos = EditorStartPos;
            }
            EditNortsSize = new Vector2(50, 50);
            editor_between_norts_pos_list = new List<Vector2>();


            if(false == norts.isLongNorts)
            {
                process_completed = true;
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="_edit_id">エディタID</param>
        /// <param name="_data">エディットデータ</param>
        public EditNortsData(int _edit_id, NortsData _data)
        {
            edit_id = _edit_id;
            norts = _data;
        }


        /// <summary>
        /// 指定のタイプの描画位置を更新します(追加は含まない。)
        /// </summary>
        /// <param name="_type">変化があったデータタイプ</param>
        /// <param name="_scroll_size">スクロールの大きさ</param>
        /// <param name="_music_length">音楽の長さ</param>
        public void UpdateDisplayData(UpdateDataType _type,Vector2 _scroll_size,float _music_length,int _lane_num)
        {
            switch (_type)
            {
                case UpdateDataType.START_DATA:
                    EditorStartPos = time_to_pos(_music_length, norts.StartTime, _scroll_size, _lane_num, norts.StartLaneID);
                    break;
                case UpdateDataType.END_DATA:
                    EditorEndPos = time_to_pos(_music_length, norts.EndTime, _scroll_size, _lane_num, norts.EndLaneID);
                    break;
                case UpdateDataType.BETWEEN_DATA:
                    if(false == norts.isExistBetWeenData)
                    {
                        return;
                    }
                    break;
            }

        }

        /// <summary>
        /// 中間属性のノーツのデータを作成する。
        /// </summary>
        /// <param name="_scroll_size">スクロールのサイズ</param>
        /// <param name="_music_length">音楽の長さ</param>
        /// <param name="_lane_num">レーンの数</param>
        /// <param name="_lane_id">レーンID</param>
        /// <param name="_display_pos">表示位置</param>
        public void InsertBetweenNorts(
            Vector2 _scroll_size,float _music_length,int _lane_num,
            int _lane_id,Vector2 _display_pos)
        {
            float _timing_time = PosToTime(_scroll_size, _display_pos, _music_length);
            norts.AddBetWeenData(_lane_id, _timing_time);
            editor_between_norts_pos_list.Add(_display_pos);
        }


        /// <summary>
        /// ロングノーツの最後のデータを作成する。
        /// </summary>
        /// <param name="_scroll_size"></param>
        /// <param name="_music_length"></param>
        /// <param name="_lane_num"></param>
        /// <param name="_lane_id"></param>
        /// <param name="_display_pos"></param>
        public void InsertLongNortsEnd(
            Vector2 _scroll_size, float _music_length, int _lane_num,
            int _lane_id, Vector2 _display_pos)
        {
            float _timing_time = PosToTime(_scroll_size, _display_pos, _music_length);
            norts.EndLaneID = _lane_id;
            norts.EndTime = _timing_time;

            EditorEndPos = time_to_pos(_music_length, _timing_time, _scroll_size, _lane_num, _lane_id);
            process_completed = true;
        }

    /// <summary>
    /// 中間データを作成する。 これは、最終地点が決まっている前提での挙動
    /// </summary>
    /// <param name="_add_num">追加する個数</param>
    /// <param name="_capacity_range">許容される1音符の秒間</param>
    /// <param name="_error_action">作成出来ない時に呼び出す必要がある場合は、設定</param>
    public void CreateBetWeenNortsData(
            int _add_num,float _capacity_range,
            Vector2 _scroll_size,float _music_length,int _lane_num,
            System.Action _error_action)
        {
            //それぞれの時間を見て、適切な範囲で行われるか？
            float _between_time = norts.DiffTimeLastandOneAgo / (_add_num  +1);
            //許容される範囲を超えてないかのチェック
            if (_capacity_range >= _between_time)
            {
                if(null != _error_action)
                {
                    _error_action();
                }
                return;
            }
            //Norts－＞EditorNortsという風に作成していく。
            norts.AddBetWeenData(_add_num,_between_time);

            var _between_data = norts.BetWeenData;
            foreach (var _data in _between_data)
            {
                Vector2 _pos = time_to_pos(_music_length,_data.exist_point_time,
                    _scroll_size,_lane_num,_data.LaneID);
                editor_between_norts_pos_list.Add(_pos);
            }
        }


        /// <summary>
        /// 中間データを削除する。
        /// </summary>
        /// <param name="_access_no"></param>
        public void DeleteBetWeenNortData(int _access_no)
        {
            Debug.Log("まだ未実装");
        }


        //以下は、interface作った方がいいかもしれない。
        //TODO:時間＜－－＞位置を求める関数が必要

        /// <summary>
        /// 音楽の時間から、表示上の位置をもとめる
        /// </summary>
        /// <param name="_music_length">対象の音楽の長さ</param>
        /// <param name="_nort_music_time">このノーツの音楽時間</param>
        /// <param name="_scroll_size">スクロールのサイズ</param>
        /// <param name="_lane_num">レーンの数を求める</param>
        /// <param name="_lane_id">レーンのID</param>
        /// <returns></returns>
        private Vector2 time_to_pos(float _music_length,
            float _nort_music_time, Vector2 _scroll_size,
            int _lane_num, int _lane_id)
        {
            //無効な値が入っていないかを確かめる
            if (float.IsNaN(_music_length) && float.IsNaN(_nort_music_time))
            {
                Debug.LogWarning("You dont input invalid_value");
                return Vector2.zero;
            }

            //不動小数点を丸めた上で、横幅を求める。
            float _one_scroll_width = calc_one_width(_scroll_size.x, _music_length) ;
            float _one_scroll_heigt = _scroll_size.y / (float)_lane_num  ;


            float _rtn_pos_y = _one_scroll_heigt * _lane_id + EditNortsSize.y / 2;
            float _rtn_pos_x = _one_scroll_width  * _nort_music_time;

            return new Vector2(_rtn_pos_x, _rtn_pos_y);
        }

        /// <summary>
        /// 位置から、ノーツの音楽時間を求める
        /// </summary>
        /// <param name="_scroll_size">スクロールのサイズ</param>
        /// <param name="_nort_pos">ノーツの位置</param>
        /// <param name="_music_length">音楽の長さ</param>
        /// <returns></returns>
        public float PosToTime(Vector2 _scroll_size, Vector2 _nort_pos, float _music_length)
        {
            //1秒頭の横幅
            float _one_width = calc_one_width(_scroll_size.x, _music_length);

            float _music_time = _nort_pos.x / _one_width;

            return _music_time;
        }


        /// <summary>
        /// スクロールの長さから、音楽の長さに基づいた量を決める
        /// </summary>
        /// <param name="_scroll_x"></param>
        /// <param name="_music_length"></param>
        /// <returns></returns>
        private float calc_one_width(float _scroll_x, float _music_length)
        {
            return _scroll_x / (int)_music_length;
        }


        

    }
}

