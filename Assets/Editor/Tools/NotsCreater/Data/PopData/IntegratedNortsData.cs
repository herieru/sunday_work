﻿///<summary>
/// 概要：掃き出しのための統合されたノーツデータ
/// 基本的にノーツデータをまとめる為のもの
///
/// <filename>
/// IntegratedNortsData.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
namespace NortsCreator
{

    [System.Serializable]
    public class IntegratedNortsData : ScriptableObject
    {
        public PopNortsData[] norts_data;
    }
}
