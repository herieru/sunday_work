﻿///<summary>
/// 概要：掃き出し用のノーツのデータ
///
/// <filename>
/// PopNortsData.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
namespace NortsCreator
{
    [System.Serializable]
    public class PopNortsData
    {
        /// <summary>
        /// ノーツの種類
        /// </summary>
        
        public int NortsTypeId;

        /// <summary>
        /// ノーツの開始レーンID
        /// </summary>
        
        public int StartLaneId;

        /// <summary>
        /// ノーツの開始時間
        /// </summary>
        
        public float StartTime;

        /// <summary>
        /// ロングノーツのデータか？どうか
        /// </summary>
        
        public bool IsLongNorts;

        /// <summary>
        /// 最後尾にあるレーンIDです。
        /// </summary>
        
        public int EndLaneID;

        /// <summary>
        /// 最後尾にあるレーンのIDです。
        /// </summary>
        
        public float EndTime;

        /// <summary>
        /// ロングノーツの際に中間のノーツが存在するかどうかです。
        /// </summary>
        
        public bool IsBetweenNortsExist;

        /// <summary>
        /// 中間ノーツのためのものです。
        /// </summary>
        
        public PopBetweenPopData[] BetweenNortsData;

    }

}
