﻿///<summary>
/// 概要：中間ノーツ用のデータ
/// 基本的にLaneIDとタイミング時間のみが入る。
///
/// <filename>
/// PopBetweenPopData.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

namespace NortsCreator
{

    [System.Serializable]
    public class PopBetweenPopData
    {
        /// <summary>
        /// レーンID　０－５　場合によっては減る。
        /// </summary>
        
        public int BetWeenLineID;

        /// <summary>
        /// 判定のかかるタイミング用の時間
        /// </summary>
        
        public float TimingTime;
    }
}