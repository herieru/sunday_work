﻿///<summary>
/// 概要：データに対してアクセスを行うためのもの。
///
/// <filename>
/// DataAccesser.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace NortsCreator
{
    /// <summary>変更があったデータのタイプ</summary>
    public enum UpdateDataType
    {
        /// <summary>開始タイミングに対しての変更</summary>
        START_DATA,
        /// <summary>終了タイミングに対しての変更</summary>
        END_DATA,
        /// <summary>中間タイミングに対しての変更</summary>
        BETWEEN_DATA,
    }


    public class DataAccesser
    {
        /// <summary>
        /// スクロールの長さ TODO;いい感じにpropatyにする。
        /// </summary>
        public readonly float ScrollSize_X = 10000.0f;
        public readonly float ScrollSize_Y = 300.0f;
        private Vector2 scroll_size = Vector2.zero;

        /// <summary>
        /// 最大のラインID数 (上限設定)実数値は別
        /// </summary>
        public int MaxLaneID = 5;

        //（秒単位で間においていい中間データ）
        private float CapacityBetweenTime = 1f;

        //1秒毎にタイムラインの値を引く
        public readonly float TimeLineScrollLine = 1f;

        private AudioClip audio_clip = null;

        /// <summary>Editorのノーツデータ</summary>
        private List<EditNortsData> edit_norts_data;

        /// <summary>何か切り替わった際の更新</summary>
        private System.Action change_value_notice = null;

        /// <summary>BPM？の値</summary>
        private int bpm_stack;

        /// <summary>ロングノーツの作業中かどうか</summary>
        private bool is_long_norts_created = false;

        /// <summary>bpm</summary>
        private int bpm = 60;

        /// <summary>lpb</summary>
        private int lpb = 4;

        //最大の設定出来るレーンの数
        public int now_lane_max = 5;
        public float play_now_time;

        /// <summary>
        /// 追加された際に増えていき、減らない事で無理やりユニークIDを振っている。
        /// </summary>
        private int irreversible_uniqe_id;

        #region propaty

        /// <summary>スクロールのサイズ</summary>
        public Vector2 ScrollSize
        {
            get
            {
                if(scroll_size == Vector2.zero)
                {
                    scroll_size = new Vector2(ScrollSize_X, ScrollSize_Y);
                }
                return scroll_size;
            }
        }


        /// <summary>
        /// 描画用の配列データ
        /// </summary>
        public EditNortsData[] DisplayDatas
        {
            get
            {
                if (null == edit_norts_data)
                {
                    return null;
                }

                return edit_norts_data.ToArray();
            }
        }

        /// <summary>
        /// 音楽データ
        /// </summary>
        public AudioClip AudioData
        {
            get
            {
                return audio_clip;
            }
            set
            {
                if (null == value)
                {
                    return;
                }
                audio_clip = value;
                play_now_time = 0.0f;
            }
        }

        /// <summary>
        /// BPM単位で、何回描画しなければいけないか？
        /// </summary>
        public int DrawCount_on_music_length
        {
            get
            {
                if (null == audio_clip)
                {
                    return 0;
                }

                //丸めたものを返す
                return (int)(((float)bpm/60.0f) * audio_clip.length);
            }
        }


        /// <summary>
        /// BPMの横幅
        /// </summary>
        public float BPMWidth
        {
            get
            {
                if (null == audio_clip)
                {
                    return 0f;
                }
                return ScrollSize_X / DrawCount_on_music_length;
            }
        }

        /// <summary>lpbの横幅</summary>
        public float LPB_Width
        {
            get
            {
                //中があくまで4つなのでそれに併せて、＋1して割ることで調整
                return BPMWidth / (lpb + 1);
            }
        }


        /// <summary>
        /// スクロール上の縦方向の幅
        /// </summary>
        public float ScrollLineHeightWidth
        {
            get
            {
                return ScrollSize_Y / NowLaneMax;
            }
        }

        /// <summary>
        /// スクロール上の縦方向の幅の半分
        /// </summary>
        public float ScrollLineCorrectionWidth
        {
            get
            {
                return ScrollLineHeightWidth / 2;
            }
        }


        /// <summary>
        /// 今の再生時間に合わした、スクロール上のX軸の値を取得する
        /// </summary>
        public float NowPlayTimeWidthPos
        {
            get
            {
                float _correct_width = play_now_time;
                //０だと表示されないため、それを防ぐ処置
                if (_correct_width <= 0)
                {
                    _correct_width = 0.01f;
                }

                return BPMWidth * _correct_width;
            }
        }

        /// <summary></summary>
        public int NowLaneMax
        {
            get
            {
                return now_lane_max;
            }
        }

        public int BPM
        {
            get
            {
                return bpm;
            }
            //とりあえずの実装　関数を用意したほうがよいかも？
            set
            {
                bpm = value;
            }
        }

        public int LPB
        {
            get
            {
                return lpb;
            }
            //とりあえずの実装　関数を用意した方がよいかも？
            set
            {
                lpb = value;
            }
        }
        





        #endregion propaty



        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DataAccesser()
        {
            edit_norts_data = new List<EditNortsData>();
        }

        /// <summary>
        /// 何か切り替わった時に通知をするためのもの
        /// </summary>
        public void ChangeStateNotice(System.Action _change_value_notice)
        {
            change_value_notice = _change_value_notice;
        }

        /// <summary>
        /// ノーツのデータを作成する。
        /// </summary>
        /// <param name="_norts_type">ノーツのタイプ=タッチのタイプ</param>
        /// <param name="_mouse_touch_pos">マウスでのタッチ位置</param>
        public void CreateEditorNorts(int _norts_type, Vector2 _mouse_touch_pos)
        {
            int _line_id = serch_line_id(_mouse_touch_pos);
            _mouse_touch_pos = modification_touch_height(_mouse_touch_pos, _line_id);

            ///失敗処理
            if (_mouse_touch_pos == Vector2.zero)
            {
                return;
            }



            //ひとまず、特殊な状態中はこっちを全て通る
            if(is_long_norts_created)
            {
                create_long_norts_accesories(_norts_type, _mouse_touch_pos);
            }
            else
            {
                ///EditoIDをユニークにする必要があるので注意　いまのままだとかぶる。
                var _edit_norts = new EditNortsData(irreversible_uniqe_id, _mouse_touch_pos, _norts_type, _line_id,
                    audio_clip.length, new Vector2(
                        ScrollSize_X, ScrollSize_Y), NowLaneMax);
                edit_norts_data.Add(_edit_norts);

                ///ロングノーツ編集を開始した時点で、状態を変化する。
                if(_norts_type == (int)NortsData.NortsType.LongNorts)
                {
                    is_long_norts_created = !is_long_norts_created;
                }


                irreversible_uniqe_id++;
            }
        }




        /// <summary>
        /// ロングノーツに関わるノーツの作成方法
        /// ロングノーツ作成中に、０－＞BetWeenNortsとして扱う
        /// 1->終端ノーツとして扱う
        /// </summary>
        /// <param name="_norts_type">押されたマウスのタイプ</param>
        /// <param name="_mouse_pos"></param>
        private void create_long_norts_accesories(int _norts_type,Vector2 _mouse_pos)
        {
            int _new_norts_type = _norts_type;
            _new_norts_type = change_norts_type_on_long_norts(_new_norts_type);
            //対象のノーツを探しだして、それを元に変更を加える －1してるのは、それの影響
            EditNortsData _target_edit_norts = 
                edit_norts_data.Find((_edit_norts) => _edit_norts.edit_id == (irreversible_uniqe_id　 -  1));

            //あり得ないと思うが、見つからなかった時用の処理
            if(null == _target_edit_norts)
            {
                Debug.LogWarning("long_norts_accesories is not done");
                return;
            }

            int _lane_id = serch_line_id(_mouse_pos);
            float _new_time = _target_edit_norts.PosToTime(new Vector2(ScrollSize_X, ScrollSize_Y), _mouse_pos, audio_clip.length);


            //最初の地点より前に行ったら、ダメなので、置けない
            if (_target_edit_norts.Norts.StartTime >= _new_time)
            {
                return;
            }

            //実際のデータ挿入 念のため、特定のもの以外データ入れないようにする
            if(_new_norts_type == (int)NortsData.NortsType.Between)
            {
                _target_edit_norts.InsertBetweenNorts(scroll_size, audio_clip.length
                   ,now_lane_max , _lane_id, _mouse_pos);
            }
            else if(_new_norts_type == (int)NortsData.NortsType.LongNortsEnd)
            {
                _target_edit_norts.InsertLongNortsEnd(ScrollSize, audio_clip.length
                   , now_lane_max, _lane_id, _mouse_pos);
            }
            else
            {
                Debug.Log("この処理がきているのは何かが起きているに違いない。");
            }
            


            if (_norts_type == (int)NortsData.NortsType.LongNorts)
            {
                //元の状態に戻す。
                is_long_norts_created = !is_long_norts_created;
            }
        }

        /// <summary>
        /// ノーツのタイプをロング―ノーツ上に配置する場合のタイプに変更する。
        /// この関数は特定のノーツタイプを変換するだけなので、特定の手順を踏む前提です。
        /// </summary>
        /// <param name="_norts_type"></param>
        /// <returns>変換後のノーツタイプ</returns>
        private int change_norts_type_on_long_norts(int _norts_type)
        {
            //通常のノーツの場合は、
            if(_norts_type == (int)NortsData.NortsType.TouchDown)
            {
                _norts_type = (int)NortsData.NortsType.Between;
            }
            if(_norts_type == (int)NortsData.NortsType.LongNorts)
            {
                _norts_type = (int)NortsData.NortsType.LongNortsEnd;
            }
            return _norts_type;
        }

        /// <summary>
        /// 指定したIDのノーツのデータを消す
        /// </summary>
        public void DeleteEditNorts(EditNortsData _delete_data)
        {
            bool _sucusess = edit_norts_data.Remove(_delete_data);

            if(_sucusess)
            {
                Debug.Log("無事に消去できました。");
            }
        }

        /// <summary>
        /// 中間データの作成
        /// </summary>
        /// <param name="_edit_data">編集しているデータ</param>
        /// <param name="_action">追加に失敗した時に呼び出すための物</param>
        public void AddBetWeenData(EditNortsData _edit_data,int _add_num, System.Action _action)
        {
            _edit_data.CreateBetWeenNortsData(
                _add_num, CapacityBetweenTime, new Vector2(ScrollSize_X, ScrollSize_Y),
                audio_clip.length, NowLaneMax, _action);
        }

        /// <summary>
        /// 中間データを消す　前から何番目という形式
        /// </summary>
        /// <param name="_number"></param>
        public void DeleteBetWeenData(int _number)
        {

        }

        /// <summary>
        /// 外部から、編集を終えた際によび出される
        /// </summary>
        /// <param name="_type">変更があった、データタイプ</param>
        /// <param name="_edit_data">対象のデータ</param>
        public void UpdateEditData(UpdateDataType _type,EditNortsData _edit_data)
        {
             _edit_data.UpdateDisplayData(_type, new Vector2(ScrollSize_X, ScrollSize_Y), audio_clip.length, NowLaneMax);

            if(null == change_value_notice)
            {
                return;
            }
            change_value_notice();

        }

        /// <summary>
        /// タッチした位置からどのラインに属しているかをIDで返す。
        /// スクロールの高さ/ライン数の範囲がそのID
        /// </summary>
        /// <param name="_touch_pos"></param>
        /// <returns></returns>
        private int serch_line_id(Vector2 _touch_pos)
        {
            //一片の高さを調べる
            float _one_height = ScrollSize_Y / NowLaneMax;

            for (int _i = 0; _i < NowLaneMax; _i++)
            {
                float _front_range = _one_height * _i;
                float _back_range = _one_height * (_i + 1);


                if (_front_range < _touch_pos.y && _touch_pos.y < _back_range)
                {
                    return _i;
                }
            }

            Debug.LogWarning("ここには来ないはず");
            return 0;
        }


        /// <summary>
        /// ラインIDから、タッチした位置の高さを調整する。
        /// </summary>
        /// <param name="_touch_pos">タッチした一</param>
        /// <param name="_line_id">ラインID</param>
        /// <returns>修正がされたタッチの場所</returns>
        private Vector2 modification_touch_height(Vector2 _touch_pos, int _line_id)
        {
            //一片の高さを調べる
            float _one_height = ScrollSize_Y / NowLaneMax;

            for (int _i = 0; _i < NowLaneMax; _i++)
            {
                float _front_range = _one_height * _i;
                float _back_range = _one_height * (_i + 1);


                if (_front_range < _touch_pos.y && _touch_pos.y < _back_range)
                {
                    _touch_pos.y = (_front_range + _back_range) / 2;
					//return _touch_pos;
					break;
                }
            }

			float _width_p = 0.0f;

			//横の計算も行う  BPM->LPBの順番で、位置の情報を計算する？
			for(int _bpm_count = 0; _bpm_count < DrawCount_on_music_length;_bpm_count++)
			{
				_width_p = (_bpm_count + 1) * BPMWidth;

				//左から攻めていき、タッチしたところを手前になった時点
				if(_width_p < _touch_pos.x)
				{
					continue;
				}
				//超える前の状態をスタートとして設定しなおす
				_width_p　= _bpm_count * BPMWidth;

				float _new_width_p = _width_p;


				for(int _lpb_count = 0; _lpb_count <= LPB;_lpb_count++)
				{
					_new_width_p = _width_p + (_lpb_count + 1) * LPB_Width;

					if(_new_width_p >= _touch_pos.x)
					{

						_touch_pos.x = _width_p + _lpb_count * LPB_Width;

						Debug.Log("最終的なタッチの位置：" + _touch_pos);

						return _touch_pos;
					}
				}

			}
			

            Debug.LogWarning("ここには来てると何か計算式がおかしい");
            return Vector2.zero;
        }


        /// <summary>
        /// EditNortsからNortsのデータ
        /// </summary>
        /// <returns></returns>
        public  List<NortsData> GetNeedPropatyOnly()
        {
            List<NortsData> _norts_data = new List<NortsData>();
            foreach(var _edit_data in edit_norts_data)
            {
                _norts_data.Add(_edit_data.Norts);
            }

            return _norts_data;
        }

        
    }
}
