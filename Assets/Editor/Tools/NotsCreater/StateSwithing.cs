﻿///<summary>
/// 概要：状態によって、表示する部分や処理を変更するためのものです。
/// ここで基本的な表示を行い、ここはインターフェースを通して、
/// 呼び出しを行っているだけ。
/// 
///
/// <filename>
/// StateSwithing.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;

namespace NortsCreator
{
    /// <summary>
    /// ノーツ作成するツールの状態です。
    /// </summary>
    public enum NortsToolState
    {
        None,
        Creater,
        ReEditing,
    }


    public class StateSwithing
    {
        private NortsToolState current_state;
        private ViewBase view_current;
        private DataAccesser database_access;
        
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public StateSwithing()
        {
            //最初の始まりは、いつもここから
            current_state = NortsToolState.None;
            database_access = new DataAccesser();
            view_current = new ModeSelectView();
            view_current.DataBankAccessSet(database_access);
            view_current.SetNotice(ChangingState);
        }

        /// <summary>
        /// 描画を行う。
        /// </summary>
        public void Display()
        {
            if(null == view_current)
            {
                return;
            }
            view_current.MenuSpace();
            view_current.TimeLineSpace();
            view_current.PlayMusicSpace();
            view_current.EventUpdate();
        }

        /// <summary>
        /// 状態が変化あった際に、と呼び出される
        /// </summary>
        /// <param name="_toolstate"></param>
        public void ChangingState(NortsToolState _toolstate)
        {
            current_state = _toolstate;


            switch (current_state)
            {
                //基本的にはならない
                case NortsToolState.None:
                    break;
                case NortsToolState.Creater:
                    view_current = new CreateNortsView();
                    break;
                case NortsToolState.ReEditing:
                    break;
            }
            set_add_infomation();

        }

        private void set_add_infomation()
        {
            if(null == view_current)
            {
                return;
            }

            view_current.Init();
            view_current.DataBankAccessSet(database_access);
            view_current.SetNotice(ChangingState);
        }
    }
}
