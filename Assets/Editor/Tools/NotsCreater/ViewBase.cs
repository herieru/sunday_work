﻿///<summary>
/// 概要：ノーツツールのビューの基底クラスです。
/// ここでは、データベースへのアクセスだけを受け取って、それに対して
/// どんどん変更点とかを行っていきます。
///
/// <filename>
/// ViewBase.cs
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailladdress:herie270714@gmail.com
/// </address>
///</summary>


using System.Collections.Generic;
using UnityEngine;
namespace NortsCreator
{
    /// <summary>
    /// 描画が必要ないところは、ここで消す。
    /// </summary>
    interface IView
    {
        /// <summary>
        /// メニュー等の場所
        /// </summary>
        void MenuSpace();

        /// <summary>
        /// タイムラインの場所
        /// </summary>
        void TimeLineSpace();

        /// <summary>
        /// 音楽を再生する箇所
        /// </summary>
        void PlayMusicSpace();
    }


    public abstract class ViewBase : EditorEasyBase, IView
    {
        #region field
        /// <summary>
        /// 状態が切り替わる状態の時に呼び出される。
        /// </summary>
        protected System.Action<NortsToolState> change_state_notice;

        protected DataAccesser data_access;

        #endregion

        /// <summary>
        /// データへのアクセスを行うためのもの。
        /// </summary>
        /// <param name="_access"></param>
        public virtual void DataBankAccessSet(DataAccesser _access)
        {
            data_access = _access;
        }
        public abstract void SetNotice(System.Action<NortsToolState> action);



        public abstract void Init();


        public abstract void MenuSpace();

        public abstract void PlayMusicSpace();

        public abstract void TimeLineSpace();

        /// <summary>
        /// イベントの更新
        /// </summary>
        public abstract void EventUpdate();


        
    }

}
