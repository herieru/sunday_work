﻿///<summary>
/// 概要:ヒエラルキーから指定のソースファイルのコンポーネントの
/// ついているオブジェクトを取得してくれる。
/// 一杯オブジェクトがついている状態とかなら、便利。
///
///
/// <filename>
/// ファイル名:FindHierarchy
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>



using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEditor;

public class FindHierarchy :EditorEasyBase
{

    [MenuItem("Tools/FindHierarch")]
    public static void Done()
    {
        GetWindow<FindHierarchy>();
    }
    /// <summary>
    /// セットを行うためのスクリプトオブジェクト
    /// </summary>
    private Object script;

    private MonoScript MonoScript;
    
    /// <summary>
    /// 見つけ出したゲームオブジェクト
    /// </summary>
    private List<GameObject> pick_up_g_obj;

    /// <summary>
    /// 見つけ出したコンポーネントの名前
    /// </summary>
    private List<Component> pick_up_comp;


    private Vector2 scroll_pos = Vector2.zero;

    void Start()
    {
        pick_up_g_obj = new List<GameObject>();
    }


    void OnGUI()
    {
        var _setting_script = EditorGUILayout.ObjectField(script, typeof(Object),false, GUILayout.ExpandWidth(true));
        Button("状態クリア",clear_status, GUILayout.ExpandWidth(true));
        Button("探索開始！",find_object, GUILayout.ExpandWidth(true));
        ScrollVirticalBlock(ref scroll_pos, show_pickup_gameobjs, GUILayout.ExpandWidth(true));

        if(GUI.changed)
        {
            if(null != _setting_script as MonoScript)
            {
                if(script != null)
                {
                    
                }
                script = _setting_script;
            }
        }
    }

    /// <summary>
    /// 状態をクリアする。
    /// </summary>
    private void clear_status()
    {
        pick_up_g_obj.Clear();
        pick_up_comp.Clear();
        script = null;
    }

    /// <summary>
    /// ピックアップしたGameObjectを全て見せる
    /// </summary>
    private void show_pickup_gameobjs()
    {
        if(null == pick_up_g_obj)
        {
            HackDebug.Console.Log("ゲームオブジェクトが存在しません");
            return;
        }
        
       if(null == pick_up_comp)
        {
            HackDebug.Console.Log("コンポーネントが存在しません");
            return;
        }

       if(pick_up_g_obj.Count > pick_up_comp.Count)
        {
            HackDebug.Console.Log("あれれ？数がゲームオブジェクトの方が多いよ");
            return;
        }

         

        int _count = pick_up_g_obj.Count;
        //同数前提で回す
        for (int _index = 0;_index < _count;_index++)
        {
            GameObject _obj = pick_up_g_obj[_index];
            EditorGUILayout.ObjectField(_obj, typeof(GameObject),false, GUILayout.ExpandWidth(true));
            //FIXME:同数じゃない場合は考慮してないので、注意
            EditorGUILayout.BeginHorizontal();
            {
                Space();
                string _display_name = pick_up_comp[_index].ToString();
                string _base_name = pick_up_comp[_index].GetType().BaseType != typeof(MonoBehaviour).GetType() ? ":" + pick_up_comp[_index].GetType().BaseType.ToString() : "";
                if (_base_name.Length > 0)
                {
                    _display_name = _display_name + _base_name;
                }
                Label(_display_name, GUILayout.ExpandWidth(true));

                
            }
            EditorGUILayout.EndHorizontal();
        }

    }

    /// <summary>
    /// ヒエラルキーの中から、対象のモノを探す。
    /// </summary>
    private void find_object()
    {
        if(null == script)
        {
            return;
        }

        if (null == pick_up_g_obj)
        {
            pick_up_g_obj = new List<GameObject>();
        }
        pick_up_g_obj.Clear();

        // 現在マウスとかで選択している物があれば取得される
        GameObject _select_obj = Selection.activeGameObject;
        if(null == _select_obj)
        {
            get_game_object_all_in_hierarchy();
        }
        else
        {
            get_game_object_in_selection(_select_obj);
        }

    }

    /// <summary>
    /// ヒエラルキーの中から探す
    /// </summary>
    private void get_game_object_all_in_hierarchy()
    {
        Scene _scene = SceneManager.GetActiveScene();
        Debug.Log("serch_name " + script.name);
        //取得したゲームオブジェクトを全て拾ってきてGetComponetしてあるか調べる
        //foreach (Object _obj in Resources.FindObjectsOfTypeAll(typeof(Object)))
        foreach(GameObject _obj in _scene.GetRootGameObjects())
        {
            //ゲームオブジェクトじゃなかったら無視
            GameObject _g_obj = _obj as GameObject;
            if(null == _g_obj)
            {
                continue;
            }

            Component _component = _g_obj.GetComponent(script.name);
            if (null == _component)
            {
                continue;
            }
            pick_up_g_obj.Add(_component.gameObject);
            pick_up_comp.Add(_component);
        }
    }
    
    /// <summary>
    /// 選んでいるオブジェクトの中から探す。
    /// FIX；コメントと関数名を適切なものに変更する必要がある。
    /// <param name ="_obj">選択しているゲームオブジェクト</param>
    /// </summary>
    private void get_game_object_in_selection(GameObject _obj)
    {
        Component _comp = _obj.GetComponent(script.name);
        if(null != _comp)
        {
            pick_up_g_obj.Add(_comp.gameObject);
        }
        serch_childeren(_obj);

    }
    /// <summary>
    /// 再帰的にゲームオブジェクトを探す旅の処理を探す。
    /// </summary>
    private void serch_childeren(GameObject _parent_obj)
    {
        int _count = _parent_obj.transform.childCount;
        for(int _i = 0;_i < _count; _i++)
        {
            //子要素としてあるゲームオブジェクトを取得する
            GameObject _obj = _parent_obj.transform.GetChild(_i).gameObject;
            if(null == _obj)
            {
                continue;
            }
            //取得したものに、ほしいコンポーネントがあるかを取得してserch_childrenが呼ばれる
            get_game_object_in_selection(_obj);

        }
    }

}
