﻿///<summary>
/// 概要:Editor拡張を行いやすくするためのもの
/// 基本的に、レイアウト系は、全てやってくれるので、すごく楽な作りになっている。
/// っていうのがEditorGUILayoutを使用するのが基本
/// 拡張する際にGUILayoutを使用してしまうと、位置がずれてしまう可能性あり
/// <filename>
/// ファイル名:EditorBase
/// </filename>
///
/// <creatername>
/// 作成者：堀　明博
/// </creatername>
/// 
/// <address>
/// mailaddress:herie270714@gmail.com
/// </address>
///</summary>


using UnityEngine;
using UnityEditor;

public class EditorEasyBase : EditorWindow
{

    /// <summary>
    /// GUIStyleの検索と取得
    /// </summary>
    /// <param name="_styleName"></param>
    /// <returns></returns>
    public GUIStyle get_style(string _styleName)
    {
        var _UIStyle = GUI.skin.FindStyle(_styleName);
        if (_UIStyle == null)
        {
            _UIStyle = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).FindStyle(_styleName);
        }
        if (_UIStyle == null)
        {
            Debug.Log("取得できなかったよ" + _styleName);
            _UIStyle = new GUIStyle();
        }
        return _UIStyle;
    }
    /// <summary>
    /// 一つのブロックとしてまとめる
    /// 縦型に配置vertical attangement
    /// </summary>
    /// <param name="_title">このブロックのタイトル</param>
    /// <param name="_action">このブロックの中の描画関数呼び出し</param>
    /// <returns>描画範囲を返せる</returns>
    public Rect BlockVertical
        (
        string _title,
        System.Action _action,
        params GUILayoutOption[] _layout
        )
    {
        Rect _rect = EditorGUILayout.BeginVertical(_layout);
        {
            if (null != _title && _title.Length > 0)
            {
                Label(_title);
            }
            if (null != _action)
            {
                _action();
            }
        }
        EditorGUILayout.EndVertical();
        return _rect;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_title"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    /// <returns></returns>
    public Rect BlockHorizontal(string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        Rect _rect = EditorGUILayout.BeginHorizontal();
        {
            if (null != _title && _title.Length > 0)
            {
                Label(_title);
            }
            if (null != _action)
            {
                _action();
            }
        }
        EditorGUILayout.EndHorizontal();
        return _rect;
    }

    /// <summary>
    /// ラベル一つのラベル
    /// </summary>
    public void Label(string _message, params GUILayoutOption[] _layout)
    {
        EditorGUILayout.LabelField(_message, _layout);
    }

    /// <summary>
    /// TODO;Rectが取れる別のボタンを使いたいな。
    /// </summary>
    /// <param name="_message"></param>
    /// <param name="_action"></param>
    public Rect Button(string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        Rect _rect = EditorGUILayout.BeginVertical("Button", _layout);
        {
            if (GUI.Button(_rect, GUIContent.none))
            {
                if (null != _action)
                {
                    _action();
                }
            }
            Label(_title);
        }
        EditorGUILayout.EndVertical();

        return _rect;
    }

    /// <summary>
    /// GUILayoutを使用した、ボタン
    /// </summary>
    /// <param name="_title"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    public void LayoutButton(string _title,System.Action _action,params GUILayoutOption[] _layout)
    {
        if(GUILayout.Button(_title, EditorStyles.miniButton,_layout))
        {
            if (null != _action)
            {
                _action();
            }
        }
    }



    /// <summary>
    /// スクロール用ブロック。縦型
    /// </summary>
    /// <param name="_scroll_pos"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    public Rect ScrollVirticalBlock(ref Vector2 _scroll_pos, System.Action _action, params GUILayoutOption[] _layout)
    {
        Rect _rect = EditorGUILayout.BeginVertical();
        {
            scroll(ref _scroll_pos, _action, _layout);
        }
        EditorGUILayout.EndVertical();

        return _rect;
    }

    /// <summary>
    /// スクロール用ブロック横型
    /// </summary>
    /// <param name="_scroll_pos"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    public Rect ScrollHorizontalBlock(ref Vector2 _scroll_pos, System.Action _action, params GUILayoutOption[] _layout)
    {
        Rect _rect = EditorGUILayout.BeginHorizontal();
        {
            scroll(ref _scroll_pos, _action, _layout);
        }
        EditorGUILayout.EndHorizontal();

        return _rect;
    }

    /// <summary>
    /// スクロールの本体
    /// </summary>
    /// <param name="_scroll_pos"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    private void scroll(ref Vector2 _scroll_pos, System.Action _action, GUILayoutOption[] _layout)
    {
        _scroll_pos = EditorGUILayout.BeginScrollView(_scroll_pos, _layout);
        {
            if (null != _action)
            {
                _action();
            }
        }
        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// Int型でのフィールド
    /// </summary>
    /// <param name="_value"></param>
    /// <param name="_layout"></param>
    /// <returns></returns>
    public void IntField(ref int _value, params GUILayoutOption[] _layout)
    {
        _value = EditorGUILayout.IntField(_value, _layout);
    }

    /// <summary>
    /// トグルによる中身までの生成するもの
    /// </summary>
    /// <param name="_toggle">トグルの値</param>
    /// <param name="_title">トグルのためのタイトル</param>
    /// <param name="_action">トグルがtrue時に呼び出すためのデリゲート</param>
    /// <param name="_layout">レイアウトオプション</param>
    public void Toggle(ref bool _toggle, string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        _toggle = EditorGUILayout.Toggle(_title, _toggle, _layout);
        if (_toggle)
        {
            if (null != _action)
            {
                _action();
            }
        }
    }


    /// <summary>
    /// トグルにスタイルを入れたもの
    /// </summary>
    /// <param name="_toggle"></param>
    /// <param name="_title"></param>
    /// <param name="_style_name"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    public bool Toggle(bool _toggle, string _title, string _style_name, System.Action _action, params GUILayoutOption[] _layout)
    {
        _toggle = GUILayout.Toggle(_toggle, _title, get_style(_style_name), _layout);
        if (_toggle)
        {
            if (null != _action)
            {
                _action();
            }
        }

        return _toggle;
    }

    /// <summary>
    /// 返り値が存在するトグル
    /// </summary>
    /// <param name="_toggle"></param>
    /// <param name="_title"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    /// <returns></returns>
    public bool Toggle(bool _toggle, string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        _toggle = EditorGUILayout.Toggle(_title, _toggle, _layout);
        if (_toggle)
        {
            if (null != _action)
            {
                _action();
            }
        }

        return _toggle;
    }

    /// <summary>
    /// ラジオボタン、変更したら通知を受け取れる。
    /// </summary>
    /// <param name="_now_select">現在選択しているもの</param>
    /// <param name="_radio_name">ラジオで表示する文字列</param>
    /// <param name="_notification">変更された際に通知を行うためのもの</param>
    /// <param name="_layout">レイアウト</param>
    /// <returns>選択しているもの</returns>
    public int Radio(int _now_select,string[] _radio_name,System.Action<int> _notification,params GUILayoutOption[] _layout)
    {
        int _prev_select = _now_select;
        bool[] _radio_prev = new bool[_radio_name.Length];
        _radio_prev[_now_select] = true;

        //羅列分描画を行う。
        for (int _i = 0;_i < _radio_name.Length;_i++)
        {
            bool _result = EditorGUILayout.Toggle(_radio_name[_i], _radio_prev[_i], EditorStyles.radioButton, _layout);
                //_radio_prev[_i],_radio_name[_i],null,_layout);
            
            if(_result != _radio_prev[_i])
            {
                _now_select = _i;
            }

        }

        
        //選択しているものが違う状態になったら通知
        if(null != _notification && _now_select != _prev_select)
        {
            _notification(_now_select);
        }

        return _now_select;
    }

    /// <summary>
    /// 左詰めのトグル
    /// チェックボックスを入れる事で、その中身が表示される。
    /// </summary>
    /// <param name="_toggle"></param>
    /// <param name="_title"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    public void ToggleLeft(ref bool _toggle, string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        _toggle = EditorGUILayout.ToggleLeft(_title, _toggle, _layout);
        if (_toggle)
        {
            if (null != _action)
            {
                _action();
            }
        }
    }

    /// <summary>
    /// 左詰めのトグル
    /// チェックボックスを入れる事で、その中身が表示される。
    /// </summary>
    /// <param name="_toggle"></param>
    /// <param name="_title"></param>
    /// <param name="_action"></param>
    /// <param name="_layout"></param>
    public bool ToggleLeft(bool _toggle, string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        _toggle = EditorGUILayout.ToggleLeft(_title, _toggle, _layout);
        if (_toggle)
        {
            if (null != _action)
            {
                _action();
            }
        }
        return _toggle;
    }

    /// <summary>
    /// 切り替わった際のみ処理を通す際トグル用
    /// </summary>
    /// <param name="_toggle">現在のトグルの状態</param>
    /// <param name="_cheak">この状態に切り替わった時だけ</param>
    /// <param name="_title">タイトル名</param>
    /// <param name="_action">切り替わった際に、行う処理</param>
    /// <param name="_layout">レイアウト</param>
    /// <returns></returns>
    public bool ToggleLeftCheakChanges(bool _toggle, bool _cheak, string _title, System.Action _action, params GUILayoutOption[] _layout)
    {
        bool _now_toggle_stats = _toggle;

        _toggle = EditorGUILayout.ToggleLeft(_title, _toggle, _layout);
        if (_toggle != _now_toggle_stats && _toggle == _cheak)
        {
            if (null != _action)
            {
                _action();
            }
        }
        return _toggle;

    }

    /// <summary>
    /// ツールバーのチェックを行うバージョン
    /// </summary>
    /// <param name="_selected"></param>
    /// <param name="_tags"></param>
    /// <param name="_selected_actions">選択したデリゲート型を呼び出し</param>
    /// <returns>変更がかかったかを一応返す</returns>
    public bool ToolbarCheakChanges(ref int _selected,string[] _tags,params System.Action[] _selected_actions)
    {
        if(null == _tags || null == _selected_actions)
        {
            return false;
        }



        if(_tags.Length != _selected_actions.Length)
        {
            HackDebug.Console.LogError("数が一致しないため、ツールバーが表示できません");
        }

        int _new_select = GUILayout.Toolbar(_selected, _tags, GUILayout.ExpandWidth(true));

        if(null != _selected_actions[_new_select])
        {
            _selected_actions[_new_select]();
        }


        if(_new_select != _selected)
        {
            _selected = _new_select;
            Repaint();
            return true;
        }

        return false;
    }




    /// <summary>
    /// このくくりをまとめるためのもの
    /// </summary>
    /// <param name="_is_open">開くかどうか</param>
    /// <param name="_title">このまとめる項目名</param>
    /// <param name="_action">開いた時の中身</param>
    public void Folding(ref bool _is_open, string _title, System.Action _action)
    {
        _is_open = EditorGUILayout.Foldout(_is_open, _title);
        if (_is_open)
        {
            if (null == _action)
            {
                _action();
            }
        }

    }

    /// <summary>
    /// スペースをとる
    /// </summary>
    public void Space()
    {
        EditorGUILayout.Space();
    }
    #region 限定的な用途のもの

    /// <summary>
    /// タイムラインのスライダー
    /// </summary>
    /// <param name="_value">現在の値</param>
    /// <param name="_min_value">最小値</param>
    /// <param name="_max_value"></param>
    /// <param name="_split_count"></param>
    /// <param name="_layout"></param>
    //タイムラインスライダーを描画
    public float TimeLineSlider
        (float _value,
        float _min_value,
        float _max_value,
        int _split_count,
        params GUILayoutOption[] _layout)
    {
        Rect _rect = EditorGUILayout.BeginVertical(_layout);
        {
            _value = GUILayout.HorizontalSlider(_value, _min_value, _max_value,
                "box", "box", _layout);
        }
        EditorGUILayout.EndVertical();

        //分割した分の一片の横幅
        float _one_width = _rect.width / _split_count;
        float _one_parameter = (_max_value - _min_value) / _split_count;

        for (int _i = 0; _i < _split_count; _i++)
        {
            Handles.DrawLine
                (
                new Vector2(_rect.x + _one_width * _i, _rect.y),
                new Vector2(_rect.x + _one_width * _i, _rect.y + _rect.height - 10.0f)
                );
            //上部に描画
            Handles.Label
                (
                new Vector2(_rect.x + _one_width * _i, _rect.y - 10.0f),
                (_one_parameter * _i).ToString("0.0")
                );
        }
        return _value;
    }

    /// <summary>
    /// 分割する量を決めて、行うタイムラインスライダー
    /// </summary>
    /// <param name="_now_time">現在の値</param>
    /// <param name="_min_value">最小値</param>
    /// <param name="_max_value">最大値</param>
    /// <param name="_split_value">分ける分量</param>
    /// <param name="_layout">レイアウト</param>
    public float TimeLineSliderFixedMemory
        (
        float _now_time,
        float _min_value,
        float _max_value,
        float _split_value,
        params GUILayoutOption[] _layout
        )
    {
        Space();
        Rect _rect = EditorGUILayout.BeginVertical(_layout);
        {
            _now_time = GUILayout.HorizontalSlider(_now_time, _min_value, _max_value,
                "box", "box", _layout);
        }
        EditorGUILayout.EndVertical();

        //分割した分の一片の横幅
        int _split_count = (int)((_max_value - _min_value) / _split_value);

        float _one_width = _rect.width / _split_count;
        float _one_parameter = (_max_value - _min_value) / _split_count;

        for (int _i = 0; _i < _split_count; _i++)
        {
            Handles.DrawLine
                (
                new Vector2(_rect.x + _one_width * _i, _rect.y),
                //-10しているのは調整用
                new Vector2(_rect.x + _one_width * _i, _rect.y + _rect.height)
                );
            //上部に描画
            Handles.Label
                (
                new Vector2(_rect.x + _one_width * _i, _rect.y + _rect.height),
                (_one_parameter * _i).ToString("0.0")
                );
        }

        return _now_time;
    }
    


    /// <summary>
    /// サーチウィンドウ
    /// </summary>
    /// <param name="_search_text">検索する文字列</param>
    /// <param name="_layout"></param>
    /// <returns></returns>
    public string SerchWindow(string _search_text, params GUILayoutOption[] _layout)
    {
        _search_text = EditorGUILayout.TextField(_search_text, get_style("ToolbarSeachTextField"), _layout);
        return _search_text;
    }


    /// <summary>
    /// ダイアログを出すためのもの簡易に出すことが出来る。
    /// </summary>
    /// <param name="_title">ウィンドウのタイトル</param>
    /// <param name="_message">ダイアログを出した時のメッセージ</param>
    /// <param name="_ok_delegate"></param>
    /// <param name="_ng_delegate"></param>
    public void DisplayDialog
        (
        string _title,
        string _message,
        System.Action _ok_delegate,
        System.Action _ng_delegate)
    {
        bool _result = EditorUtility.DisplayDialog
            (
            _title,
            _message,
            "OK",
            "NG"
            );

        if (_result)
        {
            if (null == _ok_delegate)
            {
                return;
            }
            _ok_delegate();
        }
        else
        {
            if (null == _ng_delegate)
            {
                return;
            }
            _ng_delegate();
        }
    }
#endregion

    #region ボーダーライン

    /// <summary>
    /// ボーダーラインを引く
    /// _layout部分で線のふとさを調整してください
    /// かならず、オプションには、Width、Height系で指定
    /// </summary>
    /// <param name="_layout"></param>
    public void BorderLine(params GUILayoutOption[] _layout)
    {
        GUILayout.Box(GUIContent.none, _layout);
    }


    /// <summary>
    /// シンプルにボーダーラインを引く。
    /// 横線のものを引く
    /// </summary>
    public void SimpleBorderLineHorizontal()
    {
        GUILayout.Box(GUIContent.none, GUILayout.ExpandWidth(true), GUILayout.Height(1f));
    }

    /// <summary>
    /// シンプルにボーダーラインを引く
    /// 縦線のものを引く
    /// </summary>
    public void SimpleBorderLineVerical()
    {
        GUILayout.Box(GUIContent.none, GUILayout.Width(1f), GUILayout.ExpandHeight(true));
    }
    #endregion


    #region FreeDrawLine

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_start_pos"></param>
    /// <param name="_end_pos"></param>
    /// <param name="_color"></param>
    public void FreeDrawColorLine(Vector2 _start_pos,Vector2 _end_pos ,Color _color)
    {
        Color _base_color = Handles.color;
        Handles.color = _color;
        FreeDrawLine(_start_pos, _end_pos);
        Handles.color = _base_color;
    }

    /// <summary>
    /// 好きな位置で、線を描く
    /// ただし、ブロックの中だとそこ基準
    /// </summary>
    /// <param name="_start_pos"></param>
    /// <param name="_end_pos"></param>
    public void FreeDrawLine(Vector2 _start_pos,Vector2 _end_pos)
    {
        Handles.DrawLine(_start_pos, _end_pos);
    }

    #endregion

    #region Compound Parts 複合のパーツ群


    /// <summary>
    /// 数字と、それをボタンで動向する形のもの
    /// この中では、インクリメントとデクリメントしません。
    ///             △
    /// ラベル：数字　　                という形
    ///             ▽
    /// _layoutはWidthのみの適用にしてください ラベルのみの適用になります。
    /// </summary>
    /// <param name="_label">ラベル</param>
    /// <param name="_num">現在の数値</param>
    /// <param name="_up_button">上ボタンを押した時の反応</param>
    /// <param name="_down_action">下ボタンを押した時の反応</param>
    public int InDecrementParts(string _label, int _num,System.Action _up_button,System.Action _down_action, params GUILayoutOption[] _layout)
    {
        BlockHorizontal("", () =>
        {
            Label(_label, _layout);
            _num = EditorGUILayout.IntField(_num, GUILayout.Width(30.0f),GUILayout.Height(20.0f));
            BlockVertical("", () =>
            {
                LayoutButton("△", _up_button,GUILayout.Width(20.0f),GUILayout.Height(10.0f));
                LayoutButton("▽", _down_action, GUILayout.Width(20.0f), GUILayout.Height(10.0f));
            },GUILayout.Width(20.0f),GUILayout.Height(20.0f));
            GUILayout.FlexibleSpace();
        });

        return _num;
    }


    /// <summary>
    /// シンプルな複合パーツ　InDecrementPartsの形を作る。
    /// </summary>
    /// <param name="_num">現在の数値/param>
    /// <param name="_diff_num">押された時の差分</param>
    /// <returns></returns>
    public int SimpleEcrementParts(string _label,int _num,int _diff_num)
    {
        InDecrementParts(_label,_num,
            () => { _num+= _diff_num; },
            () => { _num-= _diff_num; },GUILayout.Width(70.0f),GUILayout.Width(40.0f));

        return _num;
    }

    #endregion

}

